#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GenCStringList {prefix len} {
    set res ""

    set i 0
    while {$i < $len} {
        append res "\"$prefix$i\","
        incr i
    }

    return [string trim $res ","]
}

proc GenExtractorHouseStrs {appCnt} {
    global configs nb

    set res "\"$configs(${appCnt}:extractor0)\""
    set i 1
    while {$i < $nb(${appCnt}:extractor)} {
        append res ",\"$configs(${appCnt}:extractor$i)\""
        incr i
    }

    return $res
}

proc GetHouseCompType {house type appCnt} {
    global configs nb

    set i 0
    while {$i < $nb($appCnt:$type)} {
        if {$house in $configs($appCnt:$type$i)} {
            return $i
        }

        incr i
    }
}

proc GenHouseRecognizers {appCnt} {
    global nb houseCntBase

    set res [GetHouseCompType $houseCntBase recognizer $appCnt]
    set i 1
    while {$i < $nb($appCnt:db)} {
        append res ",[GetHouseCompType [expr $houseCntBase+$i] recognizer $appCnt]"
        incr i
    }

    return $res
}

proc GenHouseDeciders {appCnt} {
    global nb houseCntBase

    set res [GetHouseCompType $houseCntBase decider $appCnt]
    set i 1
    while {$i < $nb($appCnt:db)} {
        append res ",[GetHouseCompType [expr $houseCntBase+$i] decider $appCnt]"
        incr i
    }

    return $res
}

proc GenHouseExecuters {appCnt} {
    global nb houseCntBase

    set res [GetHouseCompType $houseCntBase executer $appCnt]
    set i 1
    while {$i < $nb($appCnt:db)} {
        append res ",[GetHouseCompType [expr $houseCntBase+$i] executer $appCnt]"
        incr i
    }

    return $res
}

proc GenHouseDBs {appCnt} {
    global nb houseCntBase

    set res [GetHouseCompType $houseCntBase db $appCnt]
    set i 1
    while {$i < $nb($appCnt:db)} {
        append res ",[GetHouseCompType [expr $houseCntBase+$i] db $appCnt]"
        incr i
    }

    return $res
}

proc GetBW {bind} {
    foreach {bw lat} $bind {
        if {[regexp {^([0-9\.]+)MBps$} $bw vv value]} {
            return [expr $value * 1000000]
        } else {
            puts "Error : BW match failed!"
        }
    }
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ gen ~~~~~~~~~~~~~~~~~~~~~~~~~~
set houseCntBase 0

set db_len $nb(0:db)
set decider_len $nb(0:decider)
set executer_len $nb(0:executer)
set recognizer_len $nb(0:recognizer)
set extractor_len $nb(0:extractor)

set HouseNb $nb(0:db)
set ExtractorNb $nb(0:extractor)
set DeciderNb $nb(0:decider)

set mb(camera) [GenCStringList "camera" $nb(House)]
set mb(screen) [GenCStringList "screen" $nb(House)]
set mb(db)         "{[GenCStringList "0:db"         $nb(0:db)]}"
set mb(decider)    "{[GenCStringList "0:decider"    $nb(0:decider)]}"
set mb(executer)   "{[GenCStringList "0:executer"   $nb(0:executer)]}"
set mb(recognizer) "{[GenCStringList "0:recognizer" $nb(0:recognizer)]}"

set house_recognizer "[GenHouseRecognizers 0]"
set house_decider "[GenHouseDeciders 0]"
set house_executer "[GenHouseExecuters 0]"
set house_db "[GenHouseDBs 0]"
set extractor_houses "{[GenExtractorHouseStrs 0]}"

set houseCntBase $nb(0:db)

set i 1
while {$i < $appCnt} {
    append mb(db)         ",{[GenCStringList "$i:db" $nb($i:db)]}"
    append mb(decider)    ",{[GenCStringList "$i:decider" $nb($i:decider)]}"
    append mb(executer)   ",{[GenCStringList "$i:executer" $nb($i:executer)]}"
    append mb(recognizer) ",{[GenCStringList "$i:recognizer" $nb($i:recognizer)]}"

    append HouseNb ",$nb($i:db)"
    append ExtractorNb ",$nb($i:extractor)"
    append DeciderNb ",$nb($i:decider)"

    append house_recognizer ",[GenHouseRecognizers $i]"
    append house_decider ",[GenHouseDeciders $i]"
    append house_executer ",[GenHouseExecuters $i]"
    append house_db ",[GenHouseDBs $i]"
    append extractor_houses ",{[GenExtractorHouseStrs $i]}"

    if {$db_len < $nb($i:db)} {
        set db_len $nb($i:db)
    }

    if {$decider_len < $nb($i:decider)} {
        set decider_len $nb($i:decider)
    }

    if {$executer_len < $nb($i:executer)} {
        set executer_len $nb($i:executer)
    }

    if {$recognizer_len < $nb($i:recognizer)} {
        set recognizer_len $nb($i:recognizer)
    }

    if {$extractor_len < $nb($i:extractor)} {
        set extractor_len $nb($i:extractor)
    }

    set houseCntBase [expr $houseCntBase + $nb($i:db)]
    
    incr i
}


set h_content "#ifndef _GENVARS_H__
#define _GENVARS_H__

#define KillTime [expr $nb(House) * 200000 + 100000]
#define TestCount 1

#define CameraStepTime 5

#define DB_PartNb   1

//cal size
#define Cal_Extract 100
#define Cal_Decide  40
#define Cal_Execute 20
#define Cal_Recorde 1000
#define Cal_RecognizeTotal 1000
#define Cal_DB 20
#define Cal_Min 1

//comm size
#define Comm_CameraImg 1000
#define Comm_Face 150
#define Comm_DB_SizeTotal 100000
#define Comm_Min 1

//link type bw
#define BW_camera2extractor     [GetBW $bind_type(camera,extractor)]
#define BW_extractor2camera     [GetBW $bind_type(extractor,camera)]
#define BW_extractor2decider    [GetBW $bind_type(extractor,decider)]
#define BW_decider2extractor    [GetBW $bind_type(decider,extractor)]
#define BW_extractor2recognizer [GetBW $bind_type(extractor,recognizer)]
#define BW_recognizer2extractor [GetBW $bind_type(recognizer,extractor)]
#define BW_recognizer2db        [GetBW $bind_type(recognizer,db)]
#define BW_db2recognizer        [GetBW $bind_type(db,recognizer)]
#define BW_decider2recorder     [GetBW $bind_type(decider,recorder)]
#define BW_recorder2decider     [GetBW $bind_type(recorder,decider)]
#define BW_decider2executer     [GetBW $bind_type(decider,executer)]
#define BW_executer2decider     [GetBW $bind_type(executer,decider)]
#define BW_executer2screen      [GetBW $bind_type(executer,screen)]
#define BW_screen2executer      [GetBW $bind_type(screen,executer)]

//mailbox
extern char mb_camera\[]\[30];
extern char mb_screen\[]\[30];
extern char mb_db\[]\[$db_len]\[30];
extern char mb_decider\[]\[$decider_len]\[30];
extern char mb_executer\[]\[$executer_len]\[30];
extern char mb_recognizer\[]\[$recognizer_len]\[30];
 
extern int house_recognizer\[];
extern int house_decider\[];
extern int house_executer\[];
extern int house_db\[];
extern char* extractor_houses\[]\[$extractor_len];

extern int HouseNb\[];
extern int ExtractorNb\[];
extern int DeciderNb\[];

#endif
"

set c_content "char mb_camera\[]\[30] = {${mb(camera)}};
char mb_screen\[]\[30] = {${mb(screen)}};
char mb_db\[]\[$db_len]\[30] = {${mb(db)}};
char mb_decider\[]\[$decider_len]\[30]  = {${mb(decider)}};
char mb_executer\[]\[$executer_len]\[30] = {${mb(executer)}};
char mb_recognizer\[]\[$recognizer_len]\[30] = {${mb(recognizer)}};

int house_recognizer\[] = {$house_recognizer};
int house_decider\[] = {$house_decider};
int house_executer\[] = {$house_executer};
int house_db\[] = {$house_db};
char* extractor_houses\[]\[$extractor_len] = {$extractor_houses};

int HouseNb\[] = {$HouseNb};
int ExtractorNb\[] = {$ExtractorNb};
int DeciderNb\[] = {$DeciderNb};
"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~
set scriptPath [file dirname [file normalize $argv0]]
set outFile [open $scriptPath/genVars.h w+]
puts $outFile $h_content
close $outFile

set outFile [open $scriptPath/genVars.c w+]
puts $outFile $c_content
close $outFile

