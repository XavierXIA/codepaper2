#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../../../sharedSetup.tcl
source $scriptPath/input.tcl

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ variable ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set mobile_index 0
set pc_index     0

array set links ""

set nb(POP) 0
set pop_nbs_list ""
set low_pop_nb 0

foreach n $pop_nbs {
    lappend pop_nbs_list $n
    incr nb(POP) $n
    set low_pop_nb $n
}

set high_pop_nb [expr $nb(POP) - $low_pop_nb]

set top_pop_nb 0
regexp {^([0-9]+)\s} $pop_nbs vv top_pop_nb

set pop_level_nb [llength $pop_nbs_list]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ gen pop connects ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
lappend links(pop-0) "pop-1"
lappend links(pop-0) "pop-2"

set i 0
while {$i < $nb(House)} {
    lappend links(box-$i) "pop-1"
    incr i
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~
set  outFile [GenXml $scriptPath/infra.xml]
puts $outFile "<infra>"

#devices
foreach type "Cloud POP" {
    set i 0
    while {$i < $nb($type)} {
        eval "Add$type $outFile $i"
        incr i
    }
}

#home0
    set idx 0
    AddBox $outFile $idx

    set pc_nb 0
    set i 0
    while {$i < $pc_nb} {
        AddPC $outFile $idx
        incr i
    }

    set mobile_nb 0
    set i 0
    while {$i < $mobile_nb} {
        AddMobile $outFile $idx
        incr i
    }

    AddCamera $outFile $idx
    AddScreen $outFile $idx


#home1
    set idx 1
    AddBox $outFile $idx

    set pc_nb 0
    set i 0
    while {$i < $pc_nb} {
        AddPC $outFile $idx
        incr i
    }

    set mobile_nb 1
    set i 0
    while {$i < $mobile_nb} {
        AddMobile $outFile $idx
        incr i
    }

    AddCamera $outFile $idx
    AddScreen $outFile $idx


#home2
    set idx 2
    AddBox $outFile $idx

    set pc_nb 1
    set i 0
    while {$i < $pc_nb} {
        AddPC $outFile $idx
        incr i
    }

    set mobile_nb 2
    set i 0
    while {$i < $mobile_nb} {
        AddMobile $outFile $idx
        incr i
    }

    AddCamera $outFile $idx
    AddScreen $outFile $idx

#links
set tcl_precision 2

foreach {src dsts} [array get links] {
    foreach dst $dsts {
        set cmd "set l \"$link_type([GetDeviceType $src],[GetDeviceType $dst])\"" 
        eval $cmd
        PutsProperty $outFile link "$src $dst $l"

#       set cmd "set l \"$link_type([GetDeviceType $dst],[GetDeviceType $src])\"" 
#       eval $cmd
        PutsProperty $outFile link "$dst $src $l"
    }
}

puts $outFile "</infra>"

close $outFile

