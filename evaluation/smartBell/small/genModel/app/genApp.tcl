#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../../../sharedSetup.tcl
source $scriptPath/input.tcl

array set configs ""
set binds ""

set user_list ""
set i 0
while {$i < $nb(House)} {
    lappend user_list "$i"
    incr i
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc AddComp {outFile type index} {
    global DZ

    puts $outFile "<comp>"

    PutsProperty $outFile id "app0:$type-$index"

    PutsCompRes $outFile $type

    if {[info exists DZ($type)]} {
        PutsProperty $outFile DZlabel [GetDZ $DZ($type) $type $index]
    }

    puts $outFile "</comp>"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ gen ~~~~~~~~~~~~~~~~~~~~~~~~~~

set appCnt 0
#config
set comp_types_bk $comp_types
foreach type $comp_types_bk {
    set related_comp_types ""
    foreach bindT $bind_label(mapped) {
        if {[regexp "^${type}_(\[a-z]+)$" $bindT vv dst]} {
            lappend related_comp_types $dst
            set idx [lsearch $comp_types_bk $dst]
            set comp_types_bk [lreplace $comp_types_bk $idx $idx]
        }
    }

    set user_sets [split [GetUserSets $user_list $nb(0:$type)] ":"]

    set i 0
    while {$i < $nb(0:$type)} {
        set configs(0:${type}$i) [lindex $user_sets $i]
        
        foreach r_type $related_comp_types {
            set configs(0:${r_type}$i) [lindex $user_sets $i]
        } 

        incr i
    }
}

set configs(0:extractor0) {0 1}
set configs(0:extractor1) {2}
set configs(0:executer0)  {1}
set configs(0:executer1)  {0 2}

#bind
set i 0
foreach src $comp_types {
    set j [expr $i + 1]
    while {$j < [llength $comp_types]} {
        set dst [lindex $comp_types $j]

        if {[info exists bind_type($src,$dst)]} {
            set m 0
            while {$m < $nb(0:$src)} {
                set n 0
                while {$n < $nb(0:$dst)} {
                    if {("${src}_$dst" in $bind_label(complete)) ||
                        ("${src}_$dst" in $bind_label(mapped) && $m == $n) ||
                        ("${src}_$dst" in $bind_label(partial) && [IsIntersected 0 ${src}$m ${dst}$n])} {
                        lappend binds "app0:${src}-$m app0:${dst}-$n $bind_type($src,$dst)"
                        lappend binds "app0:${dst}-$n app0:${src}-$m $bind_type($dst,$src)"
                    }

                    incr n
                }
                incr m
            }
        }
        incr j
    }

    incr i
}

set m 0
while {$m < $nb(0:extractor)} {
    foreach house $configs(0:extractor$m) {
        lappend binds "app0:extractor-$m camera-$house $bind_type(extractor,camera)"
        lappend binds "camera-$house app0:extractor-$m $bind_type(camera,extractor)"
    }

    incr m
}

set m 0
while {$m < $nb(0:executer)} {
    foreach house $configs(0:executer$m) {
        lappend binds "app0:executer-$m screen-$house $bind_type(executer,screen)"
        lappend binds "screen-$house app0:executer-$m $bind_type(screen,executer)"
    }

    incr m
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output app.xml ~~~~~~~~~~~~~~~~~~~~~~~~~~
set  outFile [GenXml $scriptPath/app.xml]
puts $outFile "<apps>"
puts $outFile "<app>"
#PutsProperty $outFile id "doorbell"

foreach type $comp_types {
    set i 0
    while {$i < $nb(0:$type)} {
        AddComp $outFile $type $i
        incr i
    }
}

foreach bind $binds {
    PutsProperty $outFile bind $bind
}

puts $outFile "</app>"
puts $outFile "</apps>"

close $outFile

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output genVars.h ~~~~~~~~~~~~~~~~~~~~~~~~~~
if {$argc > 1 && [lindex $argv 1] eq "noGenVars"} {
    return;
}

set appCnt 1
source $scriptPath/../../../genVars.tcl

