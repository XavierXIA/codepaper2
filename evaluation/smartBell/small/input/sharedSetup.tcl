set infraNB 10

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GetAvgValue {msg} {
    set count 0
    set sum 0

    foreach v $msg {
        set sum [expr $sum + $v]
        incr count
    }

    return [expr $sum/$count]
}

