#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../input/sharedSetup.tcl

set resultNB 100
set testNB 10

set resultsPerSimulate [expr $resultNB/$testNB]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set i 0
while {$i < $infraNB} {
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ init ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    file delete -force $scriptPath/$i
    catch {exec mkdir $i -p} msg

    catch {exec $scriptPath/../genModel/infra/infra.tcl} msg
    if {$msg ne ""} {
        puts $msg
    }
    file copy -force $scriptPath/../genModel/infra/infra.xml $scriptPath/$i/infra.xml
    file copy -force $scriptPath/../genModel/infra/infra.xml $scriptPath/../input/infra${i}.xml

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    set j 0
    while {$j < $resultNB} {
        catch {exec $scriptPath/FirstFit.sh $scriptPath/$i $j} msg

        catch {exec echo $msg | grep finish} isFinish
        if {$isFinish eq "finish"} {
            puts "finish : $i : $j"
            catch {exec echo $msg | grep Score | awk "{print \$3}"} score
            catch {exec echo $msg | grep AL | awk "{print \$3}"} al
            catch {exec echo $msg | grep Diameter | awk "{print \$3}"} diameter

            catch {exec echo $msg | grep execution | awk "{print \$4}"} exeTime
            catch {exec echo "$score $exeTime $j $al $diameter" >> $i/tmp} msg

            incr j
        } else {
            puts $msg
        }
    }

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ simulate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    set res ""
    set sorted ""
    catch {exec cat $i/tmp | sort -k 1n } sorted

    set j 0
    while {$j < $resultNB} {
        if {$j%$resultsPerSimulate == 0} {

            if {[expr $j+$resultsPerSimulate] == $resultNB} {
                set j [expr $resultNB - 1]
            }

            set wal [lindex $sorted [expr $j*5]]
            set exeTime [lindex $sorted [expr $j*5+1]]
            set idx [lindex $sorted [expr $j*5+2]]
            set al [lindex $sorted [expr $j*5+3]]
            set diameter [lindex $sorted [expr $j*5+4]]

            set platform $scriptPath/$i/platform${idx}.xml
            set deploy $scriptPath/$i/deploy${idx}.xml

            catch {exec ../../simulate/build/unit_test $platform $deploy} msg1
            catch {exec ../../simulate/build/extreme_test $platform $deploy} msg2

            set respTime [GetAvgValue "$msg1 $msg2"]
            
            lappend res "$wal,$respTime,$al,$diameter"
            catch {exec echo "$wal" >> $i/wals} msg
            catch {exec echo "$al" >> $i/als} msg
            catch {exec echo "$diameter" >> $i/diameters} msg
            catch {exec echo "$respTime" >> $i/respTimes} msg
            catch {exec echo "$exeTime" >> $i/exeTimes} msg
        }

        incr j
    }

    set outFile [open $i/result.csv w+]
    puts $outFile "wal,respTime,al,diameter"

    foreach line $res {
        puts $outFile $line
    }

    close $outFile

    incr i
}

