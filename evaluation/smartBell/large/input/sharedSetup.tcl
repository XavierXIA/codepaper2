#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GetAvgValue {msg} {
    set count 0
    set sum 0

    foreach v $msg {
        set sum [expr $sum + $v]
        incr count
    }

    return [expr $sum/$count]
}

proc ColorShow {str fg {nonewline 0}} {
    set fgList "30 black 31 red 32 green 33 yellow 34 blue 37 white"
    set fgNo   [lindex $fgList [expr [lsearch $fgList $fg] - 1]]
    set str    "\033\[0;${fgNo}m$str\033\[0m"
    if {$nonewline != 1} {
        puts "$str"
    } else {
        puts -nonewline "$str"
    }
}

