#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../input/sharedSetup.tcl

set appNB 776
set base_respTime 1

set testNB 3
set steps {0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9}
set caps {infinite 10 100 300}

catch {exec echo step,cap,respTime >> $scriptPath/result_simulate.csv}

foreach algo {AFNO DAFNO} {
    foreach step $steps {
        catch {exec mkdir -p $scriptPath/$algo/$step}
    
        foreach cap $caps {
            catch {exec mkdir -p $scriptPath/$algo/$step/$cap}
    
            set i 0
            while {$i < $testNB} {
                set platform $scriptPath/$algo/$step/$cap/platform$i.xml
                set deploy   $scriptPath/$algo/$step/$cap/deploy$i.xml
                
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ simulate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                catch {exec $scriptPath/../../simulate/build/unit_test $platform $deploy} msg1
                catch {exec $scriptPath/../../simulate/build/extreme_test $platform $deploy} msg2
    
                set respTime [GetAvgValue "$msg1 $msg2"]
                puts " respTime : $respTime"
    
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                catch {exec echo "$step,$cap,[expr $respTime/$base_respTime]" >> $scriptPath/result_simulate.csv} msg
            
                incr i
            }
        }
    }
}

