#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../input/sharedSetup.tcl

set appNB 776
set base_wal 1

set testNB 3
set steps {0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9}
set caps {infinite 10 100 300}

catch {exec echo step,cap,wal,exeTime > $scriptPath/result_test.csv}

foreach algo {AFNO DAFNO} {
    foreach step $steps {
        catch {exec mkdir -p $scriptPath/$algo/$step}
    
        foreach cap $caps {
            catch {exec mkdir -p $scriptPath/$algo/$step/$cap}
    
            set i 0
            while {$i < $testNB} {
                set platform $scriptPath/$algo/$step/$cap/platform$i.xml
                set deploy   $scriptPath/$algo/$step/$cap/deploy$i.xml
                
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                set log ""
                if {$cap eq "infinite"} {
                    catch {exec $scriptPath/$algo-InitCO-DCO.sh $appNB $step $platform $deploy} log
                } else {
                    catch {exec $scriptPath/$algo-InitCO-DCO-FailCap.sh $appNB $step $platform $deploy $cap} log
                }
                puts $log
    
                catch {exec echo $log | grep Score | awk "{print \$3}"} wal
                catch {exec echo $log | grep execution | awk "{print \$4}"} exeTime
    
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                catch {exec echo "$step,$cap,[expr $wal/$base_wal],$exeTime" >> $scriptPath/result_test.csv} msg
            
                incr i
            }
        }
    }
}

