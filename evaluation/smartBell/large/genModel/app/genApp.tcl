#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../../../sharedSetup.tcl
source $scriptPath/../sharedSetup.tcl
source $scriptPath/input.tcl

interp recursionlimit {} $nb(House)

set houseCnt 0
set appCnt 0

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc AddComp {outFile type index} {
    global DZ appCnt

    puts $outFile "<comp>"

    PutsProperty $outFile id "app$appCnt:$type-$index"

    PutsCompRes $outFile $type

    if {[info exists DZ($type)]} {
        PutsProperty $outFile DZlabel [GetDZ $DZ($type) $type $index]
    }

    puts $outFile "</comp>"
}


#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output app.xml ~~~~~~~~~~~~~~~~~~~~~~~~~~
set  outFile [GenXml $scriptPath/app.xml]
puts $outFile "<apps>"

array set configs ""
set binds ""

while {$houseCnt < $nb(House)} {
    #init
    set binds ""
    
    eval $cmd_neighbor
    
    set nb($appCnt:recognizer) [expr 1 + round(floor(3*rand()))]
    set nb($appCnt:extractor)  [expr 1 + round(floor(3*rand()))]
    set nb($appCnt:decider)    [expr 1 + round(floor(3*rand()))]
    set nb($appCnt:executer)   [expr 1 + round(floor(3*rand()))]
    set nb($appCnt:recorder)   1

    set restHouseNB [expr $nb(HousePerPOP) - $houseCnt%$nb(HousePerPOP)]
    if {$restHouseNB <= 5} {
        set nb(neighbor) $restHouseNB
    } else {
        while {$restHouseNB - $nb(neighbor) < 3} {
            eval $cmd_neighbor
        }
    }
    set nb($appCnt:db) $nb(neighbor)

    set user_list ""
    set i 0
    while {$i < $nb(neighbor)} {
        lappend user_list "$houseCnt"
        incr houseCnt
        incr i
    }

    #config
    set comp_types_bk $comp_types
    foreach type $comp_types_bk {
        set related_comp_types ""
        foreach bindT $bind_label(mapped) {
            if {[regexp "^${type}_(\[a-z]+)$" $bindT vv dst]} {
                lappend related_comp_types $dst
                set idx [lsearch $comp_types_bk $dst]
                set comp_types_bk [lreplace $comp_types_bk $idx $idx]
            }
        }
    
        set user_sets [split [GetUserSets $user_list $nb($appCnt:$type)] ":"]
    
        set i 0
        while {$i < $nb($appCnt:$type)} {
            set configs($appCnt:${type}$i) [lindex $user_sets $i]
            
            foreach r_type $related_comp_types {
                set configs($appCnt:${r_type}$i) [lindex $user_sets $i]
            } 
    
            incr i
        }
    }
    
    #bind
    set i 0
    foreach src $comp_types {
        set j [expr $i + 1]
        while {$j < [llength $comp_types]} {
            set dst [lindex $comp_types $j]
    
            if {[info exists bind_type($src,$dst)]} {
                set m 0
                while {$m < $nb($appCnt:$src)} {
                    set n 0
                    while {$n < $nb($appCnt:$dst)} {
                        if {("${src}_$dst" in $bind_label(complete)) ||
                            ("${src}_$dst" in $bind_label(mapped) && $m == $n) ||
                            ("${src}_$dst" in $bind_label(partial) && [IsIntersected $appCnt ${src}$m ${dst}$n])} {
                            lappend binds "app$appCnt:${src}-$m app$appCnt:${dst}-$n $bind_type($src,$dst)"
                            lappend binds "app$appCnt:${dst}-$n app$appCnt:${src}-$m $bind_type($dst,$src)"
                        }
    
                        incr n
                    }
                    incr m
                }
            }
            incr j
        }
    
        incr i
    }
    
    set m 0
    while {$m < $nb($appCnt:extractor)} {
        foreach house $configs($appCnt:extractor$m) {
            lappend binds "app$appCnt:extractor-$m camera-$house $bind_type(extractor,camera)"
            lappend binds "camera-$house app$appCnt:extractor-$m $bind_type(camera,extractor)"
        }
    
        incr m
    }
    
    set m 0
    while {$m < $nb($appCnt:executer)} {
        foreach house $configs($appCnt:executer$m) {
            lappend binds "app$appCnt:executer-$m screen-$house $bind_type(executer,screen)"
            lappend binds "screen-$house app$appCnt:executer-$m $bind_type(screen,executer)"
        }
    
        incr m
    }

    #output
    puts $outFile "<app>"
    #PutsProperty $outFile id "doorbell"
    
    foreach type $comp_types {
        set i 0
        while {$i < $nb($appCnt:$type)} {
            AddComp $outFile $type $i
            incr i
        }
    }
    
    foreach bind $binds {
        PutsProperty $outFile bind $bind
    }
    
    puts $outFile "</app>"

    incr appCnt
}

puts $outFile "</apps>"
close $outFile

puts "$appCnt applications generated"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output genVars.h ~~~~~~~~~~~~~~~~~~~~~~~~~~
#if {$argc > 1 && [lindex $argv 1] eq "noGenVars"} {
#    return;
#}
#
source $scriptPath/../../../genVars.tcl


#~~~~~~~~~~~~~~~~~~~~~~~~~~~ resort apps ~~~~~~~~~~~~~~~~~~~~~~~~~~
set inFile [open $scriptPath/app.xml r]

set apps ""

while {![eof $inFile]} {
    gets $inFile line

    if {$line eq "<app>"} {
        set app $line
        while {$line ne "</app>"} {
            gets $inFile line
            lappend app $line
        }

        lappend apps $app
    }
}

close $inFile

#output
set  outFile [GenXml $scriptPath/app.xml]
puts $outFile "<apps>"

set restApp_list ""
set i 0
while {$i < [llength $apps]} {
    lappend restApp_list $i
    incr i
}

while {[llength $restApp_list] != 0} {
    set index [expr round(floor([llength $restApp_list]*rand()))]
    set appIdx [lindex $restApp_list $index]
    set restApp_list [lreplace $restApp_list $index $index]

    foreach line [lindex $apps $appIdx] {
        puts $outFile $line
    }
}

puts $outFile "</apps>"
close $outFile

