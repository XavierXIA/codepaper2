#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set addDevNB [lindex $argv 0]

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../../../sharedSetup.tcl
source $scriptPath/../sharedSetup.tcl
source $scriptPath/input.tcl

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ variable ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set mobile_index 0
set pc_index     0

array set links ""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ pop connect ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set level 0
set pop_min 0
set pop_max 0
set connect_max 0
set connect_index 0

foreach n $pop_nbs {
    set nextLevel [expr $level+1]

    set pop_min $pop_max
    set pop_max [expr $pop_min + $n]

    #connect to same level
    set connect_index [expr $pop_min+1]
    while {$connect_index < $pop_max} {
        set proba $proba_pop2sameLevel
        set contain_list ""

        while {rand() < $proba} {
            set amount [expr $connect_index-$pop_min]
            set index [expr $pop_min + round(floor($amount * rand()))]
            while {$index in $contain_list} {
                set index [expr $pop_min + round(floor($amount * rand()))]
            }
            lappend contain_list $index
   
            lappend links(pop-$index) "pop-$connect_index"
           
            if {[llength $contain_list] == $amount} {
                set proba 0
            } else {
                set proba [expr $proba/6]
            }
        }
   
        incr connect_index
    }

    if {$nextLevel < $pop_level_nb} {
        set connect_max [expr $pop_max + [lindex $pop_nbs_list $nextLevel]]

        #connect to next level
        set connect_index $pop_max
        while {$connect_index < $connect_max} {
            set proba $proba_pop2nextLevel
            set contain_list ""

            while {rand() < $proba} {
                set index [expr $pop_min + round(floor($n * rand()))]
                while {$index in $contain_list} {
                    set index [expr $pop_min + round(floor($n * rand()))]
                }
                lappend contain_list $index
   
                lappend links(pop-$index) "pop-$connect_index"
               
                if {[llength $contain_list] == $n} {
                    set proba 0
                } else {
                    set proba [expr $proba/6]
                }
            }
   
            incr connect_index
        }
    }

    incr level
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~
set  outFile [GenXml $scriptPath/infra.xml]
puts $outFile "<infra>"

#devices
foreach type "Cloud POP" {
    set i 0
    while {$i < $nb($type)} {
        eval "Add$type $outFile $i"
        incr i
    }
}


set i 0
while {$i < $low_pop_nb} {
    set popIdx [expr $i + $high_pop_nb]

    set j 0
    while {$j < $nb(HousePerPOP)} {
        set houseIdx [expr $nb(HousePerPOP)*$i + $j]
        AddHouse $outFile $houseIdx [expr round(floor(4 * rand()))]
        lappend links(box-$houseIdx) "pop-$popIdx"
    
        incr j
    }

    incr i
}

#links
foreach {src dsts} [array get links] {
    foreach dst $dsts {
        set cmd "set l \"$link_type([GetDeviceType $src],[GetDeviceType $dst])\"" 
        eval $cmd
        PutsProperty $outFile link "$src $dst $l"

#       set cmd "set l \"$link_type([GetDeviceType $dst],[GetDeviceType $src])\"" 
#       eval $cmd
        PutsProperty $outFile link "$dst $src $l"
    }
}

puts $outFile "</infra>"

close $outFile

