set nb(House) 6000

set pop_nbs "4 20"
set pop_level_nb 2
foreach {high_pop_nb low_pop_nb} $pop_nbs {}

set nb(POP) 0
set pop_nbs_list ""
foreach n $pop_nbs {
    lappend pop_nbs_list $n
    incr nb(POP) $n
}

set nb(HousePerPOP) [expr $nb(House)/$low_pop_nb]

