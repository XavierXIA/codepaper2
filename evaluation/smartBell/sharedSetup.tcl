#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ infra proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc AddFogNode {outFile dev_type index} {
    global res unit

    puts $outFile "<fogNode>"

    PutsProperty $outFile id "$dev_type-$index"
    
    foreach res_type "CPU RAM DISK" {
        set cmd "set c $res($dev_type,$res_type)$unit($res_type)"
        eval $cmd

        PutsProperty $outFile $res_type $c
    }

}

proc AddCloud {outFile idx} {
    global high_pop_nb links

    AddFogNode $outFile "cloud" $idx
    puts $outFile "</fogNode>"

    set i 0
    while {$i < $high_pop_nb} {
        lappend links(cloud-$idx) "pop-$i"
        incr i
    }
}

proc AddPOP {outFile idx} {
    AddFogNode $outFile "pop" $idx
    puts $outFile "</fogNode>"
}

proc AddMobileOut {outFile idx} {
    AddMobile $outFile ""
}

proc AddMobile {outFile house} {
    global mobile_index links pop_level_nb

    AddFogNode $outFile "mobile" $mobile_index

    if {$house ne ""} {
        PutsProperty $outFile "DZlabel" "house-$house"
        lappend links(mobile-$mobile_index) "box-$house"
    } else {
        lappend links(mobile-$mobile_index) [GetRandomBS]
    }

    puts $outFile "</fogNode>"

    incr mobile_index
}

proc AddHouse {outFile idx nb} {
    set i  0

    while {$i < $nb} {
        if {rand() < 0.5} {
            AddPC $outFile $idx
        } else {
            AddMobile $outFile $idx
        }
        incr i
    }

    AddBox $outFile $idx
    AddCamera $outFile $idx
    AddScreen $outFile $idx
}

proc AddBox {outFile house} {
    global links pop_level_nb

    AddFogNode $outFile "box" $house
    PutsProperty $outFile "DZlabel" "house-$house"
    puts $outFile "</fogNode>"
}

proc AddPC {outFile house} {
    global pc_index links pop_level_nb

    AddFogNode $outFile "pc" $pc_index
    PutsProperty $outFile "DZlabel" "house-$house"
    puts $outFile "</fogNode>"

    lappend links(pc-$pc_index) "box-$house"

    incr pc_index
}

proc AddAppliance {outFile type index} {
    global pop_level_nb

    puts $outFile "<appliance>"

    PutsProperty $outFile id "$type-$index"

    puts $outFile "</appliance>"
}

proc AddCamera {outFile house} {
    global links

    AddAppliance $outFile "camera" $house

    lappend links(camera-$house) "box-$house"
}

proc AddScreen {outFile house} {
    global links

    AddAppliance $outFile "screen" $house

    lappend links(screen-$house) "box-$house"
}


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ infra proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc PutsCompRes {outFile type} {
    global res

    foreach {cpu ram disk} $res($type) {
        PutsProperty $outFile CPU  $cpu
        PutsProperty $outFile RAM  $ram
        PutsProperty $outFile DISK $disk
    }
}

proc GetDZ {str type index} {
    global configs appCnt

    set idx [string first "env_user" $str]
    if {$idx >= 0} {
        return [GetDZ [string replace $str $idx [expr $idx+7] "$configs($appCnt:${type}$index)"] $type $index]
    } else {
        return $str
    }
}

proc IsIntersected {appCnt a b} {
    global configs

    foreach i $configs($appCnt:$a) {
        if {$i in $configs($appCnt:$b)} {
            return 1
        }
    }

    return 0
}

proc GetUserSets {user_list comp_nb} {
    set res ""

    set range [expr round([llength $user_list]/$comp_nb)]

    set i 0
    while {$i < $range} {
        set res "$res [lindex $user_list $i]"
        incr i
    }
    set res [string trim $res " "]

    if {$comp_nb > 1} {
        return "$res:[GetUserSets [lrange $user_list $range end] [expr $comp_nb - 1]]"
    }
    return "$res"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ common proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GetDeviceType {id} {
    regexp {^([a-zA-Z]+)-[0-9]+$} $id vv type

    return $type
}

proc GetDeviceIdx {id} {
    regexp {^[a-zA-Z]+-([0-9]+)$} $id vv idx

    return $idx
}

proc GetDataUnitSize {reqBW} {
    return [expr (0.01+0.09*rand()) * $reqBW]
}

proc GetDataUnitCalAmount {reqCPU} {
    return [expr (0.01+0.09*rand()) * $reqCPU]
}

proc GenXml {fileName} {
    set path [file dirname [file normalize $fileName]]
    catch {exec mkdir -p $path} msg

    set outFile [open $fileName w+]
    puts $outFile "<?xml version=\"1.0\"?>"

    return $outFile
}

proc PutsProperty {outFile property value} {
    puts $outFile "  <$property>$value</$property>"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~fogNode type res~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set res(cloud,CPU) 999999
set res(cloud,RAM)   999999
set res(cloud,DISK)  999999

set res(pop,CPU)    "\[expr 100  * rand()]"
set res(pop,RAM)      "\[expr 500  * rand()]"
set res(pop,DISK)     "\[expr 5000 * rand()]"

set res(box,CPU)    "\[expr 0.1 + 0.9  * rand()]"
set res(box,RAM)      "\[expr 0.1 + 0.9  * rand()]"
set res(box,DISK)     "\[expr 0.1 + 99.9 * rand()]"

set res(pc,CPU)     "\[expr 2   * rand()]"
set res(pc,RAM)       "\[expr 4   * rand()]"
set res(pc,DISK)      "\[expr 200 * rand()]"

set res(mobile,CPU) "\[expr 1  * rand()]"
set res(mobile,RAM)   "\[expr 2  * rand()]"
set res(mobile,DISK)  "\[expr 50 * rand()]"

set unit(CPU) "Gf"
set unit(RAM)   "GB"
set unit(DISK)  "GB"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ link ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set link_type(cloud,pop)  "\[expr 1000 * rand()]MBps \[expr 30+70*rand()]ms"
set link_type(pop,cloud)  "\[expr 1000 * rand()]MBps \[expr 30+70*rand()]ms"

set link_type(pop,pop)    "\[expr 5000 * rand()]MBps \[expr 3+4*rand()]ms"


set link_type(box,pop)    "\[expr 1 + 99 * rand()]MBps \[expr 1+19*rand()]ms"
set link_type(pop,box)    "\[expr 1 + 99 * rand()]MBps \[expr 1+19*rand()]ms"

set link_type(pc,box)     "\[expr 1000 * rand()]MBps \[expr 1+rand()]ms"
set link_type(box,pc)     "\[expr 1000 * rand()]MBps \[expr 1+rand()]ms"

set link_type(mobile,box) "\[expr 1000 * rand()]MBps \[expr 1+rand()]ms"
set link_type(box,mobile) "\[expr 1000 * rand()]MBps \[expr 1+rand()]ms"

set link_type(screen,box) "1MBps \[expr 1+rand()]ms"
set link_type(box,screen) "1MBps \[expr 1+rand()]ms"

set link_type(camera,box) "1MBps \[expr 1+rand()]ms"
set link_type(box,camera) "1MBps \[expr 1+rand()]ms"


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~comp type res~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set res(db)         "0.1Gf 0.1GB 0.1GB"
set res(recorder)   "0.5Gf 0.5GB 20GB"
set res(extractor)  "0.2Gf 0.2GB 0GB"
set res(recognizer) "0.3Gf 0.3GB 0GB"
set res(decider)    "0.2Gf 0.1GB 0GB"
set res(executer)   "0.1Gf 0.2GB 0GB"

set comp_types {"db" "decider" "extractor" "executer" "recognizer" "recorder"}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~bind type~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set bind_type(camera,extractor)     "0.60MBps 25ms"
set bind_type(extractor,camera)     "0.01MBps 25ms"

set bind_type(screen,executer)      "0.01MBps 25ms"
set bind_type(executer,screen)      "0.01MBps 25ms"

set bind_type(extractor,recognizer) "0.10MBps 25ms"
set bind_type(recognizer,extractor) "0.01MBps 25ms"

set bind_type(db,recognizer)        "0.30MBps 25ms"
set bind_type(recognizer,db)        "0.01MBps 25ms"

set bind_type(decider,recorder)     "0.20MBps 25ms"
set bind_type(recorder,decider)     "0.01MBps 25ms"

set bind_type(extractor,decider)    "0.10MBps 50ms"
set bind_type(decider,extractor)    "0.01MBps 50ms"

set bind_type(decider,executer)     "0.01MBps 50ms"
set bind_type(executer,decider)     "0.01MBps 50ms"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set bind_label(partial)  {"db_recognizer" "decider_extractor" "decider_executer"}
set bind_label(mapped)   {}
set bind_label(complete) {"extractor_recognizer" "decider_recorder"}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~mapping model~~~~~~~~~~~~~~~~~~~~~~~~~~
set DZ(db) "house-env_user"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ tcl ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set tcl_precision 3

set nb(Cloud) 1
