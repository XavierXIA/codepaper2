#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(DECIDER,"DECIDER");

static int Decide(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid decider index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    msg_task_t task_decide = (msg_task_t) MSG_process_get_data(MSG_process_self());
    CameraData data = *(CameraData *) task_decide->data;

    MSG_task_execute(task_decide);

    if(data.type == Stranger)
    {
        msg_task_t task2recorder = MSG_task_create("recorde", Cal_Recorde, Comm_Face, NULL);

        char* mb_send = GetDecide2RecordMB(app, idx, 0);
        MSG_task_send_bounded(task2recorder, mb_send, BW_decider2recorder);
        xbt_free(mb_send);

        //recorder response
        char* mb_rec = GetRecord2DecideMB(app, 0, idx);
        msg_task_t task_response = NULL;
        
        //MSG_sem_acquire(xbt_dynar_get_as(sems_deciderRec, GetDeciderRecSemIdx(idx, 0), msg_sem_t));
        MSG_task_receive_bounded(&task_response, mb_rec, BW_recorder2decider);
        //MSG_sem_release(xbt_dynar_get_as(sems_deciderRec, GetDeciderRecSemIdx(idx, 0), msg_sem_t));
        xbt_free(mb_rec);

        MSG_task_execute(task_response);

        MSG_task_destroy(task_response);
    }

    msg_task_t task2executer = MSG_task_create("execute", Cal_Execute, Comm_Min, NULL);
    task2executer->data = task_decide->data;
    MSG_task_destroy(task_decide);
    MSG_task_send_bounded(task2executer, mb_executer[app][house_executer[data.house]], BW_decider2executer);

}

int Decider(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid decider index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    while(1)
    {
        msg_task_t task_decide = NULL;
        MSG_task_receive_bounded(&task_decide, mb_decider[app][idx], BW_extractor2decider);

        //?
        const char *prName = "decide";
        char** prArgv = xbt_new(char *, 3);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = bprintf("%d", idx);
        prArgv[2] = bprintf("%d", app);

        msg_process_t p = MSG_process_create_with_arguments(prName, Decide, task_decide, MSG_host_self(), 3, prArgv);
    }
}

