#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(EXTRACTOR,"EXTRACTOR");

static int AskNeighbor(int argc, char *argv[])
{
    int idx   = xbt_str_parse_int(argv[1], "Invalid extractor index: %s");
    int house = xbt_str_parse_int(argv[2], "Invalid house index: %s");
    int src   = xbt_str_parse_int(argv[3], "Invalid src: %s");
    int type  = xbt_str_parse_int(argv[4], "Invalid type: %s");
    int app   = xbt_str_parse_int(argv[5], "Invalid app index: %s");

    msg_task_t task2recognizer = MSG_task_create("face", Cal_Min, Comm_Face, NULL);

    RecognizeData* data = xbt_new(RecognizeData, 1);
    data->house = house;
    data->src   = src;
    data->dst   = house_db[house];
    data->type  = type;
    task2recognizer->data = data;

    char* mb_send = GetDetect2RecogMB(app, idx, house_recognizer[house]);

    #ifdef DEBUG
        XBT_INFO("extractor%d -> recognizer%d retrieve H%d, request from Idx%d", idx, house_recognizer[house], house, src);
    #endif
    MSG_task_send_bounded(task2recognizer, mb_send, BW_extractor2recognizer);
    xbt_free(mb_send);

    msg_task_t task_rec = NULL;
    char *mb_rec = GetRecog2DetectMB(app, house_recognizer[house], idx);
    
    //MSG_sem_acquire(xbt_dynar_get_as(sems_extractorRec, GetExtractorRecSemIdx(idx, house_recognizer[house]), msg_sem_t));
    MSG_task_receive_bounded(&task_rec, mb_rec, BW_recognizer2extractor);
    //MSG_sem_release(xbt_dynar_get_as(sems_extractorRec, GetExtractorRecSemIdx(idx, house_recognizer[house]), msg_sem_t));
    xbt_free(mb_rec);

    MSG_task_execute(task_rec);
    MSG_task_destroy(task_rec);

    msg_sem_t semAskAll = *(msg_sem_t *) MSG_process_get_data(MSG_process_self());
    MSG_sem_release(semAskAll);
}

static int AskAllNeighbors(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid extractor index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    CameraData data = *(CameraData *) MSG_process_get_data(MSG_process_self());

    int neighborNb = HouseNb[app]-1;

    msg_sem_t* semAskAll_ptr = xbt_new(msg_sem_t, 1);
    * semAskAll_ptr = MSG_sem_init(neighborNb);

    int houseBase = data.house - house_db[data.house];
    for(int i=0; i<HouseNb[app]; i++) //TODO : add neighbors
    {
        if(i != house_db[data.house])
        {
            const char *prName = "askNeighbor";
            char** prArgv = xbt_new(char *, 6);
            prArgv[0] = xbt_strdup(prName);
            prArgv[1] = bprintf("%d", idx);
            prArgv[2] = bprintf("%d", houseBase + i);
            prArgv[3] = bprintf("%d", house_db[data.house]);
            prArgv[4] = bprintf("%d", data.type);
            prArgv[5] = bprintf("%d", app);
            
            MSG_sem_acquire(*semAskAll_ptr);
            msg_process_t p = MSG_process_create_with_arguments(prName, AskNeighbor, semAskAll_ptr, MSG_host_self(), 6, prArgv);
        }
    }

    if(data.type == Stranger)
    {
        for(int j=0; j<neighborNb; j++)
        {
            MSG_sem_acquire(*semAskAll_ptr);
        }

        //finish detection and send to decider
        msg_task_t task2decider = MSG_task_create("decide", Cal_Decide, Comm_Face, NULL);
        task2decider->data = MSG_process_get_data(MSG_process_self());
        MSG_task_send_bounded(task2decider, mb_decider[app][house_decider[data.house]], BW_extractor2decider);

    }
    else
    {
        bool found = false;

        for(int j=0; j<neighborNb; j++)
        {
            MSG_sem_acquire(*semAskAll_ptr);

            if(!found && rand()%(neighborNb-j)<1)
            {
                found = true;

                msg_task_t task2decider = MSG_task_create("decide", Cal_Decide, Comm_Min, NULL);
                task2decider->data = MSG_process_get_data(MSG_process_self());
                MSG_task_send_bounded(task2decider, mb_decider[app][house_decider[data.house]], BW_extractor2decider);
            }
        }
    }

    xbt_free(semAskAll_ptr);
}

static int Detect(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid extractor index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    msg_task_t task_detect = (msg_task_t) MSG_process_get_data(MSG_process_self());

    //position face
    MSG_task_execute(task_detect);

    CameraData data = *(CameraData *) task_detect->data;

    int house = data.house;

    if(data.type != None)
    {
        //ask house_self recognizer
        msg_task_t task2recognizer = MSG_task_create("face", Cal_Min, Comm_Face, NULL);

        RecognizeData* t_data = xbt_new(RecognizeData, 1);
        t_data->house = house;
        t_data->src   = house_db[house];
        t_data->dst   = house_db[house];
        t_data->type  = data.type;
        task2recognizer->data = t_data;

        char* mb_send = GetDetect2RecogMB(app, idx, house_recognizer[house]);

        #ifdef DEBUG
            XBT_INFO("extractor%d -> recognizer%d retrieve DB%d, request from H%d", idx, house_recognizer[house], house, house);
        #endif
        MSG_task_send_bounded(task2recognizer, mb_send, BW_extractor2recognizer);
        xbt_free(mb_send);

        char* mb_rec = GetRecog2DetectMB(app, house_recognizer[house], idx);
        msg_task_t task_rec = NULL;
        //MSG_sem_acquire(xbt_dynar_get_as(sems_extractorRec, GetExtractorRecSemIdx(idx, house_recognizer[house]), msg_sem_t));
        MSG_task_receive_bounded(&task_rec, mb_rec, BW_recognizer2extractor);
        //MSG_sem_release(xbt_dynar_get_as(sems_extractorRec, GetExtractorRecSemIdx(idx, house_recognizer[house]), msg_sem_t));
        xbt_free(mb_rec);

        MSG_task_execute(task_rec);

        MSG_task_destroy(task_rec);

        if(data.type != Friend && HouseNb[app]>1)
        {
            //ask neighbor recognizer
            const char *prName = "askNeighbors";
            char** prArgv = xbt_new(char *, 3);
            prArgv[0] = xbt_strdup(prName);
            prArgv[1] = bprintf("%d", idx);
            prArgv[2] = bprintf("%d", app);

            msg_process_t p = MSG_process_create_with_arguments(prName, AskAllNeighbors, task_detect->data, MSG_host_self(), 3, prArgv);
        }
        else
        {
            //finish detection and send to decider
            msg_task_t task2decider = MSG_task_create("decide", Cal_Decide, Comm_Min, NULL);
            task2decider->data = task_detect->data;
            MSG_task_send_bounded(task2decider, mb_decider[app][house_decider[house]], BW_extractor2decider);
        }
    }

    MSG_task_destroy(task_detect);
    return 0;
}

static int Listen2camera(int argc, char *argv[])
{
    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    int * p_data = (int *) MSG_process_get_data(MSG_process_self());
    int idx   = p_data[0];
    int house = p_data[1];
    int app   = p_data[2];
    xbt_free(MSG_process_get_data(MSG_process_self()));

    while(1)
    {
        msg_task_t task_detect = NULL;
        MSG_task_receive_bounded(&task_detect, mb_camera[house], BW_camera2extractor);

        //?
        const char *prName = "detect";
        char** prArgv = xbt_new(char *, 3);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = bprintf("%d", idx);
        prArgv[2] = bprintf("%d", app);

        msg_process_t p = MSG_process_create_with_arguments(prName, Detect, task_detect, MSG_host_self(), 3, prArgv);
    }
}

int GenExtractor(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid extractor index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    xbt_dynar_t houseList = xbt_str_split(extractor_houses[app][idx], NULL);

    char* houseStr;
    int i;
    xbt_dynar_foreach(houseList, i, houseStr)
    {
        int houseIdx = xbt_str_parse_int(houseStr, "Invalid house index: %s");

        int* pr = xbt_new(int, 3);
        pr[0] = idx;
        pr[1] = houseIdx;
        pr[2] = app;
        msg_process_t p = MSG_process_create("listen", Listen2camera, pr, MSG_host_self());
    }

    xbt_dynar_free(&houseList);
}

