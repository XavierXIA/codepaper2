#ifndef _GLOBAL_H__
#define _GLOBAL_H__

#include "simgrid/msg.h"
#include "genVars.h"
#include "utils.h"

//#define DEBUG

typedef enum  
{
    None = 0, Friend = 1, NeighborFriend = 2, Stranger = 3
} VisitorType;

typedef struct
{
    int house;
    int index;
    VisitorType type;
    double sendTime;
} CameraData;

typedef struct
{
    int house;
    int src;
    int dst;
    VisitorType type;
} RecognizeData;

typedef int bool;
#define true 1
#define false 0

#define VisitorTypeNb 4

//----------------------------------------------------------------------------

extern int vmIdx;
extern xbt_dynar_t vmList;
 
//extern xbt_dynar_t sems_extractor;
//extern xbt_dynar_t sems_recognizer;
//extern xbt_dynar_t sems_decider;
//extern xbt_dynar_t sems_recorder;
//extern xbt_dynar_t sems_executer;
//extern xbt_dynar_t sems_db;
//extern xbt_dynar_t sems_extractorRec;
//extern xbt_dynar_t sems_deciderRec;

#endif

