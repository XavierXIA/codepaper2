#include "global.h"

//int GetExtractorRecSemIdx(int extractor, int recognizer)
//{
//    return recognizer * ExtractorNb + extractor;
//}
//
//int GetDeciderRecSemIdx(int decider, int recorder)
//{
//    return recorder * DeciderNb + decider;
//}

void WaitAll(xbt_dynar_t comms)
{
    //wait until all response received
    while(!xbt_dynar_is_empty(comms))
    {
        msg_comm_t comm_task = NULL;
        xbt_dynar_remove_at(comms, MSG_comm_waitany(comms), &comm_task);

        MSG_comm_destroy(comm_task);
    }

    xbt_dynar_free(&comms);
}

char* GetDetect2RecogMB(int app, int detect, int recog)
{
    return bprintf("%d,dt%d,rz%d", app, detect, recog);
}

char* GetRecog2DetectMB(int app, int recog, int detect)
{
    return bprintf("%d,rz%d,dt%d", app, recog, detect);
}

char* GetDecide2RecordMB(int app, int decide, int record)
{
    return bprintf("%d,dc%d,rd%d", app, decide, record);
}

char* GetRecord2DecideMB(int app, int record, int decide)
{
    return bprintf("%d,rd%d,dc%d", app, record, decide);
}

void FreeSemDynar(xbt_dynar_t sems)
{
    int i = xbt_dynar_length(sems) - 1;

    while(!xbt_dynar_is_empty(sems))
    {
        msg_sem_t sem = NULL;
        xbt_dynar_remove_at(sems, i, &sem);

        xbt_free(sem);
        i--;
    }

    xbt_dynar_free(&sems);
}

