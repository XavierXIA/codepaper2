#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(RECOGNIZER,"RECOGNIZER");

static int Recognize(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid recognizer index: %s");
    int extractor = xbt_str_parse_int(argv[2], "Invalid extractor index: %s");
    int app = xbt_str_parse_int(argv[3], "Invalid app index: %s");

    msg_task_t task_recognize = (msg_task_t) MSG_process_get_data(MSG_process_self());
    RecognizeData data = *(RecognizeData *) task_recognize->data;

    int dst = data.dst;
    bool canFind = (data.src==data.dst && data.type==Friend) || (data.src!=data.dst && data.type==NeighborFriend);

    //execute extractor request
    MSG_task_execute(task_recognize);

    MSG_task_destroy(task_recognize);

    //first msg
    msg_task_t task_request = MSG_task_create("db", Cal_DB, Comm_Min, NULL);
    task_request->data = & data;
    MSG_task_send_bounded(task_request, mb_db[app][dst], BW_recognizer2db);

    //recognizing process
    xbt_dynar_t comms = xbt_dynar_new(sizeof(msg_comm_t), NULL);
    bool found = false;

    int i;
    for(i=0; !found && i<DB_PartNb; i++)
    {
        msg_task_t task_rec = NULL;
        MSG_task_receive_bounded(&task_rec, mb_recognizer[app][idx], BW_db2recognizer);

        if(i < DB_PartNb-1)
        {
            task_request = MSG_task_create("db", Cal_DB, Comm_Min, NULL);
            task_request->data = & data;
            msg_comm_t comm = MSG_task_isend_bounded(task_request, mb_db[app][dst], BW_recognizer2db);
            xbt_dynar_push_as(comms, msg_comm_t, comm);
        }

        MSG_task_execute(task_rec);
        MSG_task_destroy(task_rec);

        found = canFind && (i >= DB_PartNb/2);
    }

    //xbt_free(task_recognize->data);

    #ifdef DEBUG
        //XBT_INFO("from %d, found at %d", data.src, i);
    #endif

    //finish recognizing, respond to extractor
    char* mb = GetRecog2DetectMB(app, idx, extractor);
    msg_task_t task2extractor = MSG_task_create("face", Cal_Min, Comm_Min, NULL);
    MSG_task_send_bounded(task2extractor, mb, BW_recognizer2extractor);
    xbt_free(mb);

    //receive last msg if needed
    if(i < DB_PartNb)
    {
        msg_task_t task_rec = NULL;
        MSG_task_receive_bounded(&task_rec, mb_recognizer[app][idx], BW_db2recognizer);
        MSG_task_destroy(task_rec);
    }

    //wait all isending to be finished
    WaitAll(comms);
}

static int Listen2extractor(int argc, char *argv[])
{
    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    int * p_data = (int *) MSG_process_get_data(MSG_process_self());
    int idx = p_data[0];
    int extractor = p_data[1];
    int app = p_data[2];
    xbt_free(MSG_process_get_data(MSG_process_self()));

    char* mb = GetDetect2RecogMB(app, extractor, idx);
    while(1)
    {
        msg_task_t task_recognize = NULL;
        MSG_task_receive_bounded(&task_recognize, mb, BW_extractor2recognizer);

        #ifdef DEBUG
            XBT_INFO("recognizer%d <- extractor%d", idx, extractor);
        #endif

        //?
        const char *prName = "recognize";
        char** prArgv = xbt_new(char *, 4);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = bprintf("%d", idx);
        prArgv[2] = bprintf("%d", extractor);
        prArgv[3] = bprintf("%d", app);

        msg_process_t p = MSG_process_create_with_arguments(prName, Recognize, task_recognize, MSG_host_self(), 4, prArgv);
    }
}

int GenRecognizer(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid recognizer index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    //for(int i=0; i<ExtractorNb; i++)
    //{
    //    msg_sem_t* sem = xbt_new(msg_sem_t, 1);
    //    * sem = MSG_sem_init(1);
    //    xbt_dynar_set(sems_extractorRec, idx*ExtractorNb+i, sem);
    //}

    for(int i=0; i<ExtractorNb[app]; i++)
    {
        int* pr = xbt_new(int, 3);
        pr[0] = idx;
        pr[1] = i;
        pr[2] = app;

        msg_process_t p = MSG_process_create("listen", Listen2extractor, pr, MSG_host_self());
    }
}

