#include "global.h"

static int Recorde(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid recorder index: %s");
    int decider = xbt_str_parse_int(argv[2], "Invalid decider index: %s");
    int app = xbt_str_parse_int(argv[3], "Invalid app index: %s");

    msg_task_t task_recorde = (msg_task_t) MSG_process_get_data(MSG_process_self());

    MSG_task_execute(task_recorde);

    MSG_task_destroy(task_recorde);

    char* mb = GetRecord2DecideMB(app, idx, decider);
    msg_task_t task_response = MSG_task_create("count", Cal_Min, Comm_Min, NULL);
    MSG_task_send_bounded(task_response, mb, BW_recorder2decider);
    xbt_free(mb);

}

static int Listen2decider(int argc, char *argv[])
{
    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    int * p_data = (int *) MSG_process_get_data(MSG_process_self());
    int idx = p_data[0];
    int decider = p_data[1];
    int app = p_data[2];
    xbt_free(MSG_process_get_data(MSG_process_self()));

    char* mb = GetDecide2RecordMB(app, decider, idx);
    while(1)
    {
        msg_task_t task_record = NULL;
        MSG_task_receive_bounded(&task_record, mb, BW_decider2recorder);

        //?
        const char *prName = "recognize";
        char** prArgv = xbt_new(char *, 4);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = bprintf("%d", idx);
        prArgv[2] = bprintf("%d", decider);
        prArgv[3] = bprintf("%d", app);

        msg_process_t p = MSG_process_create_with_arguments(prName, Recorde, task_record, MSG_host_self(), 4, prArgv);
    }
}

int GenRecorder(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid recorder index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    //for(int i=0; i<DeciderNb; i++)
    //{
    //    msg_sem_t* sem = xbt_new(msg_sem_t, 1);
    //    * sem = MSG_sem_init(1);
    //    xbt_dynar_set(sems_deciderRec, idx*DeciderNb+i, sem);
    //}

    for(int i=0; i<DeciderNb[app]; i++)
    {
        int* pr = xbt_new(int, 3);
        pr[0] = idx;
        pr[1] = i;
        pr[2] = app;

        msg_process_t p = MSG_process_create("listen", Listen2decider, pr, MSG_host_self());
    }
}

