#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(CAMERA_EXTREME,"CAMERA_EXTREME");

int Camera(int argc, char *argv[])
{
    int house = xbt_str_parse_int(argv[1], "Invalid house: %s");

    xbt_dynar_t comms = xbt_dynar_new(sizeof(msg_comm_t), NULL);

    for(int msgIdx=0; msgIdx < VisitorTypeNb * TestCount; msgIdx++)
    {
        msg_task_t task2extractor = MSG_task_create("detect", Cal_Extract, Comm_CameraImg, NULL);

        double time = MSG_get_clock();
        CameraData* data = xbt_new(CameraData, 1);
        data->sendTime = time;
        data->house = house;
        data->index = msgIdx;
        data->type  = msgIdx % VisitorTypeNb;

        task2extractor->data = data;

        msg_comm_t comm = MSG_task_isend_bounded(task2extractor, mb_camera[house], BW_camera2extractor);
        xbt_dynar_push_as(comms, msg_comm_t, comm);

        #ifdef DEBUG
            XBT_INFO("H%d : T%d : camera", house, data->type);
        #endif

        MSG_process_sleep(3 * CameraStepTime);
    }

    WaitAll(comms);

    return 0;
}

