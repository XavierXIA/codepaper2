#ifndef _UTILS_H__
#define _UTILS_H__

//int GetExtractorRecSemIdx(int extractor, int recognizer);
//int GetDeciderRecSemIdx(int decider, int recorder);
void WaitAll(xbt_dynar_t comms);
char* GetDetect2RecogMB(int app, int detect, int recog);
char* GetRecog2DetectMB(int app, int recog, int detect);
char* GetDecide2RecordMB(int app, int decide, int record);
char* GetRecord2DecideMB(int app, int record, int decide);
void FreeSemDynar(xbt_dynar_t sems);


#endif

