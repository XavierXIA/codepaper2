#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(DB,"DB");

static int DB_Response(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid db index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");
    int house = xbt_str_parse_int(argv[3], "Invalid house index: %s");

    msg_task_t task_request = (msg_task_t) MSG_process_get_data(MSG_process_self());

    //execute recognizer request
    MSG_task_execute(task_request);

    MSG_task_destroy(task_request);

    msg_task_t task_response = MSG_task_create("recognize", Cal_RecognizeTotal/DB_PartNb, Comm_DB_SizeTotal/DB_PartNb, NULL);
    MSG_task_send_bounded(task_response, mb_recognizer[app][house_recognizer[house]], BW_db2recognizer);
}

int DB(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid db index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    while(1)
    {
        msg_task_t task_request = NULL;
        MSG_task_receive_bounded(&task_request, mb_db[app][idx], BW_recognizer2db);

        RecognizeData data = *(RecognizeData *) task_request->data;

        const char *prName = "db_response";
        char** prArgv = xbt_new(char *, 4);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = bprintf("%d", idx);
        prArgv[2] = bprintf("%d", app);
        prArgv[3] = bprintf("%d", data.house);

        msg_process_t p = MSG_process_create_with_arguments(prName, DB_Response, task_request, MSG_host_self(), 4, prArgv);
    }
}

