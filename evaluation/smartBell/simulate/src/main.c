#include "global.h"
#include <time.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(MAIN, "MAIN");

//global variables
int vmIdx = 0;
xbt_dynar_t vmList;

//xbt_dynar_t sems_extractor;
//xbt_dynar_t sems_recognizer;
//xbt_dynar_t sems_decider;
//xbt_dynar_t sems_recorder;
//xbt_dynar_t sems_executer;
//xbt_dynar_t sems_db;
//xbt_dynar_t sems_extractorRec;
//xbt_dynar_t sems_deciderRec;

//function declaration
int DB(int argc, char *argv[]);
int GenRecognizer(int argc, char *argv[]);
int GenRecorder(int argc, char *argv[]);
int Decider(int argc, char *argv[]);
int Executer(int argc, char *argv[]);
int Screen(int argc, char *argv[]);
int Camera(int argc, char *argv[]);
int GenExtractor(int argc, char *argv[]);


static int RunVM(int argc, char *argv[])
{
    int index = xbt_str_parse_int(argv[1], "Invalid component index: %s");
    int app = xbt_str_parse_int(argv[3], "Invalid component index: %s");
    double bound = xbt_str_parse_double(argv[4], "Invalid cpu bound: %s");

    char procName[30];
    sprintf(procName, "%d%s%d", app, argv[2], index);

    int prArgc = 3;
    char** prArgv = xbt_new(char *, 3);
    prArgv[0] = xbt_strdup(procName);
    prArgv[1] = bprintf("%s", argv[1]);
    prArgv[2] = bprintf("%s", argv[3]);


    char vmName[12];
    sprintf(vmName, "VM%d", vmIdx);
    vmIdx++;

    msg_vm_t vm = MSG_vm_create_core(MSG_host_self(), vmName);
    MSG_vm_set_bound(vm, bound);
    xbt_dynar_push_as(vmList, msg_vm_t, vm);
    MSG_vm_start(vm);

    if(strcmp("db", argv[2])==0)
        MSG_process_create_with_arguments(procName, DB, NULL, vm, prArgc, prArgv);
    else if(strcmp("recognizer", argv[2])==0)
        MSG_process_create_with_arguments(procName, GenRecognizer, NULL, vm, prArgc, prArgv);
    else if(strcmp("extractor", argv[2])==0)
        MSG_process_create_with_arguments(procName, GenExtractor, NULL, vm, prArgc, prArgv);
    else if(strcmp("decider", argv[2])==0)
        MSG_process_create_with_arguments(procName, Decider, NULL, vm, prArgc, prArgv);
    else if(strcmp("executer", argv[2])==0)
        MSG_process_create_with_arguments(procName, Executer, NULL, vm, prArgc, prArgv);
    else if(strcmp("recorder", argv[2])==0)
        MSG_process_create_with_arguments(procName, GenRecorder, NULL, vm, prArgc, prArgv);

    return 0;
}

int main(int argc, char *argv[])
{
    srand(time(NULL));

    vmList = xbt_dynar_new(sizeof(msg_vm_t), NULL);

    //sems_extractorRec = xbt_dynar_new(sizeof(msg_sem_t), NULL);
    //sems_deciderRec   = xbt_dynar_new(sizeof(msg_sem_t), NULL);

    MSG_init(&argc, argv);
    xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n"
            "\tExample: %s msg_platform.xml msg_deployment.xml\n", argv[0], argv[0]);

    MSG_create_environment(argv[1]);/*platform*/
    MSG_function_register("RunVM", RunVM);
    MSG_function_register("camera", Camera);
    MSG_function_register("screen", Screen);
    MSG_launch_application(argv[2]);/*Deploy*/

    MSG_main();

    msg_vm_t vm;
    vmIdx--;
    while(!xbt_dynar_is_empty(vmList))
    {
        xbt_dynar_remove_at(vmList, vmIdx, &vm);
        //MSG_vm_destroy(vm);
        vmIdx--;
    }
    xbt_dynar_free(&vmList);

    //FreeSemDynar(sems_extractorRec);
    //FreeSemDynar(sems_deciderRec);

    return 0;
}

