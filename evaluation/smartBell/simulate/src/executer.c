#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(EXECUTER,"EXECUTER");

static int Execute(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid executer index: %s");

    msg_task_t task_execute = (msg_task_t) MSG_process_get_data(MSG_process_self());
    CameraData data = *(CameraData *) task_execute->data;
    int house = data.house;

    MSG_task_execute(task_execute);

    msg_task_t task_show = MSG_task_create("show", Cal_Min, Comm_Min, NULL);
    task_show->data = task_execute->data;
    MSG_task_destroy(task_execute);

    MSG_task_send_bounded(task_show, mb_screen[house], BW_executer2screen);
}

int Executer(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid executer index: %s");
    int app = xbt_str_parse_int(argv[2], "Invalid app index: %s");

    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    while(1)
    {
        msg_task_t task_execute = NULL;
        MSG_task_receive_bounded(&task_execute, mb_executer[app][idx], BW_decider2executer);

        const char *prName = "execute";
        char** prArgv = xbt_new(char *, 2);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = bprintf("%d", idx);

        msg_process_t p = MSG_process_create_with_arguments(prName, Execute, task_execute, MSG_host_self(), 2, prArgv);
    }
}

