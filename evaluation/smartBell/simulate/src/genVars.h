#ifndef _GENVARS_H__
#define _GENVARS_H__

#define KillTime 1000100000
#define TestCount 1

#define CameraStepTime 5

#define DB_PartNb   1

//cal size
#define Cal_Extract 100
#define Cal_Decide  40
#define Cal_Execute 20
#define Cal_Recorde 1000
#define Cal_RecognizeTotal 1000
#define Cal_DB 20
#define Cal_Min 1

//comm size
#define Comm_CameraImg 1000
#define Comm_Face 150
#define Comm_DB_SizeTotal 100000
#define Comm_Min 1

//link type bw
#define BW_camera2extractor     600000.0
#define BW_extractor2camera     10000.0
#define BW_extractor2decider    100000.0
#define BW_decider2extractor    10000.0
#define BW_extractor2recognizer 100000.0
#define BW_recognizer2extractor 10000.0
#define BW_recognizer2db        10000.0
#define BW_db2recognizer        300000.0
#define BW_decider2recorder     200000.0
#define BW_recorder2decider     10000.0
#define BW_decider2executer     10000.0
#define BW_executer2decider     10000.0
#define BW_executer2screen      10000.0
#define BW_screen2executer      10000.0

//mailbox
extern char mb_camera[][30];
extern char mb_screen[][30];
extern char mb_db[][10][30];
extern char mb_decider[][3][30];
extern char mb_executer[][3][30];
extern char mb_recognizer[][3][30];
 
extern int house_recognizer[];
extern int house_decider[];
extern int house_executer[];
extern int house_db[];
extern char* extractor_houses[][3];

extern int HouseNb[];
extern int ExtractorNb[];
extern int DeciderNb[];

#endif

