#include "global.h"
#include <stdio.h>

XBT_LOG_NEW_DEFAULT_CATEGORY(SCREEN,"SCREEN");

int Screen(int argc, char *argv[])
{
    int idx = xbt_str_parse_int(argv[1], "Invalid screen index: %s");

    MSG_process_set_kill_time(MSG_process_self(), KillTime);

    while(1)
    {
        msg_task_t task_show = NULL;
        MSG_task_receive_bounded(&task_show, mb_screen[idx], BW_executer2screen);

        CameraData data = *(CameraData *) task_show->data;
        double time = MSG_get_clock() - data.sendTime;

        #ifdef DEBUG
            XBT_INFO("H%d : T%d : %f", data.house, data.type, time);
        #else
            printf("%f\n", time);
        #endif

        xbt_free(task_show->data);
        MSG_task_destroy(task_show);
    }
}

