#cp genModel/app/genVars.h src/
#cp genModel/app/genVars.c src/
rm build -rf

if [ ! -d build ]; then
  mkdir -p build;
fi

cd build

if [ ! -f Makefile ]; then
    cmake ../src
fi

make
