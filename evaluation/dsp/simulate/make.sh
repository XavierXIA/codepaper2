if [ ! -d build ]; then
  mkdir -p build;
fi

cd build

if [ ! -f Makefile ]; then
    cmake ../src
fi

make

