#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(MAIN, "MAIN");

//global variables
int vmIdx = 0;
int killTime = 9999999;
xbt_dynar_t vmList;
#ifdef OPT_SEM
    xbt_dynar_t sems;
#endif

//function declaration
int DataSource(int argc, char *argv[]);
int DataConsumer(int argc, char *argv[]);
int Operator(int argc, char *argv[]);

//RunVM specific for processors
//argv 1 cpu_bound
//argv 2 function name
static int RunVM(int argc, char *argv[])
{
    double bound = xbt_str_parse_double(argv[1], "Invalid cpu bound: %s");

    char vmName[12];
    sprintf(vmName, "VM%d", vmIdx);
    vmIdx++;
  
    msg_vm_t vm = MSG_vm_create_core(MSG_host_self(), vmName);
    MSG_vm_set_bound(vm, bound);
    xbt_dynar_push_as(vmList, msg_vm_t, vm);
    MSG_vm_start(vm);

    if(strcmp("Operator", argv[2])==0)
    {
        //argv 3 id
        //argv 4 rec_bw
        //argv 5 send_bw
        //argv 6 comm_size
        //argv 7 cal_size
        //argv 8 to_list
        char procName[30];
        sprintf(procName, "%s", argv[3]);

        const int prArgc = 7;
        char** prArgv = xbt_new(char *, prArgc);
        prArgv[0] = xbt_strdup(procName);
        prArgv[1] = xbt_strdup(argv[3]);
        prArgv[2] = xbt_strdup(argv[4]);
        prArgv[3] = xbt_strdup(argv[5]);
        prArgv[4] = xbt_strdup(argv[6]);
        prArgv[5] = xbt_strdup(argv[7]);
        prArgv[6] = xbt_strdup(argv[8]);
  
        MSG_process_create_with_arguments(procName, Operator, NULL, vm, prArgc, prArgv);
    }
    else if(strcmp("DataSource", argv[2])==0)
    {
        //argv 3 send_bw
        //argv 4 comm_size
        //argv 5 cal_size
        //argv 6 to_list
        char procName[30];
        sprintf(procName, "DataSource");

        const int prArgc = 5;
        char** prArgv = xbt_new(char *, prArgc);
        prArgv[0] = xbt_strdup(procName);
        prArgv[1] = xbt_strdup(argv[3]);
        prArgv[2] = xbt_strdup(argv[4]);
        prArgv[3] = xbt_strdup(argv[5]);
        prArgv[4] = xbt_strdup(argv[6]);
  
        MSG_process_create_with_arguments(procName, DataSource, NULL, vm, prArgc, prArgv);
    }
    else if(strcmp("DataConsumer", argv[2])==0)
    {
        //argv 3 id
        //argv 4 rec_bw
        char procName[30];
        sprintf(procName, "%s", argv[3]);

        const int prArgc = 3;
        char** prArgv = xbt_new(char *, prArgc);
        prArgv[0] = xbt_strdup(procName);
        prArgv[1] = xbt_strdup(argv[3]);
        prArgv[2] = xbt_strdup(argv[4]);
  
        MSG_process_create_with_arguments(procName, DataConsumer, NULL, vm, prArgc, prArgv);
    }
    else
    {
        printf("\x1B[31m%s\x1B[0m\n","unknown function");
    }
}


int main(int argc, char *argv[])
{
    MSG_init(&argc, argv);
    xbt_assert(argc > 2, "Usage: %s platform_file deployment_file\n"
            "\tExample: %s msg_platform.xml msg_deployment.xml\n", argv[0], argv[0]);

    killTime = xbt_str_parse_int(argv[3], "Invalid killTime: %s");
    vmList = xbt_dynar_new(sizeof(msg_vm_t),  NULL);
    #ifdef OPT_SEM
        sems = xbt_dynar_new(sizeof(msg_sem_t), NULL);
    #endif

    MSG_create_environment(argv[1]);/*platform*/
    MSG_function_register("RunVM",        RunVM);
    MSG_function_register("DataSource",   DataSource);
    MSG_function_register("DataConsumer", DataConsumer);
    MSG_launch_application(argv[2]);/*Deploy*/

    MSG_main();

    msg_vm_t vm;
    vmIdx--;
    while(!xbt_dynar_is_empty(vmList))
    {
        xbt_dynar_remove_at(vmList, vmIdx, &vm);
        //MSG_vm_destroy(vm);
        vmIdx--;
    }
    xbt_dynar_free(&vmList);

    #ifdef OPT_SEM
        int i = xbt_dynar_length(sems) - 1;
        while(!xbt_dynar_is_empty(sems))
        {
            msg_sem_t sem = NULL;
            xbt_dynar_remove_at(sems, i, &sem);

            xbt_free(sem);
            i--;
        }
        xbt_dynar_free(&sems);
    #endif
}

