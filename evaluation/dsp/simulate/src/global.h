#ifndef _GLOBAL_H__
#define _GLOBAL_H__

#include "simgrid/msg.h"

//#define DEBUG
//#define OPT_SEM
//#define OPT_RESP

typedef struct
{
    //int index;
    double sendTime;
    #ifdef OPT_SEM
        msg_sem_t* sem;
    #endif
} dspData;

//----------------------------------------------------------------------------

extern int killTime;

extern int vmIdx;
extern xbt_dynar_t vmList;
#ifdef OPT_SEM
    extern xbt_dynar_t sems;
#endif

#endif

