#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(Operator,"Operator");

//argv 1 send_bw
//argv 2 comm_size
//argv 3 cal_size
//argv 4 to
static int Process(int argc, char *argv[])
{
    double bw = xbt_str_parse_double(argv[1], " Operator: Invalid send_BW Limit: %s");
    double comm_size = xbt_str_parse_double(argv[2], "Operator : Invalid Comm Size: %s");
    double cal_size = xbt_str_parse_double(argv[3], "Operator : Invalid Task Cal Size: %s");
    char* to = argv[4];

    //execute
    msg_task_t task = (msg_task_t) MSG_process_get_data(MSG_process_self());
    MSG_task_execute(task);

    dspData data = *(dspData *) task->data;
    #ifdef DEBUG
        XBT_INFO("task processed");
    #else
        #ifdef OPT_RESP
            //processing end times are considered as response times to take data filtering cases into account
            double time = MSG_get_clock() - data.sendTime;
            printf("%f\n", time);
        #endif
    #endif
    //create successing task
    msg_task_t nextTask = MSG_task_create("task", cal_size, comm_size, NULL);
    nextTask->data = task->data;

    //send successing task
    char mb[50];
    sprintf(mb, "%s", to);

    #ifdef OPT_SEM
        MSG_sem_acquire(data.sem);
    #endif

    #ifdef DEBUG
        XBT_INFO("opt ready to send to %s", mb);
    #endif
    
    MSG_task_send_bounded(nextTask, mb, bw);

    #ifdef OPT_SEM
        MSG_sem_release(data.sem);
    #endif
    MSG_task_destroy(task);
}

//argv 1 id
//argv 2 rec_bw
//argv 3 send_bw
//argv 4 comm_size
//argv 5 cal_size
//argv 6 to_list
int Operator(int argc, char *argv[])
{
    double rec_bw = xbt_str_parse_double(argv[2], "Operator : Invalid fromBW Limit: %s");

    xbt_dynar_t to_list = xbt_str_split(argv[6], NULL);
    int to_nb = xbt_dynar_length(to_list);
    int count = 0;

    MSG_process_set_kill_time(MSG_process_self(), killTime);

    #ifdef OPT_SEM
        int baseIdx = xbt_dynar_length(sems);
        for(int i=0; i<to_nb; i++)
        {
            msg_sem_t* sem = xbt_new(msg_sem_t, 1);
            *sem = MSG_sem_init(1);
            xbt_dynar_set(sems, baseIdx+i, sem);
        }
    #endif

    //receive & create new process
    char mb[50];
    sprintf(mb, "%s", argv[1]);

    while(1)
    {
        msg_task_t task = NULL;
        MSG_task_receive_bounded(&task, mb, rec_bw);

        #ifdef DEBUG
            XBT_INFO("%s rec", mb);
        #endif

        const char *prName = "process";
        const int prArgc = 5;
        char** prArgv = xbt_new(char *, prArgc);
        prArgv[0] = xbt_strdup(prName);
        prArgv[1] = xbt_strdup(argv[3]);
        prArgv[2] = xbt_strdup(argv[4]);
        prArgv[3] = xbt_strdup(argv[5]);
        prArgv[4] = xbt_strdup(xbt_dynar_get_as(to_list, count%to_nb, char*));

        #ifdef OPT_SEM
            ((dspData*) task->data)->sem = xbt_dynar_get_as(sems, baseIdx+(count%to_nb), msg_sem_t);
        #endif
        count++;

        msg_process_t p = MSG_process_create_with_arguments(prName, Process, task, MSG_host_self(), prArgc, prArgv);
    }
}

