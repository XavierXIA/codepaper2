#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(DataSrc,"DataSrc");

//argv 1 send_bw
//argv 2 comm_size
//argv 3 cal_size
//argv 4 to_list
int DataSource(int argc, char *argv[])
{
    //int idx = xbt_str_parse_int(argv[1], "Invalid DataSrc Index: %s");
    double bw = xbt_str_parse_double(argv[1], "DataSrc : Invalid BW Limit: %s");
    double comm_size = xbt_str_parse_double(argv[2], "DataSrc : Invalid Comm Size: %s");
    double cal_size = xbt_str_parse_double(argv[3], "DataSrc : Invalid Task Cal Size: %s");

    //send to each successor
    xbt_dynar_t to_list = xbt_str_split(argv[4], NULL);
    int to_nb = xbt_dynar_length(to_list);

    char mb[50];

    xbt_dynar_t comms = xbt_dynar_new(sizeof(msg_comm_t), NULL);

    //parallel
    for(int i=0; i<to_nb; i++)
    {
        msg_task_t task = MSG_task_create("task", cal_size, comm_size, NULL);

        dspData* data = xbt_new(dspData, 1);
        data->sendTime = MSG_get_clock();
        //data->index = idx;

        task->data = data;

        sprintf(mb, "%s", xbt_dynar_get_as(to_list, i, char*));

        MSG_task_send_bounded(task, mb, bw);

        #ifdef DEBUG
            XBT_INFO("dataSrc sent to %s", mb);
        #endif
    }
}

