#include "global.h"

XBT_LOG_NEW_DEFAULT_CATEGORY(DataCon,"DataCon");

static int Process(int argc, char *argv[])
{
    //execute
    msg_task_t task = (msg_task_t) MSG_process_get_data(MSG_process_self());
    MSG_task_execute(task);

    dspData data = *(dspData *) task->data;

    #ifdef DEBUG
        XBT_INFO("task processed");
    #else
        double time = MSG_get_clock() - data.sendTime;
        printf("%f\n", time);
    #endif

    xbt_free(task->data);
    MSG_task_destroy(task);
}


//argv 1 id
//argv 2 rec_bw
int DataConsumer(int argc, char *argv[])
{
    double bw = xbt_str_parse_double(argv[2], "DataCon : Invalid BW Limit: %s");

    MSG_process_set_kill_time(MSG_process_self(), killTime);

    char mb[50];
    sprintf(mb, "%s", argv[1]);

    //keep receiving
    while(1)
    {
        msg_task_t task = NULL;

        MSG_task_receive_bounded(&task, mb, bw);

        #ifdef DEBUG
            XBT_INFO("%s rec", mb);
        #endif

        const char *prName = "process";
        const int prArgc = 1;
        char** prArgv = xbt_new(char *, prArgc);
        prArgv[0] = xbt_strdup(prName);

        msg_process_t p = MSG_process_create_with_arguments(prName, Process, task, MSG_host_self(), prArgc, prArgv);
    }
}

