#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../input/sharedSetup.tcl

set maxAppNB [lindex $argv 0]
set appStep 100
set initAppNB 1

set i 1
while {$i < [llength $argv]} {
    switch -- [lindex $argv $i] {
        "-appStep" {
            incr i
            set appStep [lindex $argv $i]
        }
    }
    incr i
}

catch {exec echo algo,compNB,wal,respTime,exeTime > $scriptPath/result.csv}
file delete $scriptPath/base

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set algos {DAFNO-InitCO-DCOinf AFNO-DCOinf DAFNO-DCOinf DAFNO AFNO}

foreach algo $algos {
    catch {exec mkdir -p $scriptPath/$algo}
}

array set base ""
foreach algo $algos {
    set isInitCO [string first -InitCO $algo]
    set jar $algo
    if {$isInitCO >= 0} {
        set jar [string replace $algo $isInitCO [expr $isInitCO+[string length "-InitCO"]-1] ""] 
        set testNB 3
    } else {
        set testNB 10
    }
    set jar $scriptPath/../../../target/$jar.jar

    set isTimeout ""

    set appNB $initAppNB
    while {$appNB < $maxAppNB} {
        set compNB ""
        set wals ""
        set respTimes ""
        set exeTimes ""

        set i 0
        while {$i < $testNB} {
            set platform $scriptPath/$algo/platform$appNB-${i}.xml
            set deploy   $scriptPath/$algo/deploy$appNB-${i}.xml

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            catch {exec $scriptPath/launchAlgo.sh $isInitCO $jar $appNB $platform $deploy} log
            puts $log
            catch {exec echo $log | grep timeout} isTimeout

            if {$isTimeout eq "timeout"} {
                set appNB $maxAppNB
                set i $testNB
            } else {
                if {$compNB eq ""} {
                    catch {exec echo $log | grep Comp | awk "{print \$4}"} compNB
                }

                catch {exec echo $log | grep Score | awk "{print \$3}"} wal
                lappend wals $wal

                catch {exec echo $log | grep execution | awk "{print \$4}"} exeTime
                lappend exeTimes $exeTime

                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ simulate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                catch {exec $scriptPath/../../simulate/build/simulate $platform $deploy 999999} log
                puts "simulation finish"

                set respTime [GetAvgValue $log]
                lappend respTimes $respTime
            }
            incr i
        }

        if {$algo eq "DAFNO-InitCO-DCOinf"} {
            if {$isTimeout eq "timeout"} {
                puts "DAFNO-InitCO-DCOinf timeout !" 
            } else {
                set base($appNB,wal) [GetAvgValue $wals]
                puts "$appNB apps wal base : $base($appNB,wal)"
                catch {exec echo "$appNB apps wal base : $base($appNB,wal)" >> $scriptPath/base}

                set base($appNB,respTime) [GetAvgValue $respTimes]
                puts "$appNB apps respTime base : $base($appNB,respTime)"
                catch {exec echo "$appNB apps respTime base : $base($appNB,respTime)" >> $scriptPath/base}
            }
        }

        if {$isTimeout ne "timeout"} {
            set i 0
            while {$i < $testNB} {
                catch {exec echo "$algo,$compNB,[expr [lindex $wals $i]/$base($appNB,wal)],[expr [lindex $respTimes $i]/$base($appNB,respTime)],[lindex $exeTimes $i]" >> $scriptPath/result.csv}
                incr i
            }
        }
        set appNB [expr $appNB + $appStep]
    }
}

