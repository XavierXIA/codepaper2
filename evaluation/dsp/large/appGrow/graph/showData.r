#!/usr/bin/Rscript

library(plyr)
library(ggplot2)

df <- read.csv("../result.csv")
df <- ddply(df, .(algo), summarize, avgRespT=mean(respTime))

print(df)
