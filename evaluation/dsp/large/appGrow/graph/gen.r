#!/usr/bin/Rscript

library(plyr)
library(ggplot2)
library(ggforce)

df <- read.csv("../result.csv")
df <- ddply(df, .(algo,compNB), summarize, avgWAL=mean(wal), sdWAL=sd(wal), avgRespT=mean(respTime), avgExeT=mean(exeTime), sdExeT=sd(exeTime))

print(df)

#theme_set(theme_bw() + theme(axis.text = element_text(size=25), axis.title = element_text(size=24), strip.text = element_text(size=24), title = element_text(size=0)))
theme_set(theme_bw() + theme(legend.text = element_text(size=19), legend.position="bottom", legend.title = element_text(size=20), axis.text = element_text(size=26), axis.title = element_text(size=26), strip.text = element_text(size=26), title = element_text(size=0)))
#legend.position = c(.21, .8)

#df$algo <- factor(df$algo, levels = c("CPLEX", "DAFNO-InitCO-DCO(1)", "AFNO-DCO(inf)", "FirstFit"))

limits <- aes(ymax = avgExeT + 2*sdExeT/(50^0.5), ymin = avgExeT - 2*sdExeT/(50^0.5))
myplot <- ggplot(df, aes(x=compNB, y=avgExeT, color=algo)) + labs(title="", x="Component Number", y="Execution Time (s)", color="Algorithms:", shape="Algorithms:") + geom_point(mapping = aes(color=algo, shape=algo), size=4) + guides(color = guide_legend(title.position="left", override.aes = list(linetype = c(rep("blank", 3)), size=4))) + geom_line() + geom_errorbar(limits, width=1) + facet_zoom(y=avgExeT<45, x=compNB<2000, zoom.size=0.5, horizontal=TRUE) + geom_hline(yintercept=c(400), linetype="dotted", size=1) + annotate("text", x=3000, y=460, label="timeout", hjust=0, size=7)# + scale_color_manual(values = c("AFNO-DCO" = "orange", "AFNO" = "darkgreen", "DCO" = "red", "Naive" = "blue"))# + scale_x_continuous(breaks = seq(0, 120, by = 50))
ggsave(filename="./appGrowExeTime.pdf", plot=myplot, width=10, height=4.2)

# + annotate("text", x=5100, y=380, label="AFNO-DCO(1)\ntimeout", hjust=0, size=4) + annotate("text", x=7200, y=450, label="DAFNO-DCO(1)\ntimeout", hjust=0, size=4)


theme_set(theme_bw() + theme(legend.text = element_text(size=19), legend.position=c(0.6, 0.85), axis.text = element_text(size=26), axis.title = element_text(size=26), strip.text = element_text(size=26), title = element_text(size=0)))
#legend.position = c(.21, .8)

#df$algo <- factor(df$algo, levels = c("CPLEX", "DAFNO-InitCO-DCO(1)", "AFNO-DCO(inf)", "FirstFit"))

limits <- aes(ymax = avgExeT + 2*sdExeT/(50^0.5), ymin = avgExeT - 2*sdExeT/(50^0.5))
myplot <- ggplot(df, aes(x=compNB, y=avgExeT, color=algo)) + labs(title="", x="Component Number", y="Execution Time (s)") + geom_point(mapping = aes(color=algo, shape=algo), size=4) + guides(color = guide_legend(override.aes = list(linetype = c(rep("blank", 3)), size=4))) + geom_line() + geom_errorbar(limits, width=1) + facet_zoom(y=avgExeT<50, x=compNB<2000, zoom.size=0.5, horizontal=TRUE) + geom_hline(yintercept=c(400), linetype="dotted", size=1) + annotate("text", x=3000, y=460, label="timeout", hjust=0, size=7)# + scale_color_manual(values = c("AFNO-DCO" = "orange", "AFNO" = "darkgreen", "DCO" = "red", "Naive" = "blue"))# + scale_x_continuous(breaks = seq(0, 120, by = 50))
ggsave(filename="./appGrowExeTime2.pdf", plot=myplot, width=10, height=4.2)




# theme_set(theme_bw() + theme(legend.text = element_text(size=16), legend.position="bottom", axis.text = element_text(size=25), axis.title = element_text(size=24), strip.text = element_text(size=24), title = element_text(size=0)))
# 
# myplot <- ggplot(df, aes(x=compNB, y=avgExeT, color=algo)) + labs(title = "", x = "Component Number", y = "Execution Time (s)") + geom_point(mapping = aes(color=algo, shape=algo), size=2.3) + guides(color = guide_legend(override.aes = list(linetype = c(rep("blank", 3)), size=4))) + geom_line() + geom_errorbar(limits, width=4) + geom_vline(xintercept=c(6295), linetype="dotted", size=1) + annotate("text", x=6400, y=30, label="AFNO-DCO(1)\ntimeout", hjust=0, size=4) + facet_zoom(y=avgExeT<100, x=compNB<2000, zoom.size=0.7, horizontal=FALSE)# + scale_color_manual(values = c("AFNO-DCO" = "orange", "AFNO" = "darkgreen", "DCO" = "red", "Naive" = "blue"))# + scale_x_continuous(breaks = seq(0, 120, by = 50))
# ggsave(filename="./appGrowExeTime2.pdf", plot=myplot, width=8.2, height=5.4)

theme_set(theme_bw() + theme(legend.text = element_text(size=16), legend.position = c(.25, .8), axis.text = element_text(size=25), axis.title = element_text(size=24), strip.text = element_text(size=24), title = element_text(size=0)))

myplot <- ggplot(subset( subset(df, compNB>19), compNB<7500), aes(x=compNB, y=avgRespT, color=algo)) + labs(title="", x="Component Number", y="DSP Applications'\nAverage Response\nTime (normalized)") + geom_point(mapping=aes(color=algo, shape=algo), size=4) + scale_y_continuous(labels=scales::percent, limits=c(0.9965, 1.08)) + guides(color=guide_legend(override.aes=list(size=4))) + geom_vline(xintercept=c(6295), linetype="dotted", size=1) + annotate("text", x=6700, y=1.038, label="AFNO-DCO(1)\ntimeout", hjust=0, size=4.5, angle=90)
ggsave(filename="./appGrowRespTime.pdf", plot=myplot, width=8.2, height=3.4)


# ggplot(df, aes(x=wal, y=respTime)) + labs(title = "", x = "WAL (ms)", y ="DSP Application\nResponse Time (s)") + stat_smooth() + geom_point(mapping = aes(color=algo, shape=algo), size=2.3) + guides(color = guide_legend(override.aes = list(size=4)))# + scale_x_continuous(limits = c(0, 21.5))

