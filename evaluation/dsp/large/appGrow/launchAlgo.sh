#1 isInitCO
#2 jar
#3 appNB
#4 platform
#5 deploy

if [ "$1" -eq "-1" ]; then
    if
        timeout 1800s java -Xmx12048M -jar $2 -infra ../input/infra.xml -app ../input/app.xml -appLimit $3 -SimgridPlatform $4 -SimgridDeploy $5 ../input/appDU.txt
    then
        echo "finish"
    else
        echo "timeout"
    fi
else
    if
        timeout 1800s java -Xmx12048M -jar $2 -infra ../input/infra.xml -app ../input/app.xml -appLimit $3 -initCO REQ_BW -SimgridPlatform $4 -SimgridDeploy $5 ../input/appDU.txt
    then
        echo "finish"
    else
        echo "timeout"
    fi
fi

