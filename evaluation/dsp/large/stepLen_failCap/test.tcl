#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../input/sharedSetup.tcl

if {[llength $argv] < 3} {
    puts "usage : test.tcl appNB respTimeBase walBase"
}
set appNB [lindex $argv 0]

array set base ""
set base(respTime) [lindex $argv 1]
set base(wal) [lindex $argv 2]

set testNB 3
set steps {1 10 100 1000}
set caps {infinite 3 10}

catch {exec echo step,wal,respTime,exeTime > $scriptPath/result.csv}

foreach step $steps {
    catch {exec mkdir -p $scriptPath/$step}

    foreach cap $caps {
        catch {exec mkdir -p $scriptPath/$step/$cap}

        set i 0
        while {$i < $testNB} {
            set platform $scriptPath/$step/$cap/platform$i.xml
            set deploy   $scriptPath/$step/$cap/deploy$i.xml
            
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            set log ""
            if {$cap eq "infinite"} {
                catch {exec $scriptPath/DAFNO-InitCO-DCO.sh $appNB $step $platform $deploy} log
            } else {
                catch {exec $scriptPath/DAFNO-InitCO-DCO-FailCap.sh $appNB $step $platform $deploy $cap} log
            }
            puts $log

            catch {exec echo $log | grep Score | awk "{print \$3}"} wal
            catch {exec echo $log | grep execution | awk "{print \$4}"} exeTime

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ simulate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            catch {exec $scriptPath/../../simulate/build/simulate $platform $deploy 999999} log
            set respTime [GetAvgValue $log]
            puts " respTime : $respTime"

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            catch {exec echo "$step,[expr $wal/$base(wal)],[expr $respTime/$base(respTime)],$exeTime" >> $scriptPath/result.csv} msg
            puts $msg
        
            incr i
        }
    }
}

