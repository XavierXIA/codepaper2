#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
set to_nb [lindex $argv 0]

set nb 1
set out_path output
set replace 1

set i 2
while {$i < [llength $argv]} {
    switch -- [lindex $argv $i] {
        "-initNB" {
            incr i
            set nb [lindex $argv $i]
        }
        "-outPath" {
            incr i
            set out_path [lindex $argv $i]
        }
        "-noreplace" {
            set replace 0
        }
    }
    incr i
}

if {$replace} {
    file delete -force $out_path
}
catch {exec mkdir -p $out_path} msg

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GetDU_FileName {app} {
    regexp {app([0-9]+)\.xml} $app vv appIdx
    return "app${appIdx}DU.txt"
}

proc ColorShow {str fg {nonewline 0}} {
    set fgList "30 black 31 red 32 green 33 yellow 34 blue 37 white"
    set fgNo   [lindex $fgList [expr [lsearch $fgList $fg] - 1]]
    set str    "\033\[0;${fgNo}m$str\033\[0m"
    if {$nonewline != 1} {
        puts "$str"
    } else {
        puts -nonewline "$str"
    }
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
while {$nb < $to_nb} {
    catch {exec find $scriptPath/../output -name app*.xml} apps

    if {$to_nb > [llength $apps]} {
        set to_nb [llength $apps]
    }

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ gen apps ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    set appFile "$out_path/$nb.xml"
    catch {exec $scriptPath/tester_echo.sh $appFile 0} msg

    set DU_File "$out_path/$nb.txt"
    file delete $DU_File

    set i 0
    while {$i < $nb} {
        set app [lindex $apps $i]

        catch {exec tail $app -n +3 | head -n -1 >> $appFile} msg
        catch {exec cat $scriptPath/../output/[GetDU_FileName $app] >> $DU_File} msg

        incr i
    }

    catch {exec $scriptPath/tester_echo.sh $appFile 1} msg


    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    catch {exec $scriptPath/AFNO_DCOinf.sh $appFile} msg
    puts $msg

    catch {exec echo $msg | grep Score | awk "{print \$3}"} score

    if {$score==-1} {
        set appIdx ""
        foreach line $msg {
            if {[regexp {^App([0-9]+):} $line vv appIdx]} {
                break
            }
        }

        if {$appIdx eq ""} {
            regexp {app([0-9]+)\.xml$} [lindex $apps [expr $i-1]] vv appIdx
        }

        file delete $scriptPath/../output/app$appIdx.xml $scriptPath/../output/app${appIdx}DU.txt
        ColorShow "app$appIdx deleted" red

        set nb [expr $nb - 1]
    }

    set nb [expr $nb + 1]
}

