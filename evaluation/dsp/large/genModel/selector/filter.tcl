#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]

set nb 2
set out_path output
set replace 1
set allDelete 1

set i 0
while {$i < [llength $argv]} {
    switch -- [lindex $argv $i] {
        "-nb" {
            incr i
            set nb [lindex $argv $i]
        }
        "-outPath" {
            incr i
            set out_path [lindex $argv $i]
        }
        "-noreplace" {
            set replace 0
        }
        "-noAllDelete" {
            set allDelete 0
        }
    }
    incr i
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GetDU_FileName {app} {
    regexp {app([0-9]+)\.xml} $app vv appIdx
    return "app${appIdx}DU.txt"
}

proc ColorShow {str fg {nonewline 0}} {
    set fgList "30 black 31 red 32 green 33 yellow 34 blue 37 white"
    set fgNo   [lindex $fgList [expr [lsearch $fgList $fg] - 1]]
    set str    "\033\[0;${fgNo}m$str\033\[0m"
    if {$nonewline != 1} {
        puts "$str"
    } else {
        puts -nonewline "$str"
    }
}
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ gen ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
catch {exec find $scriptPath/../output -name app*.xml} apps
catch {exec mkdir -p $out_path} msg

set i 0
set len [llength $apps]
set appFile ""
set DU_File ""

while {$i < $len} {
    if {$i%$nb == 0} {
        set appFile "$out_path/app$i-[expr $i+$nb-1].xml"
        catch {exec $scriptPath/tester_echo.sh $appFile 0} msg

        set DU_File "$out_path/app$i-[expr $i+$nb-1]DU.txt"
        file delete $DU_File
    }

    set app [lindex $apps $i]
    catch {exec tail $app -n +3 | head -n -1 >> $appFile} msg
    catch {exec cat $scriptPath/output/[GetDU_FileName $app] >> $DU_File} msg

    incr i

    if {$i%$nb == 0 || $i == $len} {
        catch {exec $scriptPath/tester_echo.sh $appFile 1} msg

        catch {exec $scriptPath/AFNO_CriticTop.sh $appFile} msg
        puts $msg

        catch {exec echo $msg | grep Score | awk "{print \$3}"} score

        if {$score==-1} {
            if {$allDelete} {
                set j [expr $i-1]
                set notFinish 1
                while {$notFinish} {
                    if {$j%$nb == 0} {
                        set notFinish 0
                    }

                    regexp {app([0-9]+)\.xml$} [lindex $apps $j] vv appIdx

                    file delete $scriptPath/../output/app$appIdx.xml $scriptPath/../output/app${appIdx}DU.txt
                    ColorShow "app$appIdx deleted" red

                    set j [expr $j-1]
                }
            } else {
                set appIdx ""
                foreach line $msg {
                    if {[regexp {^App([0-9]+):} $line vv appIdx]} {
                        break
                    }
                }
                file delete $scriptPath/../output/app$appIdx.xml $scriptPath/../output/app${appIdx}DU.txt
                ColorShow "app$appIdx deleted" red
            }
        }
    }
}

