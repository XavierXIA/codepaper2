#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ infra ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set high_es_nb 10
set low_es_nb  50

set nb(EdgeServer) [expr $high_es_nb + $low_es_nb]
set nb(Cloud) 1
set nb(Gateway) 500
set nb(EndFogNode) 10000
set nb(Appliance) 20000

set proba_connect 1.0

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ app ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

