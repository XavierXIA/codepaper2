#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../../sharedSetup.tcl
source $scriptPath/setup.tcl

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ variable ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
array set links ""

array set es2aplcs ""
array set es2nodes ""
set topNodes ""

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

proc AddCloud {outFile idx} {
    global high_es_nb links topNodes

    AddFogNode $outFile Cloud $idx

    set i 0
    while {$i < $high_es_nb} {
        lappend links(Cloud-$idx) "EdgeServer-$i"
        incr i
    }

    lappend topNodes Cloud-$idx
}

proc AddEdgeServer {outFile idx} {
    global high_es_nb links topNodes es2nodes

    AddFogNode $outFile EdgeServer $idx
    if {$idx >= $high_es_nb} {
        lappend links(EdgeServer-$idx) [GetRandomHighES]

        lappend es2nodes(EdgeServer-$idx) EdgeServer-$idx
    } else {
        lappend topNodes EdgeServer-$idx
    }
}

proc AddGateway {outFile idx} {
    global links es2nodes

    AddFogNode $outFile Gateway $idx

    set es [GetRandomLowES]
    set links(Gateway-$idx) $es
    
    lappend es2nodes($es) Gateway-$idx
}

proc AddEndFogNode {outFile idx} {
    global proba_mobile links es2nodes

    if {rand() < $proba_mobile} {
        AddFogNode $outFile EndFogNode $idx

        set es [GetRandomLowES]
        set links(EndFogNode-$idx) $es

        lappend es2nodes($es) EndFogNode-$idx
    } else {
        set gatewayIdx [GetRandomGatewayIdx]
        AddFogNode $outFile EndFogNode $idx

        set links(EndFogNode-$idx) Gateway-$gatewayIdx

        lappend es2nodes($links(Gateway-$gatewayIdx)) EndFogNode-$idx
    }
}

proc AddAppliance {outFile idx} {
    global links es2aplcs

    puts $outFile "<appliance>"
    PutsProperty $outFile id "Appliance-$idx"
    puts $outFile "</appliance>"

    set gatewayIdx [GetRandomGatewayIdx]
    set links(Appliance-$idx) Gateway-$gatewayIdx

    lappend es2aplcs($links(Gateway-$gatewayIdx)) Appliance-$idx
}

proc GetRandomGatewayIdx {} {
    global nb

    return [expr round(floor($nb(Gateway) * rand()))]
}

proc GetRandomLowES {} {
    global low_es_nb high_es_nb

    return "EdgeServer-[expr $high_es_nb + round(floor($low_es_nb * rand()))]"
}

proc GetRandomHighES {} {
    global low_es_nb high_es_nb

    return "EdgeServer-[expr round(floor($high_es_nb * rand()))]"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ highEdgeServer connects ~~~~~~~~~~~~~~~~~~~~~~~~~~
set baseIdx 1
while {$baseIdx < $high_es_nb} {
    set proba $proba_connect
    set contain_list ""

    while {rand() < $proba} {
        set idx [expr round(floor($baseIdx * rand()))]
        while {$idx in $contain_list} {
            set idx [expr round(floor($baseIdx * rand()))]
        }
        lappend contain_list $idx

        lappend links(EdgeServer-$idx) "EdgeServer-$baseIdx"
        
        if {[llength $contain_list] == $baseIdx} {
            set proba 0
        } else {
            set proba [expr $proba/6]
        }
    }

    incr baseIdx
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~
set  outFile [GenXml $scriptPath/output/infra.xml]
puts $outFile "<infra>"

#devices
foreach type $dev_types {
    set i 0

    while {$i < $nb($type)} {
        eval "Add$type $outFile $i"
        incr i
    }
}

#links
foreach {src dsts} [array get links] {
    foreach dst $dsts {
        set link_type "[GetDeviceType $src],[GetDeviceType $dst]"

        set cmd "set l \"$lat($link_type)\"" 
        eval $cmd
        set cmd "set b \"$bw($link_type)\"" 
        eval $cmd

        PutsProperty $outFile link "$src $dst $b $l"

        PutsProperty $outFile link "$dst $src $b $l"
    }
}

puts $outFile "</infra>"
close $outFile

set outFile [open $scriptPath/output/infraInfo.tcl w+]

puts $outFile "array set es2aplcs \"\""
puts $outFile "array set es2nodes \"\""

foreach {es aplcs} [array get es2aplcs] {
    puts $outFile "set es2aplcs($es) {$aplcs}"
}

foreach {es aplcs} [array get es2nodes] {
    puts $outFile "set es2nodes($es) {$aplcs}"
}

puts $outFile "set topNodes {$topNodes}"

close $outFile

