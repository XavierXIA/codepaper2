#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../input/sharedSetup.tcl

set algos {CPLEX AFNO-DCOinf DAFNO-InitCO-DCO FirstFit}
foreach algo $algos {
    file delete -force $scriptPath/$algo
    catch {exec mkdir -p $scriptPath/$algo}
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
foreach algo $algos {
    catch {exec echo "wal,respTime,exeTime" > $algo/result.csv} msg
}

array set base ""
set i 0
while {$i < $infraNB} {
    foreach algo $algos {

        set wals ""
        set respTimes ""
        set exeTimes ""

        set j 0
        set testNB 0

        if {$algo eq "CPLEX"} {
            set base(wal) 1
            set base(simulate) 1
            set testNB 3
        } elseif {$algo eq "FirstFit"} {
            catch {exec cat $scriptPath/../objFunc/$i/wals} wals
            catch {exec cat $scriptPath/../objFunc/$i/respTimes} respTimes
            catch {exec cat $scriptPath/../objFunc/$i/exeTimes} exeTimes
        } elseif {$algo eq "AFNO-DCOinf"} {
            set testNB 10
        } elseif {$algo eq "DAFNO-InitCO-DCO"} {
            set testNB 3
        }    

        while {$j < $testNB} {

            set platform $scriptPath/$algo/platform${i}-${j}.xml
            set deploy   $scriptPath/$algo/deploy${i}-${j}.xml

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            set msg ""

            switch $algo {
                CPLEX {
                    catch {exec java -Djava.library.path=/opt/ibm/ILOG/CPLEX_Studio1271/cplex/bin/x86-64_linux -jar $scriptPath/$algo.jar -infra $scriptPath/../input/infra${i}.xml -app $scriptPath/../input/app.xml -SimgridPlatform $platform -SimgridDeploy $deploy $scriptPath/../input/appDU.txt} msg
                }
                AFNO-DCOinf {
                    catch {exec java -jar $scriptPath/../../../target/$algo.jar -infra $scriptPath/../input/infra${i}.xml -app $scriptPath/../input/app.xml -SimgridPlatform $platform -SimgridDeploy $deploy $scriptPath/../input/appDU.txt} msg
                }
                DAFNO-InitCO-DCO {
                    catch {exec java -jar $scriptPath/../../../target/DAFNO-DCO.jar -infra $scriptPath/../input/infra${i}.xml -app $scriptPath/../input/app.xml -initCO REQ_BW -SimgridPlatform $platform -SimgridDeploy $deploy $scriptPath/../input/appDU.txt} msg
                }
            }

            puts ""
            puts "$algo : $i : $j"

            catch {exec echo $msg | grep Score | awk "{print \$3}"} wal
            lappend wals $wal
            puts "  wal : $wal"

            catch {exec echo $msg | grep execution | awk "{print \$4}"} exeTime
            lappend exeTimes $exeTime
            puts "  exeTime : $exeTime"

            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ simulate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            catch {exec $scriptPath/../../simulate/build/simulate $platform $deploy 999} msg
            set respTime [GetAvgValue $msg]
            lappend respTimes $respTime
            puts "  respTime : $respTime"

            incr j
        }

        if {$algo eq "CPLEX"} {
            set base(wal) [GetAvgValue $wals]
            set base(respTime) [GetAvgValue $respTimes]
        }
        
        set j 0
        while {$j < $testNB} {
            set wal [expr [lindex $wals $j]/$base(wal)]
            set respTime [expr [lindex $respTimes $j]/$base(respTime)]
            catch {exec echo "$wal,$respTime,[lindex $exeTimes $j]" >> $algo/result.csv} msg

            incr j
        }
    }
    incr i
}

