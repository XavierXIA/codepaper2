#!/usr/bin/Rscript

library(plyr)
library(scales)

algos <- c("AFNO-DCOinf", "CPLEX", "DAFNO-InitCO-DCO", "FirstFit")

for (algo in algos) {
    df <- read.csv(paste(algo, "/result.csv", sep=""))
    df <- ddply(df, .(), summarize, avgWAL=mean(wal), sdWAL=sd(wal), avgResp=mean(respTime), sdResp=sd(respTime), avgExe=mean(exeTime), sdExe=sd(exeTime))
    
    print(algo)
    print(df)
}

