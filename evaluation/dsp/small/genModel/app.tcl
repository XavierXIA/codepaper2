#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]
source $scriptPath/../../sharedSetup.tcl
source $scriptPath/setup.tcl
source $scriptPath/output/infraInfo.tcl

namespace import ::tcl::mathfunc::max

set appIdx [lindex $argv 0]
set toAppIdx [lindex $argv 1]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc GetRandomDev {ES_idx} {
    global es2aplcs es2nodes topNodes

    set candidates [concat $es2nodes(EdgeServer-$ES_idx) $es2aplcs(EdgeServer-$ES_idx) $topNodes]

    return [lindex $candidates [expr round(floor([llength $candidates]*rand()))]]
}

proc GetDZ {ES_idx} {
    global es2nodes topNodes proba_devInDZ

    set cmd "set proba \"$proba_devInDZ\""
    eval $cmd

    set dz ""
    foreach node [concat $es2nodes(EdgeServer-$ES_idx) $topNodes] {
        if {rand() < $proba} {
            set dz "$dz $node"
        }
    }

    if {$dz eq ""} {
        puts "Warning : empty DZ generated, try to redo the generation !"
        return [GetDZ $ES_idx]
    } else {
        return [string trim $dz]
    }
}

proc GetRandomES_Idx {} {
    return 1
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~ output ~~~~~~~~~~~~~~~~~~~~~~~~~~
while {$appIdx <= $toAppIdx} {
    set outFile [GenXml $scriptPath/output/app${appIdx}.xml]
    puts $outFile "<apps>"
    puts $outFile "<app>"
    
    PutsProperty $outFile id "App$appIdx"

    set binds ""

    if {[info exists dataUnits]} {
        array unset dataUnits
    }
    array set dataUnits ""

    set ES_idx [GetRandomES_Idx]

    set typeIdx 0
    set lastInstNB ""
    set dataSrcs ""
    set dataCons ""

    set isDataSrcDev 1

    puts "app${appIdx}:"

    while {$typeIdx < $typeNB} {
        set instNB $instNBs($typeIdx)

        foreach type "CPU RAM DISK" {
            set cmd "set req$type \"$require($type)\""
            eval $cmd
        }

        set DZ ""
        if {rand() < $proba_specDZ} {
            set DZ [GetDZ $ES_idx]
        }

        #unmixed
        set isComp 1

        if {$typeIdx==0 || $typeIdx+1==$typeNB} {
            if {$isDataSrcDev && rand() < $proba_src_con_comp} {
                set isDataSrcDev 0
                puts "  comp"
            } else {
                set isComp 0
                puts "  dev"
            }
        }
    
        #comps of this type
        set optIdx 0
        while {$optIdx < $instNB} {
            #mixed
            #set isComp 1
            #if {($typeIdx==0 || $typeIdx+1==$typeNB) && rand() < $proba_src_con_dev} {
            #    set isComp 0
            #}

            if {$isComp} {
                set id "Comp:$appIdx-$typeIdx-$optIdx"

                puts $outFile "<comp>"
                PutsProperty $outFile id   $id
                PutsProperty $outFile CPU  $reqCPU
                PutsProperty $outFile RAM  $reqRAM
                PutsProperty $outFile DISK $reqDISK
                if {$DZ ne ""} {
                    PutsProperty $outFile DZ $DZ
                }
                puts $outFile "</comp>"

                if {$typeIdx==0} {
                    #dataSrc
                    lappend dataSrcs $id
                } elseif {$typeIdx+1==$typeNB} {
                    #dataCon
                    lappend dataCons $id
                }
            } else {
                set dev [GetRandomDev $ES_idx]

                if {$typeIdx==0} {
                    while {$dev in $dataSrcs} {
                        puts "device duplicated"
                        set dev [GetRandomDev $ES_idx]
                    }
                    lappend dataSrcs $dev
                } elseif {$typeIdx+1==$typeNB} {
                    while {$dev in $dataCons} {
                        puts "device duplicated"
                        set dev [GetRandomDev $ES_idx]
                    }
                    lappend dataCons $dev
                }
            }

            incr optIdx
        }

        #binds
        foreach type "BW LAT" {
            set cmd "set req$type \"$require($type)\""
            eval $cmd
        }

        regexp {^([0-9\.]+)[a-zA-Z]+$} $reqBW  vv reqBW_value
        set reqBW_value [expr $reqBW_value * 1000000]
        regexp {^([0-9\.]+)[a-zA-Z]+$} $reqCPU vv reqCPU_value
        set reqCPU_value [expr $reqCPU_value * 1000000000]

        if {$typeIdx == 1} {
            #connect data src
            set dataSrcNB [llength $dataSrcs]
            set bindNB [max $instNB $dataSrcNB]

            set i 0
            while {$i < $bindNB} {
                set comp    "Comp:$appIdx-$typeIdx-[expr $i%$instNB]"
                set dataSrc [lindex $dataSrcs [expr $i%$dataSrcNB]]

                lappend binds "$comp $dataSrc $reqBW $reqLAT"
                lappend binds "$dataSrc $comp $reqBW $reqLAT"
                

                set dataUnits(rec:$comp) $reqBW_value
                if {[info exists dataUnits(dataSrc:App${appIdx}:$dataSrc)]} {
                    set dataUnits(dataSrc:App${appIdx}:$dataSrc) "$dataUnits(dataSrc:App${appIdx}:$dataSrc) $comp"
                } else {
                    set dataUnits(dataSrc:App${appIdx}:$dataSrc) "$reqBW_value,[GetDataUnitSize $reqBW_value],[GetDataUnitCalAmount $reqCPU_value],$comp"
                }

                incr i
            }
        } elseif {$typeIdx+1 == $typeNB} {
            #connect data con
            foreach type "BW LAT" {
                set cmd "set req$type \"$require($type)\""
                eval $cmd
            }

            set bindNB [max $instNB $lastInstNB]

            set i 0
            while {$i < $bindNB} {
                set comp    "Comp:$appIdx-[expr $typeIdx-1]-[expr $i%$lastInstNB]"
                set dataCon [lindex $dataCons [expr $i%$instNB]]

                lappend binds "$comp $dataCon $reqBW $reqLAT"
                lappend binds "$dataCon $comp $reqBW $reqLAT"

                set dataUnits(dataCon:App${appIdx}:$dataCon) $reqBW_value
                if {[info exists dataUnits(operator:$comp)]} {
                    set dataUnits(operator:$comp) "$dataUnits(operator:$comp) App${appIdx}:$dataCon"
                } else {
                    set cal_size 0
                    if {$isComp} {
                        set cal_size [GetDataUnitCalAmount $reqCPU_value]
                    }

                    set dataUnits(operator:$comp) "$dataUnits(rec:$comp),$reqBW_value,[GetDataUnitSize $reqBW_value],$cal_size,App${appIdx}:$dataCon"
                    unset dataUnits(rec:$comp)
                }

                incr i
            }
        } elseif {$typeIdx > 1} {
            #connect comps
            set bindNB [max $lastInstNB $instNB]

            set i 0
            while {$i < $bindNB} {
                set comp1 "Comp:$appIdx-$typeIdx-[expr $i%$instNB]"
                set comp2 "Comp:$appIdx-[expr $typeIdx-1]-[expr $i%$lastInstNB]"

                lappend binds "$comp1 $comp2 $reqBW $reqLAT"
                lappend binds "$comp2 $comp1 $reqBW $reqLAT"

                set dataUnits(rec:$comp1) $reqBW_value
                if {[info exists dataUnits(operator:$comp2)]} {
                    set dataUnits(operator:$comp2) "$dataUnits(operator:$comp2) $comp1"
                } else {
                    set dataUnits(operator:$comp2) "$dataUnits(rec:$comp2),$reqBW_value,[GetDataUnitSize $reqBW_value],[GetDataUnitCalAmount $reqCPU_value],$comp1"
                    unset dataUnits(rec:$comp2)
                }

                incr i
            }
        }

        set lastInstNB $instNB
        incr typeIdx
    }
    
    #binds
    foreach bind $binds {
        PutsProperty $outFile bind $bind
    }
    
    puts $outFile "</app>"
    puts $outFile "</apps>"
    close $outFile
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~ DataUnitFile ~~~~~~~~~~~~~~~~~~~~~~~~~~
    set outFile [open $scriptPath/output/app${appIdx}DU.txt w+]
    foreach {sender paras} [array get dataUnits] {
        puts $outFile $sender
        puts $outFile $paras
    }
    close $outFile

    incr appIdx
}

