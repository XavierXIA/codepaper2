#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ infra ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set high_es_nb 1
set low_es_nb  2

set nb(EdgeServer) [expr $high_es_nb + $low_es_nb]
set nb(Cloud) 1
set nb(Gateway) 3
set nb(EndFogNode) 20
set nb(Appliance)  40

set proba_connect 1.0

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ app ~~~~~~~~~~~~~~~~~~~~~~~~~~~~

set typeNB 4
array set instNBs ""
set instNBs(0) 10
set instNBs(1) 8
set instNBs(2) 5
set instNBs(3) 3

