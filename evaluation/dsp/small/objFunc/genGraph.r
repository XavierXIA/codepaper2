#!/usr/bin/Rscript

library(ggplot2)

infraNB = 3 - 1

df <- read.csv("0/result.csv")

for(i in 1:infraNB)
{
    df <- rbind(df, read.csv(paste(i, "/result.csv", sep="")))
}

theme_set(theme_bw() + theme(axis.text = element_text(size=25), axis.title = element_text(size=24), strip.text = element_text(size=24), title = element_text(size=0)))

png("./evaObjFunc.png", width = 600, height = 240)
ggplot(df, aes(x=wal, y=respTime)) + labs(title = "", x = "WAL (ms)", y ="DSP Application\nResponse Time (ms)") + geom_point() + stat_smooth()# + scale_x_continuous(limits = c(0, 21.5))
dev.off()

setEPS()
postscript("evaObjFunc.eps", width = 600, height = 240)
ggplot(df, aes(x=wal, y=respTime)) + labs(title = "", x = "WAL (ms)", y ="DSP Application\nResponse Time (ms)") + geom_point() + stat_smooth()# + scale_x_continuous(limits = c(0, 21.5))
dev.off()

