#!/bin/sh
# The backslash makes the next line a comment in Tcl \
exec tclsh "$0" ${1+"$@"}

set scriptPath [file dirname [file normalize $argv0]]

source $scriptPath/../input/sharedSetup.tcl

set resultNB 100
set testNB 10

set resultsPerSimulate [expr $resultNB/$testNB]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set i 0
while {$i < $infraNB} {
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ init ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    file delete -force $scriptPath/$i
    catch {exec mkdir $i -p} msg

    catch {exec $scriptPath/../genModel/infra.tcl} msg
    if {$msg ne ""} {
        puts $msg
    }
    file copy -force $scriptPath/../genModel/output/infra.xml $scriptPath/$i/infra.xml
    file copy -force $scriptPath/../genModel/output/infra.xml $scriptPath/../input/infra${i}.xml

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ place ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    set j 0
    while {$j < $resultNB} {
        catch {exec $scriptPath/FirstFit.sh $scriptPath/$i $j} msg

        catch {exec echo $msg | grep finish} isFinish
        if {$isFinish eq "finish"} {
            puts "finish : $i : $j"
            catch {exec echo $msg | grep Score | awk "{print \$3}"} score
            catch {exec echo $msg | grep execution | awk "{print \$4}"} exeTime
            catch {exec echo "$score $exeTime $j" >> $i/tmp} msg

            incr j
        } else {
            puts $msg
        }
    }

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ simulate ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    set res ""
    set sorted ""
    catch {exec cat $i/tmp | sort -k 1n } sorted

    set j 0
    while {$j < $resultNB} {
        if {$j%$resultsPerSimulate == 0} {

            if {[expr $j+$resultsPerSimulate] == $resultNB} {
                set j [expr $resultNB - 1]
            }

            set wal [lindex $sorted [expr $j*3]]
            set exeTime [lindex $sorted [expr $j*3+1]]
            set idx [lindex $sorted [expr $j*3+2]]

            set platform $scriptPath/$i/platform${idx}.xml
            set deploy $scriptPath/$i/deploy${idx}.xml
            catch {exec ../../simulate/build/simulate $platform $deploy 999} msg

            set respTime [GetAvgValue $msg]
            
            lappend res "$wal,$respTime"
            catch {exec echo "$wal" >> $i/wals} msg
            catch {exec echo "$respTime" >> $i/respTimes} msg
            catch {exec echo "$exeTime" >> $i/exeTimes} msg
        }

        incr j
    }

    set outFile [open $i/result.csv w+]
    puts $outFile "wal,respTime"

    foreach line $res {
        puts $outFile $line
    }

    close $outFile

    incr i
}

