#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ proc ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
proc AddFogNode {outFile dev_type index} {
    global res

    puts $outFile "<fogNode>"

    PutsProperty $outFile id "$dev_type-$index"
    
    foreach res_type "CPU RAM DISK" {
        set cmd "set c $res($dev_type,$res_type)"
        eval $cmd

        PutsProperty $outFile $res_type $c
    }

    puts $outFile "</fogNode>"
}

proc AddComp {outFile type index DZ} {
    global appCnt

    puts $outFile "<comp>"

    PutsProperty $outFile id "app$appCnt:comp-$index"

    foreach res_type "CPU RAM DISK" {
        set cmd "set c $require($res_type)"
        eval $cmd

        PutsProperty $outFile $res_type $c
    }

    if {$DZ ne ""} {
        PutsProperty $outFile DZ $DZ
    }

    puts $outFile "</comp>"
}

proc GetDeviceType {id} {
    regexp {^([a-zA-Z]+)-[0-9]+$} $id vv type

    return $type
}

proc GetDeviceIdx {id} {
    regexp {^[a-zA-Z]+-([0-9]+)$} $id vv idx

    return $idx
}

proc GetDataUnitSize {reqBW} {
    return [expr (0.01+0.09*rand()) * $reqBW]
}

proc GetDataUnitCalAmount {reqCPU} {
    return [expr (0.01+0.09*rand()) * $reqCPU]
}

proc GenXml {fileName} {
    set path [file dirname [file normalize $fileName]]
    catch {exec mkdir -p $path} msg

    set outFile [open $fileName w+]
    puts $outFile "<?xml version=\"1.0\"?>"

    return $outFile
}

proc PutsProperty {outFile property value} {
    puts $outFile "  <$property>$value</$property>"
}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~device type res~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set dev_types "Cloud EdgeServer Gateway EndFogNode Appliance"

set res(Cloud,CPU)  999999Gf
set res(Cloud,RAM)  999999GB
set res(Cloud,DISK) 999999GB

set res(EdgeServer,CPU)  "\[expr 100  * rand()]Gf"
set res(EdgeServer,RAM)  "\[expr 500  * rand()]GB"
set res(EdgeServer,DISK) "\[expr 5000 * rand()]GB"

set res(Gateway,CPU)     "\[expr 8    * rand()]Gf"
set res(Gateway,RAM)     "\[expr 10   * rand()]GB"
set res(Gateway,DISK)    "\[expr 500  * rand()]GB"

set res(EndFogNode,CPU)  "\[expr 2    * rand()]Gf"
set res(EndFogNode,RAM)  "\[expr 4    * rand()]GB"
set res(EndFogNode,DISK) "\[expr 200  * rand()]GB"

set proba_mobile 0.2

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ link ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set lat(Cloud,EdgeServer)      "\[expr 30 + 70*rand()]ms"
set lat(EdgeServer,EdgeServer) "\[expr 3  + 7 *rand()]ms"
set lat(Gateway,EdgeServer)    "\[expr 1  + 19*rand()]ms"
set lat(EndFogNode,EdgeServer) "\[expr 10 + 15*rand()]ms"
set lat(EndFogNode,Gateway)    "\[expr 1  + 4 *rand()]ms"
set lat(Appliance,Gateway)     "\[expr 1  + 4 *rand()]ms"

set bw(Cloud,EdgeServer)      "\[expr 1000 * rand()]MBps"
set bw(EdgeServer,EdgeServer) "\[expr 1000 * rand()]MBps"
set bw(Gateway,EdgeServer)    "\[expr 100  * rand()]MBps"
set bw(EndFogNode,EdgeServer) "\[expr 100  * rand()]MBps"
set bw(EndFogNode,Gateway)    "\[expr 1000 * rand()]MBps"
set bw(Appliance,Gateway)     "\[expr 1000 * rand()]MBps"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ app ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set require(CPU)  "\[expr 0.1 + 0.9*rand()]Gf"
set require(RAM)  "\[expr 0.1 + 0.9*rand()]GB"
set require(DISK) "\[expr 2 * rand()]GB"
set require(LAT)  "\[expr 25 + 25*rand()]ms"
set require(BW)   "\[expr 0.01 + 0.99 * rand()]MBps"

set proba_specDZ 0.1
set proba_src_con_comp 0.5
set proba_devInDZ "\[expr 0.01 + 0.09*rand()]"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~ tcl ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
set tcl_precision 3

