package model.route;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import model.app.Entity;
import model.infra.Appliance;
import model.infra.Device;
import model.infra.FogNode;
import model.infra.InfraParser;
import model.infra.Infrastructure;
import model.infra.Link;

public class CustomRouteTable implements RouteTableImpl {
    private Route[][] routes = null;
    private final int devNB;

    private final Map<Integer, InterRoute> idx_interRoute_map = new HashMap<>();

    public CustomRouteTable(String nodeTypesNonTree) {
        devNB = InfraParser.getDevNB();

        init(nodeTypesNonTree);

        while (routeRemoteDevices())
            ;

        for (Entry<Integer, InterRoute> entry : new HashSet<>(idx_interRoute_map.entrySet())) {
            int key = entry.getKey();
            for (int i = 0; i < devNB; i++) {
                if (routes[key][i] != null && !idx_interRoute_map.containsKey(i)) {
                    idx_interRoute_map.put(i, entry.getValue());
                }
            }
        }

        for (int i = 0; i < devNB; i++) {
            routes[i][i] = new Route(0);
        }
    }

    private void init(String nodeTypesNonTree) {
        routes = new Route[devNB][devNB];
        for (int i = 0; i < devNB; i++) {
            for (int j = 0; j < devNB; j++) {
                routes[i][j] = null;
            }
        }

        Pattern p = Pattern.compile("^(" + nodeTypesNonTree + ")");

        for (FogNode n : Infrastructure.getFogNodes()) {
            for (Link l : n.outLinks) {
                boolean isSrcNonTree = p.matcher(l.src.id).find();
                boolean isDstNonTree = p.matcher(l.dst.id).find();

                if (isSrcNonTree && !isDstNonTree) {
                    if (idx_interRoute_map.containsKey(l.dst.index)) {
                        idx_interRoute_map.get(l.dst.index).setDownLink(l);
                    } else {
                        idx_interRoute_map.put(l.dst.index, new InterRoute(null, l));
                    }
                } else if (!isSrcNonTree && isDstNonTree) {
                    if (idx_interRoute_map.containsKey(l.src.index)) {
                        idx_interRoute_map.get(l.src.index).setUpLink(l);
                    } else {
                        idx_interRoute_map.put(l.src.index, new InterRoute(l, null));
                    }
                } else {
                    routes[l.src.index][l.dst.index] = new Route(l);
                }
            }
        }

        for (Appliance a : Infrastructure.getAppliances()) {
            for (Link l : a.outLinks) {
                routes[l.src.index][l.dst.index] = new Route(l);
            }
        }
    }

    private boolean routeRemoteDevices() {
        boolean res = false;

        for (int i = 0; i < devNB; i++) {
            for (int j = 0; j < devNB; j++) {
                if (routes[i][j] != null) {
                    for (int k = 0; k < devNB; k++) {
                        if (k != i && routes[j][k] != null) {
                            if (routes[i][k] == null
                                    || routes[i][k].lat > routes[i][j].lat + routes[j][k].lat) {
                                routes[i][k] = new Route(routes[i][j], routes[j][k]);
                                res = true;
                            }
                        }
                    }
                }
            }
        }

        return res;
    }

    @Override
    public Route getRoute(Device src, Device dst) {
        if (routes[src.index][dst.index] != null) {
            return routes[src.index][dst.index];
        }

        Entity coreSrc = src;
        Entity coreDst = dst;

        Route srcRoute = null;
        Route dstRoute = null;

        if (idx_interRoute_map.containsKey(src.index)) {
            Link interLink = idx_interRoute_map.get(src.index).getUpLink();
            coreSrc = interLink.dst;

            if (routes[src.index][coreSrc.index] == null) {
                routes[src.index][coreSrc.index] = new Route(routes[src.index][interLink.src.index], interLink);
            }

            srcRoute = routes[src.index][coreSrc.index];
        }

        if (idx_interRoute_map.containsKey(dst.index)) {
            Link interLink = idx_interRoute_map.get(dst.index).getDownLink();
            coreDst = interLink.src;

            if (routes[coreDst.index][dst.index] == null) {
                routes[coreDst.index][dst.index] = new Route(interLink, routes[interLink.dst.index][dst.index]);
            }

            dstRoute = routes[coreDst.index][dst.index];
        }

        Route tmp = routes[coreSrc.index][coreDst.index];

        if (srcRoute != null) {
            if (routes[src.index][coreDst.index] == null) {
                routes[src.index][coreDst.index] = new Route(srcRoute, tmp);
            }

            tmp = routes[src.index][coreDst.index];
        }

        if (dstRoute != null) {
            routes[src.index][dst.index] = new Route(tmp, dstRoute);
        }

        return routes[src.index][dst.index];
    }

    // public static int GetHopNB(Entity src, Entity dst)
    // {
    // return GetRoute(src, dst).getLinkList().size();
    // }
}

class InterRoute {
    private Link upLink;
    private Link downLink;

    public InterRoute(Link upLink, Link downLink) {
        this.upLink = upLink;
        this.downLink = downLink;
    }

    public void setDownLink(Link downLink) {
        this.downLink = downLink;
    }

    public Link getDownLink() {
        return downLink;
    }

    public void setUpLink(Link upLink) {
        this.upLink = upLink;
    }

    public Link getUpLink() {
        return upLink;
    }
}
