package model.route;

import model.infra.Device;

public class RouteTable {
    static RouteTableImpl routeTab = null;

    public static void genRoutes() {
        routeTab = new DynRouteTable();
    }

    public static void genRoutes(String highNodeTypesNonTree) {
        routeTab = new CustomRouteTable(highNodeTypesNonTree);
    }

    public static Route getRoute(Device src, Device dst) {
        return routeTab.getRoute(src, dst);
    }
}
