package model.route;

import java.util.HashSet;
import java.util.Set;

import model.infra.Device;
import model.infra.InfraParser;
import model.infra.Link;

import common.RunTimeCounter;
import common.Utils;

public class DynRouteTable implements RouteTableImpl {
    private Route[][] routes = null;

    public DynRouteTable() {
        int devNB = InfraParser.getDevNB();

        routes = new Route[devNB][devNB];
        for (int i = 0; i < devNB; i++) {
            for (int j = 0; j < devNB; j++) {
                routes[i][j] = null;
            }
        }

        for (int i = 0; i < devNB; i++) {
            routes[i][i] = new Route(0);
        }
    }

    @Override
    public Route getRoute(Device src, Device dst) {
        if (routes[src.index][dst.index] != null) {
            return routes[src.index][dst.index];
        }

        RunTimeCounter.pause();

        Set<Device> dev_set = new HashSet<>();
        Set<Device> tmp = new HashSet<>();
        Set<Device> visited = new HashSet<>();
        dev_set.add(src);
        int devNB = InfraParser.getDevNB();

        boolean isReach = false;

        while (!dev_set.isEmpty()) {
            double roundMinLat = Double.MAX_VALUE;

            for (Device d : dev_set) {
                for (Link l : d.outLinks) {
                    if (routes[src.index][l.dst.index] == null
                            || routes[src.index][l.dst.index].lat - (routes[src.index][l.src.index].lat + l.lat) > Utils.EPSILON) {
                        routes[src.index][l.dst.index] = new Route(routes[src.index][l.src.index], l);
                        tmp.add(l.dst);

                        if (roundMinLat - routes[src.index][l.dst.index].lat > Utils.EPSILON) {
                            roundMinLat = routes[src.index][l.dst.index].lat;
                        }

                        if (l.dst == dst) {
                            isReach = true;
                        }
                    } else if (Utils.doubleEqual(routes[src.index][l.dst.index].lat,
                            routes[src.index][l.src.index].lat + l.lat) && !visited.contains(l.dst)) {
                        tmp.add(l.dst);
                        visited.add(l.dst);

                        if (roundMinLat - routes[src.index][l.dst.index].lat > Utils.EPSILON) {
                            roundMinLat = routes[src.index][l.dst.index].lat;
                        }
                    }
                }
            }

            if (isReach && roundMinLat - routes[src.index][dst.index].lat > Utils.EPSILON) {
                for (int i = 0; i < devNB; i++) {
                    if (routes[src.index][i] != null && routes[src.index][i].lat - roundMinLat > Utils.EPSILON) {
                        routes[src.index][i] = null;
                    }
                }

                RunTimeCounter.start();
                return routes[src.index][dst.index];
            }

            dev_set.clear();
            dev_set.addAll(tmp);
            tmp.clear();
        }

        RunTimeCounter.start();
        return null;
    }
}