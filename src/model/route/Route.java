package model.route;

import java.util.ArrayList;
import java.util.List;

import model.infra.Link;

public class Route {
    public final double lat;
    private final List<Link> links;

    public Route(double lat) {
        this.lat = lat;
        links = new ArrayList<>();
    }

    public Route(Link link) {
        this(link.lat);
        links.add(link);
    }

    public Route(Route r1, Route r2) {
        this(r1.lat + r2.lat);

        links.addAll(r1.getLinkList());
        links.addAll(r2.getLinkList());
    }

    public Route(Route r1, Route r2, Route r3) {
        this(r1.lat + r2.lat + r3.lat);

        links.addAll(r1.getLinkList());
        links.addAll(r2.getLinkList());
        links.addAll(r3.getLinkList());
    }

    public Route(Route r, Link l) {
        this(r.lat + l.lat);

        links.addAll(r.getLinkList());
        links.add(l);
    }

    public Route(Link l, Route r) {
        this(l.lat + r.lat);

        links.add(l);
        links.addAll(r.getLinkList());
    }

    public List<Link> getLinkList() {
        return links;
    }

    public boolean contains(Link l) {
        return links.contains(l);
    }
}