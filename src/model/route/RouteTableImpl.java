package model.route;

import model.infra.Device;

public interface RouteTableImpl {
    public Route getRoute(Device src, Device dst);
}
