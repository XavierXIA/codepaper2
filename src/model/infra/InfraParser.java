package model.infra;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import common.Printer;
import common.Units;

public class InfraParser {
    private static int devNB = 0;

    public static void parse(String path) {
        assert !path.isEmpty() : "empty input infra model!";

        try {
            File file = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();

            Element infra = (Element) doc.getElementsByTagName("infra").item(0);

            // device
            NodeList nodeList = infra.getElementsByTagName("fogNode");
            for (int i = 0; i < nodeList.getLength(); i++) {
                createFogNode((Element) nodeList.item(i));
            }

            // appliance
            nodeList = infra.getElementsByTagName("appliance");
            for (int i = 0; i < nodeList.getLength(); i++) {
                createAppliance((Element) nodeList.item(i));
            }

            // link
            nodeList = infra.getElementsByTagName("link");
            for (int i = 0; i < nodeList.getLength(); i++) {
                createLink((Element) nodeList.item(i));
            }
        } catch (Exception e) {
            Printer.err("Infra Parse Exception :");
            e.printStackTrace();
        }
    }

    private static void createFogNode(Element e) {
        String id = e.getElementsByTagName("id").item(0).getFirstChild().getNodeValue();
        double cpu = Units.CPU2Double(e.getElementsByTagName("CPU").item(0).getFirstChild().getNodeValue());
        double ram = Units.RAM2Double(e.getElementsByTagName("RAM").item(0).getFirstChild().getNodeValue());
        double disk = Units.DISK2Double(e.getElementsByTagName("DISK").item(0).getFirstChild().getNodeValue());

        FogNode n = new FogNode(id, devNB, cpu, ram, disk);// , speed, core
        Infrastructure.addFogNode(n);
        devNB++;

        NodeList labelList = e.getElementsByTagName("DZlabel");
        for (int i = 0; i < labelList.getLength(); i++) {
            String label = labelList.item(i).getFirstChild().getNodeValue().intern();
            Infrastructure.addNode2DZ(label, n);
        }
    }

    private static void createAppliance(Element e) {
        String id = e.getElementsByTagName("id").item(0).getFirstChild().getNodeValue();
        Infrastructure.addAppliance(new Appliance(id, devNB));
        devNB++;
    }

    private static void createLink(Element e) {
        String s[] = e.getFirstChild().getNodeValue().split(" ");

        Infrastructure.addLink(new Link(Infrastructure.getDevFromId(s[0], true), Infrastructure
                .getDevFromId(s[1], true), Units.LAT2Double(s[3]), Units.BW2Double(s[2])));
    }

    public static int getDevNB() {
        return devNB;
    }
}
