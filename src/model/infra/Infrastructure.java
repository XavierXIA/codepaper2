package model.infra;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Infrastructure {
    private static final Map<String, FogNode> id_node_map = new HashMap<>();
    private static final Map<String, Appliance> id_aplc_map = new HashMap<>();

    private static final Map<String, Set<FogNode>> label_DZ_map = new HashMap<String, Set<FogNode>>();

    public static void addFogNode(FogNode n) {
        FogNode originalNode = id_node_map.put(n.id, n);
        assert originalNode==null : "addFogNode : FogNode id duplicated!";
    }

    public static void addAppliance(Appliance a) {
        Appliance originalAplc = id_aplc_map.put(a.id, a);
        assert originalAplc==null : "addAppliance : Appliance id duplicated!";
    }

    public static void addLink(Link l) {
        l.src.addSrcLink(l);
    }

    public static int getNodeNB() {
        return id_node_map.size();
    }

    public static int getAplcNB() {
        return id_aplc_map.size();
    }

    public static Device getDevFromId(String id, boolean isNodeFirst) {
        assert id!=null && !id.isEmpty() : "getDevFromId : input null/empty id!";

        Device res = null;

        if (isNodeFirst) {
            res = id_node_map.get(id);
        }

        if (res == null) {
            res = id_aplc_map.get(id);
        }

        if (!isNodeFirst && res == null) {
            res = id_node_map.get(id);
        }

        assert res!=null : "getDevFromId : null device returned!";
        return res;
    }

    public static Collection<FogNode> getFogNodes() {
        return id_node_map.values();
    }

    public static Collection<Appliance> getAppliances() {
        return id_aplc_map.values();
    }

    public static void addNode2DZ(String label, FogNode n) {
        assert !label.isEmpty() : "addNode2DZ : empty DZ label!";

        if (!label_DZ_map.containsKey(label)) {
            label_DZ_map.put(label, new HashSet<FogNode>());
        }

        label_DZ_map.get(label).add(n);
    }

    public static Collection<FogNode> getDZ(String label) {
        Collection<FogNode> DZ = label_DZ_map.get(label);
        assert DZ!=null : "getDZ : null dz returned!";

        return DZ;
    }
}