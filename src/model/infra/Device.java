package model.infra;

import java.util.HashSet;
import java.util.Set;

import model.app.Entity;

public class Device extends Entity {
    public final Set<Link> outLinks;

    public Device(String id, int idx) {
        super(id, idx);

        outLinks = new HashSet<>();
    }

    public void addSrcLink(Link l) {
        outLinks.add(l);
    }
}
