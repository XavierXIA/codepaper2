package model.infra;

import model.app.Binding;

import common.Printer;
import common.Utils;

public class Link {
    public final Device src;
    public final Device dst;

    public final double lat;
    public final double capBW;

    private double avBW; // current available bw (always up to date)

    public Link(Device src, Device dst, double lat, double bw) {
        assert Utils.EPSILON + lat > 0 : "Link : negative lat!";

        this.src = src;
        this.dst = dst;

        this.lat = lat;
        this.capBW = bw;

        avBW = bw;
    }

    // return true if fail
    public boolean place(Binding bind) {
        avBW -= bind.reqBW;
        return avBW < 0;
    }

    public void deplace(Binding bind) {
        avBW += bind.reqBW;
    }

    // return true if fail
    public boolean test() {
        if (!Utils.doubleEqual(capBW, avBW)) {
            Printer.err("cap:" + capBW + " , av:" + avBW);
            return true;
        }

        return false;
    }

    public void print() {
        System.out.println(src.id + " -> " + dst.id);
        System.out.println("bw : " + avBW);
    }
}
