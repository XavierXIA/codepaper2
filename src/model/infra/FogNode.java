package model.infra;

import model.app.Component;

import common.Printer;
import common.Utils;

public class FogNode extends Device {
    public final double capCPU;

    private double avCPU;
    private double avRAM;
    private double avDISK;

    public FogNode(String id, int idx, double cpu, double ram, double disk) {
        super(id, idx);

        capCPU = cpu;

        avCPU = cpu;
        avRAM = ram;
        avDISK = disk;
    }

    //return true if fail
    public boolean place(Component comp) {
        if (avCPU < comp.reqCPU || avRAM < comp.reqRAM || avDISK < comp.reqDISK) {
            return true;
        } else {
            avCPU -= comp.reqCPU;
            avRAM -= comp.reqRAM;
            avDISK -= comp.reqDISK;
        }

        return false;
    }

    public void deplace(Component comp) {
        avCPU += comp.reqCPU;
        avRAM += comp.reqRAM;
        avDISK += comp.reqDISK;
    }

    // return true if fail
    public boolean test() {
        if (!Utils.doubleEqual(avCPU, capCPU)) {
            Printer.err("cpu cap:" + capCPU + " , av:" + avCPU);
            return true;
        }

        return false;
    }

    public void print() {
        System.out.println(id + " :");
        System.out.println("  cpu " + avCPU);
        System.out.println("  ram " + avRAM);
        System.out.println("  disk " + avDISK);
    }
}