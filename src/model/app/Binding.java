package model.app;

import common.Utils;

public class Binding {
    public final Entity src;
    public final Entity dst;

    public final double maxLAT;
    public final double reqBW;

    public Binding(Entity src, Entity dst, double lat, double bw) {
        assert Utils.EPSILON + lat > 0 : "Component : negative lat requirement!";
        assert Utils.EPSILON + bw > 0 : "Component : negative bw requirement!";

        this.src = src;
        this.dst = dst;

        this.maxLAT = lat;
        this.reqBW = bw;
    }
}