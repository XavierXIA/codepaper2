package model.app;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import model.infra.FogNode;
import model.infra.Infrastructure;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import common.Printer;
import common.Units;

public class AppParser {
    private static int totalCompNB = 0;
    private static int consideredAppNB = Integer.MAX_VALUE;

    public static void parse(String path) {
        assert !path.isEmpty() : "empty input app model!";
        Application currApp = null;

        try {
            File file = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();

            NodeList appList = doc.getElementsByTagName("app");
            for (int i = 0; i < appList.getLength() && i < consideredAppNB; i++) {
                Element app = (Element) appList.item(i);
                currApp = Application.addApp(app.getElementsByTagName("id").item(0).getFirstChild().getNodeValue());

                //components
                NodeList nodeList = app.getElementsByTagName("comp");
                for (int j = 0; j < nodeList.getLength(); j++) {
                    createComp(currApp, (Element) nodeList.item(j), totalCompNB);
                    totalCompNB++;
                }

                //bindings
                nodeList = app.getElementsByTagName("bind");
                for (int j = 0; j < nodeList.getLength(); j++) {
                    createBind(currApp, (Element) nodeList.item(j));
                }
            }
        } catch (Exception e) {
            Printer.err("App Parse Exception :");
            e.printStackTrace();
        }
    }

    private static void createComp(Application app, Element e, int compIdx) {
        List<FogNode> DZ = null;
        if (e.getElementsByTagName("DZlabel").getLength() > 0) {
            DZ = new ArrayList<>(getDZfromLabel(e.getElementsByTagName("DZlabel").item(0).getFirstChild()
                    .getNodeValue()));
        } else if (e.getElementsByTagName("DZ").getLength() > 0) {
            DZ = getDZfromIDs(e.getElementsByTagName("DZ").item(0).getFirstChild().getNodeValue());
        } else {
            DZ = new ArrayList<>(Infrastructure.getFogNodes());
        }
        Collections.shuffle(DZ);

        Component comp = new Component(e.getElementsByTagName("id").item(0).getFirstChild().getNodeValue(), compIdx,
                Units.CPU2Double(e.getElementsByTagName("CPU").item(0).getFirstChild().getNodeValue()),
                Units.RAM2Double(e.getElementsByTagName("RAM").item(0).getFirstChild().getNodeValue()),
                Units.DISK2Double(e.getElementsByTagName("DISK").item(0).getFirstChild().getNodeValue()), DZ);
        app.addComp(comp);
    }

    private static List<FogNode> getDZfromIDs(String s) {
        List<FogNode> res = new ArrayList<>();

        String[] nodeIDs = s.split(" ");
        for (String id : nodeIDs) {
            res.add((FogNode) Infrastructure.getDevFromId(id, true));
        }

        return res;
    }

    // TODO : and or
    private static Set<FogNode> getDZfromLabel(String s) {
        Set<FogNode> res = new HashSet<>();

        String[] labels = s.split("\\|");
        for (String label : labels) {
            label = label.trim();
            res.addAll(Infrastructure.getDZ(label));
        }

        return res;
    }

    private static void createBind(Application app, Element e) {
        String[] s = e.getFirstChild().getNodeValue().split(" ");

        app.addBind(new Binding(app.getEntityFromId(s[0]), app.getEntityFromId(s[1]), Units.LAT2Double(s[3]),
                Units.BW2Double(s[2])));
    }

    public static int getTotalCompNB() {
        return totalCompNB;
    }

    public static void setConsideredAppNB(String nb) {
        consideredAppNB = Integer.parseInt(nb);
    }
}