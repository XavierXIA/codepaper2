package model.app;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.infra.Device;
import model.infra.FogNode;
import model.infra.Infrastructure;
import model.route.RouteTable;

import common.Printer;
import common.Utils;

public class Component extends Entity {
    public final double reqCPU;
    public final double reqRAM;
    public final double reqDISK;

    public final List<FogNode> DZ;

    public final Set<Binding> outCompBinds = new HashSet<>();
    public final Set<Binding> inCompBinds = new HashSet<>();
    public final Set<Binding> outAplcBinds = new HashSet<>();
    public final Set<Binding> inAplcBinds = new HashSet<>();

    private double bw2comp = 0.0;
    private double bw2aplc = 0.0;

    public Component(String id, int idx, double cpu, double ram, double disk, List<FogNode> DZ) {
        super(id, idx);

        assert Utils.EPSILON + cpu > 0 : "Component : negative cpu requirement!";
        assert Utils.EPSILON + ram > 0 : "Component : negative ram requirement!";
        assert Utils.EPSILON + disk > 0 : "Component : negative disk requirement!";

        if (DZ.isEmpty()) {
            Printer.warning("empty DZ!");
        }

        reqCPU = cpu;
        reqRAM = ram;
        reqDISK = disk;

        this.DZ = DZ;
    }

    public void addOutCompBind(Binding b) {
        outCompBinds.add(b);
        bw2comp += b.reqBW;
    }

    public void addInCompBind(Binding b) {
        inCompBinds.add(b);
        bw2comp += b.reqBW;
    }

    public void addOutAplcBind(Binding b) {
        outAplcBinds.add(b);
        bw2aplc += b.reqBW;
    }

    public void addInAplcBind(Binding b) {
        inAplcBinds.add(b);
        bw2aplc += b.reqBW;
    }

    public boolean isInDZ(FogNode n) {
        if (DZ.size() == Infrastructure.getNodeNB()) {
            return true;
        }

        return DZ.contains(n);
    }

    public FogNode getClosestNodeInDZ(Device target) {
        return getClosestNode(target, DZ);
    }

    public int getCompBindNB() {
        return outCompBinds.size() + inCompBinds.size();
    }

    public Set<Component> getBoundComps() {
        Set<Component> res = new HashSet<>();

        for (Binding b : inCompBinds) {
            res.add((Component) b.src);
        }
        for (Binding b : outCompBinds) {
            res.add((Component) b.dst);
        }

        return res;
    }

    public double getBw2Comp() {
        return bw2comp;
    }

    public double getBw2Aplc() {
        return bw2aplc;
    }

    public double getTotalBW() {
        return bw2comp + bw2aplc;
    }

    public static FogNode getClosestNode(Device target, Collection<FogNode> nodes) {
        if (nodes.contains(target)) {
            return (FogNode) target;
        }

        FogNode res = null;
        double min = Double.MAX_VALUE;

        for (FogNode n : nodes) {
            double lat = RouteTable.getRoute(n, target).lat + RouteTable.getRoute(target, n).lat;

            if (min - lat > Utils.EPSILON) {
                res = n;
                min = lat;
            }
        }

        return res;
    }
}