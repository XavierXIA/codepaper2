package model.app;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.infra.Infrastructure;

public class Application {
    private static final List<Application> apps = new ArrayList<>();
    private static double totalBW = 0.0;

    private final Map<String, Component> id_comp_map;
    public final String id;

    private Application(String id) {
        id_comp_map = new HashMap<>();
        this.id = id;
    }

    public static Application addApp(String id) {
        Application app = new Application(id);
        apps.add(app);

        return app;
    }

    public void addComp(Component c) {
        Component originalComp = id_comp_map.put(c.id, c);
        assert originalComp==null : "addComp : Component id duplicated!";
    }

    public void addBind(Binding b) {
        totalBW += b.reqBW;

        if (!(b.src instanceof Component)) {
            ((Component) b.dst).addInAplcBind(b);
            return;
        }

        if (!(b.dst instanceof Component)) {
            ((Component) b.src).addOutAplcBind(b);
            return;
        }

        ((Component) b.src).addOutCompBind(b);
        ((Component) b.dst).addInCompBind(b);
    }

    public Entity getEntityFromId(String id) {
        Entity res = id_comp_map.get(id);

        if (res == null) {
            return Infrastructure.getDevFromId(id, false);
        }

        return res;
    }

    public Collection<Component> getComps() {
        return id_comp_map.values();
    }

    public int getCompNB() {
        return id_comp_map.size();
    }

    public boolean contains(Component comp) {
        return id_comp_map.containsValue(comp);
    }

    public static List<Application> getApps() {
        return apps;
    }

    public static double getTotalBW() {
        return totalBW;
    }
}