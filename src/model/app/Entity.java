package model.app;

public class Entity {
    public final String id;
    public final int index;

    public Entity(String id, int idx) {
        assert !id.isEmpty() : "Entity : empty id!";

        this.id = id;
        this.index = idx;
    }

    @Override
    public int hashCode() {
        return index;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }
}