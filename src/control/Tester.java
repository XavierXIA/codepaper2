package control;

import model.infra.Appliance;
import model.infra.FogNode;
import model.infra.Infrastructure;
import model.infra.Link;
import search.Placer;

import common.Printer;

public class Tester {
    public static void Test(Placer placer) {
        placer.reset();

        boolean isFail = false;
        for (FogNode n : Infrastructure.getFogNodes()) {
            if (n.test()) {
                isFail = true;
            }

            for (Link l : n.outLinks) {
                if (l.test()) {
                    isFail = true;
                }
            }
        }

        for (Appliance a : Infrastructure.getAppliances()) {
            for (Link l : a.outLinks) {
                if (l.test()) {
                    isFail = true;
                }
            }
        }

        if (!isFail) {
            Printer.pass("test pass.");
        }
    }
}
