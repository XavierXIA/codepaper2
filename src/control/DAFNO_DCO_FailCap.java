package control;

import java.text.DecimalFormat;

import search.Placer;
import search.FNO.DAFNO_Evolver;
import search.FailCap.DAFNO_DCO_FailCap_Placer;

import common.Printer;

public class DAFNO_DCO_FailCap {
    public static void main(String args[]) {
        SharedProc.init(args);

        long startTime = System.nanoTime();

        DAFNO_Evolver evolver = new DAFNO_Evolver(true);
        evolver.applyInitOrder();

        DAFNO_DCO_FailCap_Placer.setEvolver(evolver);

        Placer placer = new DAFNO_DCO_FailCap_Placer(SharedProc.initCO, SharedProc.failNB, SharedProc.stepLen);
        boolean isFail = placer.search();

        DecimalFormat formatter = new DecimalFormat("#.###");
        Printer.info("execution time : " + formatter.format((System.nanoTime() - startTime) / 1e9));

        evolver.finish();

        SharedProc.conclude(isFail, placer);
    }
}
