package control;

import java.text.DecimalFormat;

import search.DAFNO_Placer;
import search.Placer;
import search.FNO.DAFNO_Evolver;

import common.Printer;

public class DAFNO {
    public static void main(String args[]) {
        SharedProc.init(args);

        long startTime = System.nanoTime();

        DAFNO_Evolver evolver = new DAFNO_Evolver(false);
        evolver.applyInitOrder();

        Placer placer = new DAFNO_Placer(SharedProc.initCO);
        DAFNO_Placer.setEvolver(evolver);

        boolean isFail = placer.search();

        DecimalFormat formatter = new DecimalFormat("#.###");
        Printer.info("execution time : " + formatter.format((System.nanoTime() - startTime) / 1e9));

        evolver.finish();

        SharedProc.conclude(isFail, placer);
    }
}
