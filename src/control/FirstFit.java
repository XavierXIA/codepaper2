package control;

import java.text.DecimalFormat;

import search.Placer;

import common.Printer;

public class FirstFit {
    public static void main(String args[]) {
        SharedProc.init(args);

        long startTime = System.nanoTime();

        Placer placer = new Placer(SharedProc.initCO);
        boolean isFail = placer.search();

        DecimalFormat formatter = new DecimalFormat("#.###");
        Printer.info("execution time : " + formatter.format((System.nanoTime() - startTime) / 1e9));

        SharedProc.conclude(isFail, placer);
    }
}
