package control;

import model.app.AppParser;
import model.infra.InfraParser;
import model.infra.Infrastructure;
import model.route.RouteTable;
import search.Placer;
import search.Placer.CompInitOrder;
import search.Solution;
import simulation.DSP_DeployGenerator;
import simulation.DeployGenerator;
import simulation.PlatformGenerator;
import simulation.SB_DeployGenerator;

import common.Printer;
import common.Utils;

public class SharedProc {
    static String appPath = "";
    static String infraPath = "";
    static String nodeTypesNonTree = "cloud|pop"; //"EdgeServer|Cloud";

    static double stepLen = 1;
    static int failNB = 50;
    static boolean isTest = false;
    static CompInitOrder initCO = CompInitOrder.RANDOM;

    static boolean isPrintRes = false;

    static String useCase = "SmartBell";

    public static void init(String args[]) {
        // parse parameters
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
            case "-infra":
                i++;
                infraPath = args[i];
                break;

            case "-app":
                i++;
                appPath = args[i];
                break;

            case "-appNB":
                i++;
                AppParser.setConsideredAppNB(args[i]);
                break;

            case "-stepLen":
                i++;
                stepLen = Double.parseDouble(args[i]);

                assert stepLen+Utils.EPSILON>0 && stepLen<1+Utils.EPSILON : "Invalid stepLen value, 0 < stepLen < 1";

                break;

            case "-failNB":
                i++;
                failNB = Integer.parseInt(args[i]);

                assert failNB > 0 : "Invalid failNB value, failNB > 0";

                break;

            case "-initCO":
                i++;
                switch (args[i]) {
                case "REQ_BW":
                    initCO = CompInitOrder.REQ_BW;
                    break;
                case "DZ_DEV_NB":
                    initCO = CompInitOrder.DZ_NODE_NB;
                    break;
                case "DZ_RES":
                    initCO = CompInitOrder.DZ_RES;
                    break;
                case "RANDOM":
                    break;
                default:
                    Printer.err("undefined -initCO option: " + args[i]);
                }
                break;

            case "-nodeTypesNonTree":
                i++;
                nodeTypesNonTree = args[i];
                break;

            case "-SimgridPlatform":
                i++;
                PlatformGenerator.setOutPath(args[i]);
                break;

            case "-SimgridDeploy":
                i++;
                DeployGenerator.setPaths(args[i], args[i + 1]);
                i++;
                break;

            case "-UseCase":
                i++;
                useCase = args[i];
                break;

            case "-test":
                isTest = true;
                break;

            case "-color":
                Printer.enableColor();
                break;

            case "-PrintRes":
                isPrintRes = true;
                break;

            default:
                Printer.err("undefined option: " + args[i]);
            }
        }

        InfraParser.parse(infraPath);
        Printer.info("FogNode NB : " + Infrastructure.getNodeNB());
        Printer.info("Appliance NB : " + Infrastructure.getAplcNB());

        if (nodeTypesNonTree.isEmpty()) {
            RouteTable.genRoutes();
        } else {
            RouteTable.genRoutes(nodeTypesNonTree);
        }

        AppParser.parse(appPath);
        Printer.info("Comp NB : " + AppParser.getTotalCompNB());

        System.gc();
    }

    public static void conclude(boolean isFail, Placer placer) {
        if (!isFail) {
            Printer.info("Score : " + placer.getWAL());
            Printer.info("AL : " + placer.getAverageLatency());
            Printer.info("Diameter : " + placer.getDiameter());
            Solution solution = new Solution(placer);

            if (isPrintRes) {
                solution.print();
            }

            if (!PlatformGenerator.getOutPath().isEmpty()) {
                // PlatformGenerator.saveFloydPlatform();
                PlatformGenerator.saveFullPlatform(solution);
            }

            if (!DeployGenerator.getOutPath().isEmpty()){
                DeployGenerator deployGen = null;
                if (useCase.equals("SmartBell")) {
                    deployGen = new SB_DeployGenerator();
                } else if (useCase.equals("DSP")) {
                    deployGen = new DSP_DeployGenerator();
                } else {
                    Printer.err("unexpected use case : " + useCase);
                }

                deployGen.saveDeployment(solution);
            }
        } else {
            Printer.info("Score : -1");
        }

        if (SharedProc.isTest) {
            Tester.Test(placer);
        }
    }
}
