package control;

import java.text.DecimalFormat;

import search.Placer;
import search.DCO.DCO_Placer;

import common.Printer;

public class DCO {
    public static void main(String args[]) {
        SharedProc.init(args);

        long startTime = System.nanoTime();

        Placer placer = new DCO_Placer(SharedProc.initCO, SharedProc.stepLen);
        boolean isFail = placer.search();

        DecimalFormat formatter = new DecimalFormat("#.###");
        Printer.info("execution time : " + formatter.format((System.nanoTime() - startTime) / 1e9));

        SharedProc.conclude(isFail, placer);
    }
}
