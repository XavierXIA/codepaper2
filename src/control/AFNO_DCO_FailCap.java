package control;

import java.text.DecimalFormat;

import search.Placer;
import search.FNO.AFNO_Evolver;
import search.FailCap.DCO_FailCap_Placer;

import common.Printer;

public class AFNO_DCO_FailCap {
    public static void main(String args[]) {
        SharedProc.init(args);

        long startTime = System.nanoTime();

        AFNO_Evolver evolver = new AFNO_Evolver();
        evolver.applyInitOrder();

        Placer placer = new DCO_FailCap_Placer(SharedProc.initCO, SharedProc.failNB, SharedProc.stepLen);
        boolean isFail = placer.search();

        DecimalFormat formatter = new DecimalFormat("#.###");
        Printer.info("execution time : " + formatter.format((System.nanoTime() - startTime) / 1e9));

        evolver.finish();

        SharedProc.conclude(isFail, placer);
    }
}
