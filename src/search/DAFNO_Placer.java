package search;

import model.app.AppParser;
import model.app.Component;
import model.infra.Device;
import search.FNO.DAFNO_Evolver;

public class DAFNO_Placer extends Placer {
    private static DAFNO_Evolver dynEvolver;
    private Device[] appliedTarget_tab;

    public DAFNO_Placer(CompInitOrder criteria) {
        super(criteria);
        init();
    }

    private void init() {
        appliedTarget_tab = new Device[AppParser.getTotalCompNB()];
        for (int i = 0; i < AppParser.getTotalCompNB(); i++) {
            appliedTarget_tab[i] = null;
        }
    }

    @Override
    public boolean search() {
        Component comp;
        CompPlacer tester;

        CompPlacer.setCurrPlacer(this);

        while (hasNext()) {
            comp = nextComp();
            tester = focusCompPlacer(comp);

            if (appliedTarget_tab[comp.index] != dynEvolver.getTarget(comp)) {
                dynEvolver.sortCandidateNodes(comp);
                appliedTarget_tab[comp.index] = dynEvolver.getTarget(comp);
            }

            while (tester.nextSubSolution()) {
                comp = previous();

                if (comp == null)
                    return true;

                dynEvolver.setUnplaced(comp);
                tester = focusCompPlacer(comp);
            }

            dynEvolver.setPlaced(comp, getHost(comp));
        }

        return false;
    }

    @Override
    public void finish() {
        super.finish();
        appliedTarget_tab = null;
    }

    public static void setEvolver(DAFNO_Evolver evolver) {
        DAFNO_Placer.dynEvolver = evolver;
    }
}