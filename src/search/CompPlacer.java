package search;

import java.util.List;

import model.app.Binding;
import model.app.Component;
import model.infra.Device;
import model.infra.FogNode;
import model.infra.Link;
import model.route.RouteTable;

import common.Utils;

public class CompPlacer {
    private final List<FogNode> node_list;
    protected int currIdx = -1;

    protected boolean placed = false;

    protected static Component currComp = null;
    protected static Placer currPlacer = null;

    // private static Timer timer = null;

    protected CompPlacer(Component comp) {
        node_list = comp.DZ;
    }

    // not thread safe
    // return true if fail
    public boolean nextSubSolution() {
        do {
            if (hasNext()) {
                next();
                place();
            } else {
                reset();
                return true;
            }
        } while (!placed);

        return false;
    }

    // not thread safe
    // return true if fail
    public boolean nextSubSolution(int latFailCeil) {
        if (latFailCeil < 0) {
            return nextSubSolution();
        }

        int count = -1;
        do {
            if (hasNext()) {
                next();
                if (place()) {
                    if (count >= 0) {
                        count++;
                        if (count == latFailCeil) {
                            reset();
                            return true;
                        }
                    }
                } else {
                    count = 0;
                }
            } else {
                reset();
                return true;
            }
        } while (!placed);

        return false;
    }

    private boolean hasNext() {
        return currIdx + 1 < node_list.size();
    }

    private void next() {
        deplace();

        currIdx++;
    }

    // return true if fail caused by lat
    private boolean place() {
        if (verifyLat()) {
            return true;
        }

        if (getCurrHost().place(currComp)) {
            return false;
        }

        if (verifyBW()) {
            getCurrHost().deplace(currComp);
            return false;
        }

        placed = true;
        return false;
    }

    // return true if fail
    private boolean verifyLat() {
        FogNode host = getCurrHost();

        for (Binding b : currComp.outCompBinds) {
            if (currPlacer.isPlaced((Component) b.dst)) {
                if (RouteTable.getRoute(host, currPlacer.getHost((Component) b.dst)).lat - b.maxLAT > Utils.EPSILON) {
                    return true;
                }
            }
        }

        for (Binding b : currComp.inCompBinds) {
            if (currPlacer.isPlaced((Component) b.src)) {
                if (RouteTable.getRoute(currPlacer.getHost((Component) b.src), host).lat - b.maxLAT > Utils.EPSILON) {
                    return true;
                }
            }
        }

        for (Binding b : currComp.outAplcBinds) {
            if (RouteTable.getRoute(getCurrHost(), (Device) b.dst).lat - b.maxLAT > Utils.EPSILON) {
                rmCurrCandNode();
                return true;
            }
        }

        for (Binding b : currComp.inAplcBinds) {
            if (RouteTable.getRoute((Device) b.src, getCurrHost()).lat - b.maxLAT > Utils.EPSILON) {
                rmCurrCandNode();
                return true;
            }
        }

        return false;
    }

    // return true if fail
    private boolean verifyBW() {
        boolean res = false;
        FogNode host = getCurrHost();

        for (Binding bind : currComp.outAplcBinds) {
            for (Link link : RouteTable.getRoute(host, (Device) bind.dst).getLinkList()) {
                if (link.place(bind)) {
                    res = true;
                }
            }
        }

        for (Binding bind : currComp.inAplcBinds) {
            for (Link link : RouteTable.getRoute((Device) bind.src, host).getLinkList()) {
                if (link.place(bind)) {
                    res = true;
                }
            }
        }

        for (Binding bind : currComp.outCompBinds) {
            if (currPlacer.isPlaced((Component) bind.dst)) {
                FogNode dst = currPlacer.getHost((Component) bind.dst);

                for (Link link : RouteTable.getRoute(host, dst).getLinkList()) {
                    if (link.place(bind)) {
                        res = true;
                    }
                }
            }
        }

        for (Binding bind : currComp.inCompBinds) {
            if (currPlacer.isPlaced((Component) bind.src)) {
                FogNode src = currPlacer.getHost((Component) bind.src);

                for (Link link : RouteTable.getRoute(src, host).getLinkList()) {
                    if (link.place(bind)) {
                        res = true;
                    }
                }
            }
        }

        if (res) {
            for (Binding bind : currComp.outAplcBinds) {
                for (Link link : RouteTable.getRoute(getCurrHost(), (Device) bind.dst).getLinkList()) {
                    link.deplace(bind);
                }
            }

            for (Binding bind : currComp.inAplcBinds) {
                for (Link link : RouteTable.getRoute((Device) bind.src, getCurrHost()).getLinkList()) {
                    link.deplace(bind);
                }
            }

            for (Binding bind : currComp.outCompBinds) {
                if (currPlacer.isPlaced((Component) bind.dst)) {
                    FogNode dst = currPlacer.getHost((Component) bind.dst);

                    for (Link link : RouteTable.getRoute(host, dst).getLinkList()) {
                        link.deplace(bind);
                    }
                }
            }

            for (Binding bind : currComp.inCompBinds) {
                if (currPlacer.isPlaced((Component) bind.src)) {
                    FogNode src = currPlacer.getHost((Component) bind.src);

                    for (Link link : RouteTable.getRoute(src, host).getLinkList()) {
                        link.deplace(bind);
                    }
                }
            }
        }

        return res;
    }

    private void deplace() {
        if (placed) {
            FogNode host = getCurrHost();

            // bw
            for (Binding bind : currComp.outAplcBinds) {
                for (Link link : RouteTable.getRoute(host, (Device) bind.dst).getLinkList()) {
                    link.deplace(bind);
                }
            }

            for (Binding bind : currComp.inAplcBinds) {
                for (Link link : RouteTable.getRoute((Device) bind.src, host).getLinkList()) {
                    link.deplace(bind);
                }
            }

            for (Binding bind : currComp.outCompBinds) {
                if (currPlacer.isPlaced((Component) bind.dst)) {
                    FogNode dst = currPlacer.getHost((Component) bind.dst);

                    for (Link link : RouteTable.getRoute(host, dst).getLinkList()) {
                        link.deplace(bind);
                    }
                }
            }

            for (Binding bind : currComp.inCompBinds) {
                if (currPlacer.isPlaced((Component) bind.src)) {
                    FogNode src = currPlacer.getHost((Component) bind.src);

                    for (Link link : RouteTable.getRoute(src, host).getLinkList()) {
                        link.deplace(bind);
                    }
                }
            }

            // dev res
            host.deplace(currComp);

            placed = false;
        }
    }

    public void reset() {
        deplace();

        currIdx = -1;
    }

    protected FogNode getCurrHost() {
        assert currIdx >= 0 : currComp.id + " not placed!";

        return node_list.get(currIdx);
    }

    private void rmCurrCandNode() {
        node_list.remove(currIdx);
        currIdx--;
    }

    public static void setCurrPlacer(Placer placer) {
        currPlacer = placer;
    }

    protected static void setCurrComp(Component comp) {
        currComp = comp;
    }
}