package search;

import java.util.HashMap;
import java.util.Map;

import model.app.Component;

public class CO_Recorder {
    private final OrderNode root;
    private OrderNode curr;

    public CO_Recorder() {
        root = new OrderNode(false);
        curr = root;
    }

    public void reset() {
        curr = root;
    }

    public boolean isTested(Component comp, boolean end) {
        curr = curr.getNextNode(comp, end);

        if (curr == null) {
            return true;
        }

        return false;
    }

    public boolean isNotPorper(Component comp) {
        if (curr == null) {
            return false;
        }

        curr = curr.getNextNode(comp);

        if (curr != null && curr.isEnd()) {
            return true;
        }

        return false;
    }
}

class OrderNode {
    private final Map<Component, OrderNode> succs;
    private boolean end;

    public OrderNode(boolean isEnd) {
        end = isEnd;
        succs = new HashMap<>();
    }

    public OrderNode getNextNode(Component comp, boolean end) {
        OrderNode node = succs.get(comp);

        if (node == null) {
            OrderNode newSucc = new OrderNode(end);
            succs.put(comp, newSucc);

            return newSucc;
        } else {
            if (node.isEnd()) {
                return null;
            } else {
                if (end) {
                    node.setEnd(end);
                    node.succs.clear();
                }

                return node;
            }
        }
    }

    public OrderNode getNextNode(Component comp) {
        return succs.get(comp);
    }

    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }
}