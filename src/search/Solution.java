package search;

import model.app.AppParser;
import model.app.Application;
import model.app.Component;
import model.infra.FogNode;

import common.Printer;

public class Solution {
    private final FogNode[] mapping;
    public final double score;

    public Solution(double s) {
        mapping = new FogNode[AppParser.getTotalCompNB()];
        score = s;
    }

    public Solution(Placer placer) {
        score = placer.getWAL();

        mapping = new FogNode[AppParser.getTotalCompNB()];
        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                mapping[comp.index] = placer.getHost(comp);
            }
        }
    }

    public void setHost(Component comp, FogNode n) {
        mapping[comp.index] = n;
    }

    public FogNode getHost(Component comp) {
        return mapping[comp.index];
    }

    public void print() {
        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                Printer.pass("  " + comp.id + " -> " + getHost(comp).id);
            }
        }
    }
}
