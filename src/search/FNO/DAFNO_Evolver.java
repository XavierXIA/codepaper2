package search.FNO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import model.app.AppParser;
import model.app.Application;
import model.app.Component;
import model.infra.Device;
import model.infra.FogNode;

import common.Printer;
import common.Utils;

public class DAFNO_Evolver extends AFNO_Evolver {
    protected final Wrapper[] wrapper_tab;
    protected final Set<Component> placedComp_set;

    private List<Map<Component, Device>> tarRecorder;
    private List<Map<Component, FogNode>> ancRecorder;

    public DAFNO_Evolver(boolean is2recorde) {
        super(true);

        placedComp_set = new HashSet<>(AppParser.getTotalCompNB());

        wrapper_tab = new Wrapper[AppParser.getTotalCompNB()];
        for (int i = 0; i < AppParser.getTotalCompNB(); i++) {
            wrapper_tab[i] = new Wrapper();
        }

        if (is2recorde) {
            tarRecorder = new ArrayList<Map<Component, Device>>(AppParser.getTotalCompNB());
            ancRecorder = new ArrayList<Map<Component, FogNode>>(AppParser.getTotalCompNB());
            for (int i = 0; i < AppParser.getTotalCompNB(); i++) {
                tarRecorder.add(new HashMap<Component, Device>());
                ancRecorder.add(new HashMap<Component, FogNode>());
            }
        }

        init();
    }

    @Override
    public void applyInitOrder() {
        Comparator<Component> compCompare = new Comparator<Component>() {
            @Override
            public int compare(Component c1, Component c2) {
                int DZ_size1 = c1.DZ.size();
                int DZ_size2 = c2.DZ.size();

                return DZ_size1 > DZ_size2 ? -1 : DZ_size1 == DZ_size2 ? 0 : 1;
            }
        };

        for (Application app : Application.getApps()) {
            comps2evolve = new ArrayList<>(app.getComps());
            Collections.sort(comps2evolve, compCompare);
            evolve();
        }
    }

    @Override
    public void evolve() {
        while (!comps2evolve.isEmpty()) {
            Component comp = comps2evolve.get(0);
            comps2evolve.remove(0);

            Device target = updateTarget(comp);
            setTarget(comp, target);

            FogNode anc = comp.getClosestNodeInDZ(target);

            // add bound components to comps2evolve
            if (getAnc(comp) != anc) {
                setAnc(comp, anc);

                Set<Component> boundComps = comp.getBoundComps();
                boundComps.removeAll(comps2evolve);
                boundComps.removeAll(noEvolvableComp_set);
                boundComps.removeAll(placedComp_set);

                comps2evolve.addAll(0, boundComps);
            } else if (comp.getBw2Aplc() - comp.getBw2Comp() > Utils.EPSILON) {
                noEvolvableComp_set.add(comp);
            }
        }
    }

    public void setPlaced(Component comp, FogNode node) {
        placedComp_set.add(comp);

        if (getAnc(comp) != node) {
            setAnc(comp, node);

            Set<Component> boundComps = comp.getBoundComps();
            boundComps.removeAll(placedComp_set);
            boundComps.removeAll(noEvolvableComp_set);

            comps2evolve.addAll(boundComps);

            evolve();
        }
    }

    public void setUnplaced(Component c) {
        placedComp_set.remove(c);
    }

    public void setPlaced(Component comp, FogNode node, int compPos) {
        placedComp_set.add(comp);

        if (getAnc(comp) != node) {
            ancRecorder.get(compPos).put(comp, getAnc(comp));
            setAnc(comp, node);

            Set<Component> boundComps = comp.getBoundComps();
            boundComps.removeAll(placedComp_set);
            boundComps.removeAll(noEvolvableComp_set);

            comps2evolve.addAll(boundComps);

            evolveAndStoreState(compPos);
        }
    }

    public void setUnplaced(Component c, int compPos) {
        tarRecorder.get(compPos).clear();
        ancRecorder.get(compPos).clear();

        setUnplaced(c);
    }

    public void reloadState(Collection<Component> comps, int from, int to) {
        placedComp_set.removeAll(comps);

        for (int i = from; i >= to; i--) {
            for (Entry<Component, Device> entry : tarRecorder.get(i).entrySet()) {
                setTarget(entry.getKey(), entry.getValue());
            }
            tarRecorder.get(i).clear();

            for (Entry<Component, FogNode> entry : ancRecorder.get(i).entrySet()) {
                setAnc(entry.getKey(), entry.getValue());
            }
            ancRecorder.get(i).clear();
        }
    }

    private void evolveAndStoreState(int compPos) {
        while (!comps2evolve.isEmpty()) {
            Component comp = comps2evolve.get(0);
            comps2evolve.remove(0);

            Device target = updateTarget(comp);
            Device oriTarget = getTarget(comp);

            if (oriTarget != target) {
                tarRecorder.get(compPos).put(comp, oriTarget);
                setTarget(comp, target);
            }

            FogNode anc = comp.getClosestNodeInDZ(target);
            FogNode oriAnc = getAnc(comp);

            // add bound components to comps2evolve
            if (oriAnc != anc) {
                ancRecorder.get(compPos).put(comp, oriAnc);
                setAnc(comp, anc);

                Set<Component> boundComps = comp.getBoundComps();
                boundComps.removeAll(comps2evolve);
                boundComps.removeAll(noEvolvableComp_set);
                boundComps.removeAll(placedComp_set);

                comps2evolve.addAll(0, boundComps);
            } else if (comp.getBw2Aplc()-comp.getBw2Comp() > Utils.EPSILON) {
                noEvolvableComp_set.add(comp);
            }
        }
    }

    public void sortCandidateNodes(Component comp) {
        nodeComparer.setTarget(getTarget(comp));
        Collections.sort(comp.DZ, nodeComparer);
    }

    public Device getTarget(Component comp) {
        return wrapper_tab[comp.index].getTarget();
    }

    protected void setTarget(Component comp, Device tar) {
        wrapper_tab[comp.index].setTarget(tar);
    }

    @Override
    protected FogNode getAnc(Component comp) {
        return wrapper_tab[comp.index].getAnc();
    }

    @Override
    protected void setAnc(Component comp, FogNode anc) {
        wrapper_tab[comp.index].setAnc(anc);
    }

    @Override
    protected boolean hasAnc(Component comp) {
        return wrapper_tab[comp.index].getAnc() != null;
    }

    @Override
    public void finish() {
        super.finish();

        placedComp_set.clear();
        tarRecorder = null;
        ancRecorder = null;
    }

    public void printTargets() {
        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                Printer.pass("  " + comp.id + " -> " + getTarget(comp).id);
            }
        }
    }
}

class Wrapper {
    private FogNode anc = null;
    private Device target = null;

    public FogNode getAnc() {
        return anc;
    }

    public void setAnc(FogNode anc) {
        this.anc = anc;
    }

    public Device getTarget() {
        return target;
    }

    public void setTarget(Device target) {
        this.target = target;
    }
}
