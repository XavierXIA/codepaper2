package search.FNO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.app.AppParser;
import model.app.Application;
import model.app.Binding;
import model.app.Component;
import model.infra.Device;
import model.infra.FogNode;
import model.infra.Link;
import model.route.RouteTable;

import common.Printer;
import common.Utils;

public class AFNO_Evolver {
    protected FogNode[] anc_tab;
    protected List<Component> comps2evolve;
    protected NodeComparator nodeComparer = new NodeComparator();

    protected Set<Component> noEvolvableComp_set;

    public AFNO_Evolver() {
        anc_tab = new FogNode[AppParser.getTotalCompNB()];
        noEvolvableComp_set = new HashSet<>();
        init();
    }

    protected AFNO_Evolver(boolean withoutInit) {
        noEvolvableComp_set = new HashSet<>();
    }

    public void applyInitOrder() {
        Comparator<Component> compCompare = new Comparator<Component>() {
            @Override
            public int compare(Component c1, Component c2) {
                int DZ_size1 = c1.DZ.size();
                int DZ_size2 = c2.DZ.size();

                return DZ_size1 > DZ_size2 ? -1 : DZ_size1 == DZ_size2 ? 0 : 1;
            }
        };

        for (Application app : Application.getApps()) {
            comps2evolve = new ArrayList<>(app.getComps());
            Collections.sort(comps2evolve, compCompare);
            evolve();
        }

        // sort candidate fog nodes
        NodeComparator nodeComparer = new NodeComparator();
        for (Application app : Application.getApps()) {
            for (Component c : app.getComps()) {
                nodeComparer.setTarget(getAnc(c));
                Collections.sort(c.DZ, nodeComparer);
            }
        }
    }

    public void finish() {
        anc_tab = null;
        noEvolvableComp_set.clear();
    }

    protected void evolve() {
        while (!comps2evolve.isEmpty()) {
            Component comp = comps2evolve.get(0);
            comps2evolve.remove(0);

            FogNode anc = comp.getClosestNodeInDZ(updateTarget(comp));

            // add bound components to comps2evolve
            if (getAnc(comp) != anc) {
                setAnc(comp, anc);

                Set<Component> boundComps = comp.getBoundComps();
                boundComps.removeAll(comps2evolve);
                boundComps.removeAll(noEvolvableComp_set);

                comps2evolve.addAll(0, boundComps);
            } else if (comp.getBw2Aplc()-comp.getBw2Comp() > Utils.EPSILON) {
                noEvolvableComp_set.add(comp);
            }
        }
    }

    protected void init() {
        Set<Device> roughAncs = new HashSet<>();
        Set<Device> boundObjects = new HashSet<>();

        for (Application app : Application.getApps()) {
            roughAncs.clear();

            // app with aplc
            for (Component c : app.getComps()) {
                boundObjects.clear();
                for (Binding b : c.outAplcBinds) {
                    boundObjects.add((Device) b.dst);
                }
                for (Binding b : c.inAplcBinds) {
                    boundObjects.add((Device) b.src);
                }

                if (!boundObjects.isEmpty()) {
                    Device center = getRoughCenterDev(boundObjects);
                    FogNode anc = c.getClosestNodeInDZ(center);
                    setAnc(c, anc);

                    roughAncs.add(anc);
                } else {
                    setAnc(c, null);
                }
            }

            assert !roughAncs.isEmpty() : "no objects connected with input application!";

            Device globalAnc = getRoughCenterDev(roughAncs);
            for (Component c : app.getComps()) {
                if (!hasAnc(c)) {
                    setAnc(c, c.getClosestNodeInDZ(globalAnc));
                }
            }
        }
    }

    protected Device updateTarget(Component comp) {
        // get connected devices
        Set<Device> connectDevs = new HashSet<>();
        for (Binding b : comp.inCompBinds) {
            connectDevs.add(getAnc((Component) b.src));
        }
        for (Binding b : comp.outCompBinds) {
            connectDevs.add(getAnc((Component) b.dst));
        }
        for (Binding b : comp.inAplcBinds) {
            connectDevs.add((Device) b.src);
        }
        for (Binding b : comp.outAplcBinds) {
            connectDevs.add((Device) b.dst);
        }

        // get candidates
        if (connectDevs.isEmpty()) {
            return getAnc(comp);
        }

        Set<Device> devsOnRoute = getOnRouteDevs(connectDevs);

        // get ideal place without considering DZ
        Device res = getAnc(comp);
        double min = getScore(comp, res);

        for (Device d : devsOnRoute) {
            double score = getScore(comp, d);

            if (Utils.doubleEqual(min, score)) {
                if (getMaxLat(res, connectDevs) > getMaxLat(d, connectDevs)) {
                    res = d;
                    min = score;
                }
            } else if (min > score) {
                res = d;
                min = score;
            }
        }

        return res;
    }

    protected FogNode getAnc(Component comp) {
        return anc_tab[comp.index];
    }

    protected void setAnc(Component comp, FogNode anc) {
        anc_tab[comp.index] = anc;
    }

    protected boolean hasAnc(Component comp) {
        return anc_tab[comp.index] != null;
    }

    private double getScore(Component c, Device place) {
        double score = 0.0;
        for (Binding b : c.inCompBinds) {
            score += b.reqBW * RouteTable.getRoute(getAnc((Component) b.src), place).lat;
        }
        for (Binding b : c.outCompBinds) {
            score += b.reqBW * RouteTable.getRoute(place, getAnc((Component) b.dst)).lat;
        }
        for (Binding b : c.inAplcBinds) {
            score += b.reqBW * RouteTable.getRoute((Device) b.src, place).lat;
        }
        for (Binding b : c.outAplcBinds) {
            score += b.reqBW * RouteTable.getRoute(place, (Device) b.dst).lat;
        }

        return score;
    }

    private Set<Device> getOnRouteDevs(Collection<? extends Device> devs) {
        assert devs!=null && !devs.isEmpty(): "input invalid dev set";

        Set<Device> devsOnRoute = new HashSet<>();

        if (devs.size() == 1) {
            for (Device d : devs) {
                if (d instanceof FogNode) {
                    devsOnRoute.add(d);
                } else {
                    devsOnRoute.add(getNearestNode(d));
                }

                return devsOnRoute;
            }
        }

        for (Device src : devs) {
            for (Device dst : devs) {
                for (Link l : RouteTable.getRoute(src, dst).getLinkList()) {
                    if (l.dst instanceof FogNode) {
                        devsOnRoute.add(l.dst);
                    }
                }
            }
        }

        return devsOnRoute;
    }

    private Device getRoughCenterDev(Collection<? extends Device> devs) {
        assert devs!=null && !devs.isEmpty(): "input invalid dev set";

        if (devs.size() == 1) {
            for (Device d : devs) {
                if (d instanceof FogNode) {
                    return d;
                } else {
                    return getNearestNode(d);
                }
            }
        }

        Device res = null;
        double min = Double.MAX_VALUE;

        Set<Device> dst_set = new HashSet<>(devs);

        for (Device src : devs) {
            dst_set.remove(src);
            for (Device dst : dst_set) {
                List<Link> link_list = RouteTable.getRoute(src, dst).getLinkList();
                Device candidate = link_list.get(link_list.size() / 2).src;
                double score = getMaxLat(candidate, devs);

                if (Utils.doubleEqual(min, score)) {
                    if (getLatSum(candidate, devs) < getLatSum(res, devs)) {
                        min = score;
                        res = candidate;
                    }
                } else if (min > score) {
                    min = score;
                    res = candidate;
                }
            }
        }

        return res;
    }

    private FogNode getNearestNode(Device srcDev) {
        Set<Device> currSet = new HashSet<>();
        currSet.add(srcDev);

        while (true) {
            Set<Device> tmp = new HashSet<>();

            for (Device d : currSet) {
                for (Link l : d.outLinks) {
                    if (l.dst instanceof FogNode) {
                        return (FogNode) l.dst;
                    } else {
                        tmp.add(l.dst);
                    }
                }
            }

            currSet = tmp;
        }
    }

    private double getMaxLat(Device place, Collection<? extends Device> devs) {
        double res = 0.0;

        for (Device d : devs) {
            double sum = RouteTable.getRoute(place, d).lat + RouteTable.getRoute(d, place).lat;

            if (sum > res) {
                res = sum;
            }
        }

        return res;
    }

    private double getLatSum(Device place, Collection<? extends Device> devs) {
        double sum = 0.0;

        for (Device d : devs) {
            sum += RouteTable.getRoute(place, d).lat;
            sum += RouteTable.getRoute(d, place).lat;
        }

        return sum;
    }

    public void printAncs() {
        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                Printer.pass("  " + comp.id + " -> " + getAnc(comp).id);
            }
        }
    }
}

class NodeComparator implements Comparator<FogNode> {
    Device target;

    public void setTarget(Device d) {
        target = d;
    }

    @Override
    public int compare(FogNode n1, FogNode n2) {
        double lat1 = RouteTable.getRoute(n1, target).lat + RouteTable.getRoute(target, n1).lat;
        double lat2 = RouteTable.getRoute(n2, target).lat + RouteTable.getRoute(target, n2).lat;

        if (Utils.doubleEqual(lat1, lat2)) {
            return n1.capCPU > n2.capCPU ? -1 : n1.capCPU < n2.capCPU ? 1 : 0;
        }

        return lat1 > lat2 ? 1 : -1;
    }
}