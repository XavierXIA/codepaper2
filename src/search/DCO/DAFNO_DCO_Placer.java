package search.DCO;

import model.app.AppParser;
import model.app.Component;
import model.infra.Device;
import search.CompPlacer;
import search.FNO.DAFNO_Evolver;

public class DAFNO_DCO_Placer extends DCO_Placer {
    private static DAFNO_Evolver dynEvolver;
    private Device[] appliedTarget_tab;

    public DAFNO_DCO_Placer(CompInitOrder criteria, double stepLen) {
        super(criteria, stepLen);

        appliedTarget_tab = new Device[AppParser.getTotalCompNB()];
        for (int i = 0; i < AppParser.getTotalCompNB(); i++) {
            appliedTarget_tab[i] = null;
        }
    }

    @Override
    public boolean search() {
        Component comp;
        CompPlacer tester;

        CompPlacer.setCurrPlacer(this);

        while (hasNext()) {
            comp = nextComp();
            tester = focusCompPlacer(comp);

            if (appliedTarget_tab[comp.index] != dynEvolver.getTarget(comp)) {
                dynEvolver.sortCandidateNodes(comp);
                appliedTarget_tab[comp.index] = dynEvolver.getTarget(comp);
            }

            while (tester.nextSubSolution()) {
                int forwardLen = getForwardLen();

                if (forwardLen > 0) {
                    int targetIdx = currIdx - forwardLen;

                    for (int i = targetIdx; i < currIdx; i++) {
                        focusCompPlacer(comp_list.get(i)).reset();
                    }

                    dynEvolver.reloadState(comp_list.subList(targetIdx, currIdx + 1), currIdx,
                            targetIdx);

                    comp_list.remove(comp);
                    comp_list.add(targetIdx, comp);

                    currIdx = targetIdx - 1;

                    comp = null;
                    break;
                } else {
                    comp = previous();

                    if (comp == null) {
                        return true;
                    }

                    dynEvolver.setUnplaced(comp, currIdx);
                    tester = focusCompPlacer(comp);
                }
            }

            if (comp != null) {
                dynEvolver.setPlaced(comp, getHost(comp), currIdx);
            }
        }

        return false;
    }

    @Override
    public void finish() {
        super.finish();
        appliedTarget_tab = null;
    }

    public static void setEvolver(DAFNO_Evolver evolver) {
        DAFNO_DCO_Placer.dynEvolver = evolver;
    }
}
