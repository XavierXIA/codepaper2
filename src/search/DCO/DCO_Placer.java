package search.DCO;

import model.app.Component;
import search.CO_Recorder;
import search.CompPlacer;
import search.Placer;

public class DCO_Placer extends Placer {
    private CO_Recorder recorder;
    protected final double stepLen; // half-interval if negative

    public DCO_Placer(CompInitOrder criteria, double stepLen) {
        super(criteria);

        recorder = new CO_Recorder();
        this.stepLen = stepLen;
    }

    @Override
    public boolean search() {
        Component comp;
        CompPlacer tester;

        CompPlacer.setCurrPlacer(this);

        while (hasNext()) {
            comp = nextComp();
            tester = focusCompPlacer(comp);

            while (tester.nextSubSolution()) {
                int forwardLen = getForwardLen();

                if (forwardLen > 0) {
                    int targetIdx = currIdx - forwardLen;

                    for (int i = targetIdx; i < currIdx; i++) {
                        focusCompPlacer(comp_list.get(i)).reset();
                    }

                    comp_list.remove(comp);
                    comp_list.add(targetIdx, comp);

                    currIdx = targetIdx - 1;

                    comp = null;
                    break;
                } else {
                    comp = previous();

                    if (comp == null) {
                        return true;
                    }

                    tester = focusCompPlacer(comp);
                }
            }
        }

        return false;
    }

    // return -1 if fail
    protected int getForwardLen() {
        if (currIdx == 0) {
            return -1;
        }

        storeCurrOrder();

        int forwardLen = (int) Math.min(currIdx*stepLen, currIdx);
        forwardLen = Math.max(forwardLen, 1);

        while (isCO_Tested(forwardLen)) {
            forwardLen += Math.max((currIdx - forwardLen) * stepLen, 1);

            if (forwardLen > currIdx) {
                return -1;
            }
        }

        return forwardLen;
    }

    private void storeCurrOrder() {
        recorder.reset();

        for (int i = 0; i < currIdx; i++) {
            if (recorder.isTested(comp_list.get(i), false)) {
                return;
            }
        }

        recorder.isTested(comp_list.get(currIdx), true);
    }

    // return true if tested
    private boolean isCO_Tested(int forwardLen) {
        if (currIdx == 0) {
            return true;
        }

        recorder.reset();

        for (int i = 0; i < currIdx - forwardLen; i++) {
            if (recorder.isNotPorper(comp_list.get(i))) {
                return true;
            }
        }

        if (recorder.isNotPorper(comp_list.get(currIdx))) {
            return true;
        }

        for (int i = currIdx - forwardLen; i < currIdx; i++) {
            if (recorder.isNotPorper(comp_list.get(i))) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void finish() {
        super.finish();
        recorder = null;
    }
}
