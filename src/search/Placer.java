package search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import model.app.AppParser;
import model.app.Application;
import model.app.Binding;
import model.app.Component;
import model.infra.Device;
import model.infra.FogNode;
import model.route.RouteTable;

import common.Printer;
import common.Utils;

public class Placer {
    public static enum CompInitOrder {
        REQ_BW, DZ_NODE_NB, DZ_RES, RANDOM
    };

    protected final List<Component> comp_list;
    protected final CompPlacer[] compPlacer_tab;
    protected int currIdx = -1;

    public Placer(CompInitOrder criteria) {
        comp_list = new ArrayList<>(AppParser.getTotalCompNB());
        compPlacer_tab = new CompPlacer[AppParser.getTotalCompNB()];

        for (Application app : Application.getApps()) {
            comp_list.addAll(app.getComps());

            for (Component comp : app.getComps()) {
                compPlacer_tab[comp.index] = new CompPlacer(comp);
            }
        }

        orderComps(criteria);
    }

    protected void orderComps(CompInitOrder criteria) {
        switch (criteria) {
        case DZ_NODE_NB:
            Collections.sort(comp_list, new Comparator<Component>() {
                @Override
                public int compare(Component c1, Component c2) {
                    int DZ_size1 = c1.DZ.size();
                    int DZ_size2 = c2.DZ.size();

                    return DZ_size1 > DZ_size2 ? 1 : DZ_size1 == DZ_size2 ? 0 : -1;
                }
            });

            break;

        case REQ_BW:
            Collections.sort(comp_list, new Comparator<Component>() {
                @Override
                public int compare(Component c1, Component c2) {
                    double bw1 = c1.getTotalBW();
                    double bw2 = c2.getTotalBW();

                    if (Utils.doubleEqual(bw1, bw2)) {
                        int DZ_size1 = c1.DZ.size();
                        int DZ_size2 = c2.DZ.size();

                        return DZ_size1 > DZ_size2 ? 1 : DZ_size1 == DZ_size2 ? 0 : -1;
                    }

                    return bw1 > bw2 ? -1 : 1;
                }
            });

            break;

        case DZ_RES:
            // TODO
            break;

        case RANDOM:
            Collections.shuffle(comp_list);
            break;

        default:
            Printer.err("undefined InitCO !");
        }
    }

    // not thread safe
    // return true if fail
    public boolean search() {
        Component comp;
        CompPlacer compPlacer;

        CompPlacer.setCurrPlacer(this);

        while (hasNext()) {
            comp = nextComp();
            compPlacer = focusCompPlacer(comp);

            while (compPlacer.nextSubSolution()) {
                comp = previous();

                if (comp == null) {
                    return true;
                }

                compPlacer = focusCompPlacer(comp);
            }
        }

        return false;
    }

    protected boolean hasNext() {
        return currIdx + 1 < comp_list.size();
    }

    protected Component nextComp() {
        if (currIdx + 1 < comp_list.size()) {
            currIdx++;
        }

        return comp_list.get(currIdx);
    }

    protected Component previous() {
        if (currIdx > 0) {
            currIdx--;
        } else {
            return null;
        }

        return comp_list.get(currIdx);
    }

    protected CompPlacer focusCompPlacer(Component comp) {
        CompPlacer.setCurrComp(comp);
        return compPlacer_tab[comp.index];
    }

    protected boolean isPlaced(Component comp) {
        return compPlacer_tab[comp.index].placed;
    }

    protected FogNode getHost(Component comp) {
        assert isPlaced(comp) : "comp " + comp.id + " not placed yet";

        return compPlacer_tab[comp.index].getCurrHost();
    }

    public double getWAL() {
        double score = 0.0;

        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                for (Binding bind : comp.outAplcBinds) {
                    double lat = RouteTable.getRoute(getHost(comp), (Device) bind.dst).lat;
                    score += lat * bind.reqBW / Application.getTotalBW();
                }

                for (Binding bind : comp.inAplcBinds) {
                    double lat = RouteTable.getRoute((Device) bind.src, getHost(comp)).lat;
                    score += lat * bind.reqBW / Application.getTotalBW();
                }

                for (Binding bind : comp.outCompBinds) {
                    FogNode src = getHost(comp);
                    FogNode dst = getHost((Component) bind.dst);

                    double lat = RouteTable.getRoute(src, dst).lat;
                    score += lat * bind.reqBW / Application.getTotalBW();
                }
            }
        }

        return score;
    }

    public double getAverageLatency() {
        double sum = 0.0;
        int count = 0;

        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                for (Binding bind : comp.outAplcBinds) {
                    sum += RouteTable.getRoute(getHost(comp), (Device) bind.dst).lat;
                    count++;
                }

                for (Binding bind : comp.inAplcBinds) {
                    sum += RouteTable.getRoute((Device) bind.src, getHost(comp)).lat;
                    count++;
                }

                for (Binding bind : comp.outCompBinds) {
                    FogNode src = getHost(comp);
                    FogNode dst = getHost((Component) bind.dst);

                    sum += RouteTable.getRoute(src, dst).lat;
                    count++;
                }
            }
        }

        return sum / count;
    }

    public double getDiameter() {
        double max = 0.0;
        Set<Device> connectedObjects = new HashSet<>();

        for (Application app : Application.getApps()) {
            connectedObjects.clear();

            List<Component> comp_list = new ArrayList<>(app.getComps());
            for (Component comp : comp_list) {
                for (Binding bind : comp.outAplcBinds) {
                    connectedObjects.add((Device) bind.dst);
                }

                for (Binding bind : comp.inAplcBinds) {
                    connectedObjects.add((Device) bind.src);
                }
            }

            for (int i = 0; i < comp_list.size(); i++) {
                Component src = comp_list.get(i);

                for (int j = i + 1; j < comp_list.size(); j++) {
                    FogNode srcHost = getHost(src);
                    FogNode dstHost = getHost(comp_list.get(j));

                    double lat = RouteTable.getRoute(srcHost, dstHost).lat;
                    if (lat > max) {
                        max = lat;
                    }

                    lat = RouteTable.getRoute(dstHost, srcHost).lat;
                    if (lat > max) {
                        max = lat;
                    }
                }

                for (Device d : connectedObjects) {
                    FogNode srcHost = getHost(src);

                    double lat = RouteTable.getRoute(srcHost, d).lat;
                    if (lat > max) {
                        max = lat;
                    }

                    lat = RouteTable.getRoute(d, srcHost).lat;
                    if (lat > max) {
                        max = lat;
                    }
                }
            }
        }

        return max;
    }

    public void reset() {
        for (int i = 0; i < comp_list.size(); i++) {
            focusCompPlacer(comp_list.get(i)).reset();
        }
    }

    public void finish() {
        comp_list.clear();
    }

    public void printCompOrder() {
        for (Component comp : comp_list) {
            Printer.pass(comp.id);
        }
    }

    public void PrintFirstComp() {
        Printer.info(comp_list.get(0).id);
    }
}