package common;

//TODO : generalize units / make units use-case dependent / use input config
public class Units {
    public static final String CPU = "Gf";
    public static final String RAM = "GB";
    public static final String DISK = "GB";
    public static final String BW = "MBps";
    public static final String LAT = "ms";

    public static final int G = 1000000000;
    public static final int M = 1000000;

    public static double RAM2Double(String s) {
        if (s.endsWith(Units.RAM)) {
            return Double.parseDouble(s.substring(0, s.length() - 2));
        }

        Printer.warning("RAM unit not GB!");
        Printer.warning("Input : " + s);
        return -1;
    }

    public static double DISK2Double(String s) {
        if (s.endsWith(Units.DISK)) {
            return Double.parseDouble(s.substring(0, s.length() - 2));
        }

        Printer.warning("DISK unit not GB!");
        Printer.warning("Input : " + s);

        return -1;
    }

    public static double BW2Double(String s) {
        if (s.endsWith(Units.BW)) {
            return Double.parseDouble(s.substring(0, s.length() - 4));
        }

        Printer.warning("BW unit not MBps!");
        Printer.warning("Input : " + s);

        return -1;
    }

    public static double LAT2Double(String s) {
        if (s.endsWith(Units.LAT)) {
            return Double.parseDouble(s.substring(0, s.length() - 2));
        }

        Printer.warning("Latency unit not ms!");
        Printer.warning("Input : " + s);

        return -1;
    }

    public static double CPU2Double(String s) {
        if (s.endsWith(Units.CPU)) {
            return Double.parseDouble(s.substring(0, s.length() - 2));
        }

        Printer.warning("CPU unit not Gf!");
        Printer.warning("Input : " + s);

        return -1;
    }
}