package common;

public class Printer {
    private static boolean isColor = false;

    public static void enableColor() {
        isColor = true;
    }

    public static void info(String s) {
        if (isColor) {
            System.out.println("\u001B[34m" + s + "\u001B[0m");
        } else {
            System.out.println(s);
        }
    }

    public static void pass(String s) {
        if (isColor) {
            System.out.println("\u001B[32m" + s + "\u001B[0m");
        } else {
            System.out.println(s);
        }
    }

    public static void warning(String s) {
        if (isColor) {
            System.out.println("\u001B[33m" + "Warning : " + s + "\u001B[0m");
        } else {
            System.out.println(s);
        }
    }

    public static void err(String s) {
        if (isColor) {
            System.out.println("\u001B[31m" + "Err : " + s + "\u001B[0m");
        } else {
            System.out.println(s);
        }
    }
}
