package common;

public class RunTimeCounter {
    private static long startTime = 0;
    private static long runTime = 0;

    public static void start() {
        startTime = System.nanoTime();
    }

    public static void pause() {
        runTime += (System.nanoTime() - startTime);
    }

    public static double end() {
        double res = (runTime + System.nanoTime() - startTime) / 1e9;
        runTime = 0;

        return res;
    }
}
