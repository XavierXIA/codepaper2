package common;

import java.util.Random;

public class Utils {
    public static final double EPSILON = 0.00001;
    private static final Random random = new Random();

    // return true if equal
    public static boolean doubleEqual(double in1, double in2) {
        return Math.abs(in1 - in2) < EPSILON;
    }

    public static boolean nextBoolean() {
        return random.nextBoolean();
    }

    public static double nextDouble() {
        return random.nextDouble();
    }

    public static int nextInt(int max) {
        return random.nextInt(max);
    }
}