package simulation;

import search.Solution;

public abstract class DeployGenerator {
    protected static String outPath = "";
    protected static String DUParaFilePath = "";

    public static String getOutPath() {
        return outPath;
    }

    public static void setPaths(String p, String DU_Path) {
        outPath = p;
        DUParaFilePath = DU_Path;
    }

    public abstract void saveDeployment(Solution solution);
}
