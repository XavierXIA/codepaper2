package simulation;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import model.app.Application;
import model.app.Binding;
import model.app.Component;
import model.infra.Appliance;
import model.infra.Device;
import model.infra.FogNode;
import model.infra.InfraParser;
import model.infra.Infrastructure;
import model.infra.Link;
import model.route.Route;
import model.route.RouteTable;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import search.Solution;

import common.Units;

public class PlatformGenerator {
    private static String outPath = "";

    public static void saveFloydPlatform() {
        assert !outPath.isEmpty() : "empty outPath!";

        Document dom;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            dom = builder.newDocument();

            Element platform = dom.createElement("platform");
            platform.setAttribute("version", "4.1");

            Element zone = dom.createElement("zone");
            zone.setAttribute("id", "z0");
            zone.setAttribute("routing", "Floyd");

            // FogNode
            for (FogNode n : Infrastructure.getFogNodes()) {
                Element host = dom.createElement("host");
                host.setAttribute("id", n.id);
                host.setAttribute("speed", n.capCPU + Units.CPU);
                host.setAttribute("core", "1");

                zone.appendChild(host);
            }

            // Appliance
            for (Appliance a : Infrastructure.getAppliances()) {
                Element host = dom.createElement("host");
                host.setAttribute("id", a.id);
                host.setAttribute("speed", 0.1 + Units.CPU);

                zone.appendChild(host);
            }

            // link
            List<Link> link_list = new ArrayList<>(InfraParser.getDevNB() * 2);

            for (FogNode n : Infrastructure.getFogNodes()) {
                for (Link l : n.outLinks) {
                    link_list.add(l);
                }
            }
            for (Appliance a : Infrastructure.getAppliances()) {
                for (Link l : a.outLinks) {
                    link_list.add(l);
                }
            }

            int i = 0;
            for (Link l : link_list) {
                Element link = dom.createElement("link");
                link.setAttribute("id", "l_" + i);
                link.setAttribute("bandwidth", l.capBW + Units.BW);
                link.setAttribute("latency", l.lat + Units.LAT);

                zone.appendChild(link);
                i++;
            }

            // route
            i = 0;
            for (Link l : link_list) {
                Element route = dom.createElement("route");

                String src = l.src.id;
                String dst = l.dst.id;

                route.setAttribute("src", src);
                route.setAttribute("dst", dst);
                route.setAttribute("symmetrical", "NO");

                Element link_ctn = dom.createElement("link_ctn");
                link_ctn.setAttribute("id", "l_" + i);
                route.appendChild(link_ctn);

                zone.appendChild(route);
                i++;
            }

            platform.appendChild(zone);

            dom.appendChild(platform);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            dom.setXmlStandalone(true);

            File file = new File(outPath);

            if (file.getParentFile() != null) {
                file.getParentFile().mkdirs();
            }

            file.createNewFile();

            tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(file)));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void saveFullPlatform(Solution solution) {
        assert !outPath.isEmpty() : "empty outPath!";

        Document dom;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            dom = builder.newDocument();

            Element platform = dom.createElement("platform");
            platform.setAttribute("version", "4.1");

            Element zone = dom.createElement("zone");
            zone.setAttribute("id", "z0");
            zone.setAttribute("routing", "Full");

            // FogNode
            for (FogNode n : Infrastructure.getFogNodes()) {
                Element host = dom.createElement("host");
                host.setAttribute("id", n.id);
                host.setAttribute("speed", n.capCPU + Units.CPU);
                host.setAttribute("core", "1");

                zone.appendChild(host);
            }

            // Appliance
            for (Appliance a : Infrastructure.getAppliances()) {
                Element host = dom.createElement("host");
                host.setAttribute("id", a.id);
                host.setAttribute("speed", 0.1 + Units.CPU);

                zone.appendChild(host);
            }

            // link
            List<Link> link_list = new ArrayList<>(InfraParser.getDevNB() * 2);

            for (FogNode n : Infrastructure.getFogNodes()) {
                for (Link l : n.outLinks) {
                    link_list.add(l);
                }
            }
            for (Appliance a : Infrastructure.getAppliances()) {
                for (Link l : a.outLinks) {
                    link_list.add(l);
                }
            }

            int i = 0;
            for (Link l : link_list) {
                Element link = dom.createElement("link");
                link.setAttribute("id", "l_" + i);
                link.setAttribute("bandwidth", l.capBW + Units.BW);
                link.setAttribute("latency", l.lat + Units.LAT);

                zone.appendChild(link);
                i++;
            }

            // route
            Set<Route> route_set = new HashSet<>();

            for (Application app : Application.getApps()) {
                for (Component comp : app.getComps()) {
                    FogNode host = solution.getHost(comp);

                    for (Binding bind : comp.outAplcBinds) {
                        if (host != bind.dst) {
                            route_set.add(RouteTable.getRoute(host, (Device) bind.dst));
                        }
                    }

                    for (Binding bind : comp.inAplcBinds) {
                        if (host != bind.src) {
                            route_set.add(RouteTable.getRoute((Device) bind.src, host));
                        }
                    }

                    for (Binding bind : comp.outCompBinds) {
                        FogNode dst = solution.getHost((Component) bind.dst);

                        if (host != dst) {
                            route_set.add(RouteTable.getRoute(host, dst));
                        }
                    }
                }
            }

            for (Route r : route_set) {
                List<Link> links = r.getLinkList();

                Element route = dom.createElement("route");

                route.setAttribute("src", links.get(0).src.id);
                route.setAttribute("dst", links.get(links.size() - 1).dst.id);
                route.setAttribute("symmetrical", "NO");

                for (Link link : links) {
                    Element link_ctn = dom.createElement("link_ctn");
                    link_ctn.setAttribute("id", "l_" + link_list.indexOf(link));
                    route.appendChild(link_ctn);
                }

                zone.appendChild(route);
            }

            platform.appendChild(zone);

            dom.appendChild(platform);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            dom.setXmlStandalone(true);

            File file = new File(outPath);

            if (file.getParentFile() != null) {
                file.getParentFile().mkdirs();
            }

            file.createNewFile();

            tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(file)));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static String getOutPath() {
        return outPath;
    }

    public static void setOutPath(String p) {
        outPath = p;
    }
}