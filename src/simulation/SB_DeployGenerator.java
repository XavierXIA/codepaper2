package simulation;

import java.io.File;
import java.io.FileOutputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import model.app.Application;
import model.app.Binding;
import model.app.Component;
import model.infra.Appliance;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import search.Solution;

import common.Units;

public class SB_DeployGenerator extends DeployGenerator {
    @Override
    public void saveDeployment(Solution solution) {
        assert !outPath.isEmpty() : "empty outPath!";

        Document dom;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            dom = builder.newDocument();

            Element platform = dom.createElement("platform");
            platform.setAttribute("version", "4.1");

            for (Application app : Application.getApps()) {
                for (Component comp : app.getComps()) {
                    Element deploy = dom.createElement("actor");

                    deploy.setAttribute("host", solution.getHost(comp).id);
                    deploy.setAttribute("function", "RunVM");

                    String id = comp.id;
                    String[] info = getInfo(id);

                    Element idx = dom.createElement("argument");
                    idx.setAttribute("value", info[2]);
                    deploy.appendChild(idx);

                    Element type = dom.createElement("argument");
                    type.setAttribute("value", info[1]);
                    deploy.appendChild(type);

                    Element appIdx = dom.createElement("argument");
                    appIdx.setAttribute("value", info[0]);
                    deploy.appendChild(appIdx);

                    Element cpu = dom.createElement("argument");
                    cpu.setAttribute("value", "" + comp.reqCPU * Units.G);
                    deploy.appendChild(cpu);

                    platform.appendChild(deploy);
                }
            }

            addAppliance(dom, platform);
            dom.appendChild(platform);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            dom.setXmlStandalone(true);

            File file = new File(outPath);
            if (file.getParentFile() != null) {
                file.getParentFile().mkdirs();
            }
            file.createNewFile();

            tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(file)));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private static void addAppliance(Document dom, Element platform) {
        for (Application app : Application.getApps()) {
            for (Component comp : app.getComps()) {
                for (Binding bind : comp.outAplcBinds) {
                    Appliance aplc = (Appliance) bind.dst;

                    Element deploy = dom.createElement("actor");

                    String id = aplc.id;
                    String[] info = getAplcInfo(id);

                    deploy.setAttribute("host", id);
                    deploy.setAttribute("function", info[0]);

                    Element idx = dom.createElement("argument");
                    idx.setAttribute("value", info[1]);
                    deploy.appendChild(idx);

                    platform.appendChild(deploy);
                }
            }
        }

//        for (Appliance aplc : Infrastructure.getAppliances()) {
//            Element deploy = dom.createElement("actor");
//
//            String id = aplc.id;
//            String[] info = getAplcInfo(id);
//
//            deploy.setAttribute("host", id);
//            deploy.setAttribute("function", info[0]);
//
//            Element idx = dom.createElement("argument");
//            idx.setAttribute("value", info[1]);
//            deploy.appendChild(idx);
//
//            platform.appendChild(deploy);
//        }
    }

    private static String[] getInfo(String id) {
        Pattern p = Pattern.compile("^app([0-9]+):(.*)-([0-9]+)$");
        Matcher m = p.matcher(id);
        boolean isFound = m.find();

        assert isFound : "Warning : not matched!";

        String[] res = {m.group(1), m.group(2), m.group(3)};
        return res;
    }

    private static String[] getAplcInfo(String id) {
        Pattern p = Pattern.compile("^(.*)-([0-9]+)$");
        Matcher m = p.matcher(id);
        boolean isFound = m.find();

        assert isFound : "Warning : not matched!";

        String[] res = {m.group(1), m.group(2)};
        return res;
    }
}