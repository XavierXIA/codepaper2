package simulation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import model.app.Application;
import model.app.Component;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import search.Solution;

import common.Printer;
import common.Units;

public class DSP_DeployGenerator extends DeployGenerator {
    private Map<String, String> dataUnitParas;

    @Override
    public void saveDeployment(Solution solution) {
        assert !outPath.isEmpty() : "empty outPath!";
        assert !DUParaFilePath.isEmpty() : "empty DUParaFilePath!";

        parseDataUnitParas();

        Document dom;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            dom = builder.newDocument();

            Element platform = dom.createElement("platform");
            platform.setAttribute("version", "4.1");

            for (Application app : Application.getApps()) {
                for (Entry<String, String> entry : dataUnitParas.entrySet()) {
                    if (entry.getKey().indexOf("dataSrc") == 0) {
                        Pattern p = Pattern.compile(app.id + ":(.+)$");
                        Matcher m = p.matcher(entry.getKey());

                        if (m.find()) {
                            if (m.group(1).indexOf("Comp") < 0) {// device
                                Element deploy = dom.createElement("actor");

                                deploy.setAttribute("host", m.group(1));
                                deploy.setAttribute("function", "DataSource");

                                String[] dataUnitPara = entry.getValue().split(",");

                                Element sendBW = dom.createElement("argument");
                                sendBW.setAttribute("value", "" + dataUnitPara[0]);
                                deploy.appendChild(sendBW);

                                Element commSize = dom.createElement("argument");
                                commSize.setAttribute("value", dataUnitPara[1]);
                                deploy.appendChild(commSize);

                                Element calSize = dom.createElement("argument");
                                calSize.setAttribute("value", dataUnitPara[2]);
                                deploy.appendChild(calSize);

                                Element toList = dom.createElement("argument");
                                toList.setAttribute("value", dataUnitPara[3]);
                                deploy.appendChild(toList);

                                platform.appendChild(deploy);
                            }
                        }
                    } else if (entry.getKey().indexOf("dataCon") == 0) {
                        Pattern p = Pattern.compile(app.id + ":(.+)$");
                        Matcher m = p.matcher(entry.getKey());

                        if (m.find()) {
                            if (m.group(1).indexOf("Comp") < 0) {// device
                                Element deploy = dom.createElement("actor");

                                deploy.setAttribute("host", m.group(1));
                                deploy.setAttribute("function", "DataConsumer");

                                Element idE = dom.createElement("argument");
                                idE.setAttribute("value", app.id + ":" + m.group(1));
                                deploy.appendChild(idE);

                                Element recBW = dom.createElement("argument");
                                recBW.setAttribute("value", entry.getValue());
                                deploy.appendChild(recBW);

                                platform.appendChild(deploy);
                            }
                        }
                    }
                }

                for (Component comp : app.getComps()) {
                    Element deploy = dom.createElement("actor");

                    deploy.setAttribute("host", solution.getHost(comp).id);
                    deploy.setAttribute("function", "RunVM");

                    // cpu cap
                    Element cpu = dom.createElement("argument");
                    cpu.setAttribute("value", "" + comp.reqCPU * Units.G);
                    deploy.appendChild(cpu);

                    if (addCompDataSrc(app.id + ":" + comp.id, dom, deploy)) {
                        if (addCompDataCon(app.id + ":" + comp.id, dom, deploy)) {
                            if (addOperator(comp.id, dom, deploy)) {
                                Printer.err("dataUnit parameter of " + comp.id + " not found !");
                            }
                        }
                    }

                    platform.appendChild(deploy);
                }
            }

            dom.appendChild(platform);

            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "http://simgrid.gforge.inria.fr/simgrid/simgrid.dtd");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            dom.setXmlStandalone(true);

            File file = new File(outPath);

            if (file.getParentFile() != null) {
                file.getParentFile().mkdirs();
            }

            file.createNewFile();

            tr.transform(new DOMSource(dom), new StreamResult(new FileOutputStream(file)));
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private boolean addCompDataSrc(String id, Document dom, Element deploy) {
        String paras = dataUnitParas.get("dataSrc:" + id);

        if (paras == null) {
            return true;
        }

        String[] para_tab = paras.split(",");

        Element func = dom.createElement("argument");
        func.setAttribute("value", "DataSource");
        deploy.appendChild(func);

        Element sendBW = dom.createElement("argument");
        sendBW.setAttribute("value", "" + para_tab[0]);
        deploy.appendChild(sendBW);

        Element commSize = dom.createElement("argument");
        commSize.setAttribute("value", para_tab[1]);
        deploy.appendChild(commSize);

        Element calSize = dom.createElement("argument");
        calSize.setAttribute("value", para_tab[2]);
        deploy.appendChild(calSize);

        Element toList = dom.createElement("argument");
        toList.setAttribute("value", para_tab[3]);
        deploy.appendChild(toList);

        return false;
    }

    private boolean addCompDataCon(String id, Document dom, Element deploy) {
        String paras = dataUnitParas.get("dataCon:" + id);

        if (paras == null) {
            return true;
        }

        Element func = dom.createElement("argument");
        func.setAttribute("value", "DataConsumer");
        deploy.appendChild(func);

        Element idE = dom.createElement("argument");
        idE.setAttribute("value", id);
        deploy.appendChild(idE);

        Element recBW = dom.createElement("argument");
        recBW.setAttribute("value", paras);
        deploy.appendChild(recBW);

        return false;
    }

    private boolean addOperator(String id, Document dom, Element deploy) {
        String paras = dataUnitParas.get("operator:" + id);

        if (paras == null) {
            return true;
        }

        Element func = dom.createElement("argument");
        func.setAttribute("value", "Operator");
        deploy.appendChild(func);

        Element idE = dom.createElement("argument");
        idE.setAttribute("value", id);
        deploy.appendChild(idE);

        String[] para_tab = paras.split(",");

        Element recBW = dom.createElement("argument");
        recBW.setAttribute("value", para_tab[0]);
        deploy.appendChild(recBW);

        Element sendBW = dom.createElement("argument");
        sendBW.setAttribute("value", "" + para_tab[1]);
        deploy.appendChild(sendBW);

        Element commSize = dom.createElement("argument");
        commSize.setAttribute("value", para_tab[2]);
        deploy.appendChild(commSize);

        Element calSize = dom.createElement("argument");
        calSize.setAttribute("value", para_tab[3]);
        deploy.appendChild(calSize);

        Element toList = dom.createElement("argument");
        toList.setAttribute("value", para_tab[4]);
        deploy.appendChild(toList);

        return false;
    }

    private void parseDataUnitParas() {
        try {
            File file = new File(DUParaFilePath);
            Scanner sc = new Scanner(file);
            dataUnitParas = new HashMap<>();

            while (sc.hasNextLine()) {
                dataUnitParas.put(sc.nextLine(), sc.nextLine());
            }

            sc.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
